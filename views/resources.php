<style>
    #box-shadow {
  padding-top: 40px;
  padding-right: 40px;
  padding-left: 40px;
  padding-bottom: 32px;
  box-shadow: 5px 6px 18px #d7d7d7;
  min-height: 330px;
}
</style>
<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/NationalPEO-Resources.jpg
">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Resources</h1>
            <span>Blogs, Training, Events, and More</span>
        </div>
    </div>
</section>

<section>
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-lg-3">
                    <div id="box-shadow">
                        <h2>Forms</h2>
                        <p>Everything from tax forms to new hire packets, you are sure to find the form you need here.</p>
                        <div class="inside-spacer"></div>
                        <p class="font-size-15"><a href="<?php echo basePathUrl();?>peo-forms" class="home-link">View Forms ></a></p>
                    </div>
                    <div class="section-spacer-20"></div>
                </div>
                
                <div class="col-lg-3">
                    <div id="box-shadow">
                        <h2>PEO Facts</h2>
                        <p>Learn about PEO’s and the industry to gain a better understanding of what National PEO offers.</p>
                        <div class="inside-spacer"></div>
                        <p class="font-size-15"><a href="<?php echo basePathUrl();?>peo-facts" class="home-link">View Facts ></a></p>
                    </div>
                    <div class="section-spacer-20"></div>
                </div>
                
                <div class="col-lg-3">
                    <div id="box-shadow">
                        <h2>State & Federal Links</h2>
                        <p>Links to various state and federal agencies simplifying your search.</p>
                        <div class="inside-spacer"></div>
                        <p class="font-size-15"><a href="https://www.nationalpeo.com/state-federal-links/" class="home-link">View Links ></a></p>
                    </div>
                    <div class="section-spacer-20"></div>
                </div>
                
                <div class="col-lg-3">
                    <div id="box-shadow">
                        <h2>Articles</h2>
                        <p>Professional articles written by our staff and industry contributors.</p>
                        <div class="inside-spacer"></div>
                        <p class="font-size-15"><a href="https://www.nationalpeo.com/article/" class="home-link">View Articles ></a></p>
                    </div>
                    <div class="section-spacer-20"></div>
                </div>
                
                <div class="col-lg-3">
                    <div id="box-shadow">
                        <h2>Testimonials</h2>
                        <p>Read for yourself what our clients have to say about National PEO and the services we offer.</p>
                        <div class="inside-spacer"></div>
                        <p class="font-size-15"><a href="https://www.nationalpeo.com/client-testimonials/" class="home-link">View Testimonials ></a></p>
                    </div>
                    <div class="section-spacer-20"></div>
                </div>
                
                <div class="col-lg-3">
                    <div id="box-shadow">
                        <h2>Help Center</h2>
                        <p>Help is right around the corner by visiting our help center.</p>
                        <div class="inside-spacer"></div>
                        <p class="font-size-15"><a href="https://www.nationalpeo.com/help-center/" class="home-link">View Help Center ></a></p>
                    </div>
                    <div class="section-spacer-20"></div>
                </div>
                
                <div class="col-lg-3">
                    <div id="box-shadow">
                        <h2>Case Studies</h2>
                        <p>Real problems and real solutions that we have provided our clients.</p>
                        <div class="inside-spacer"></div>
                        <p class="font-size-15"><a href="https://www.nationalpeo.com/case-studies/" class="home-link">View Case Studies ></a></p>
                    </div>
                    <div class="section-spacer-20"></div>
                </div>
                
                <div class="col-lg-3">
                    <div id="box-shadow">
                        <h2>Press Releases</h2>
                        <p>Read what the press has to say about National PEO.</p>
                        <div class="inside-spacer"></div>
                        <p class="font-size-15"><a href="https://www.nationalpeo.com/press-releases/" class="home-link">View Press Releases ></a></p>
                    </div>
                    <div class="section-spacer-20"></div>
                </div>
            
            </div>
        </div>
    </div>
</section>
