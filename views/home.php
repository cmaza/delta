
    <section class="fullscreen home" data-bg-parallax-home="<?php echo basePathUrl();?>images/home/National-PEO-Home-Hero.png">
        <div class=""></div>
        <div class="container-wide">
            <div class="container-fullscreen">
                <div class="text-middle">
                    <div class="heading-text col-lg-7">
                        <h1><span>We Take Care of Your Employees. &nbsp;
                        You Take Care of Your Business.</span></h1>
                        <p></p>
                        <p><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Request a Quote</a>&nbsp;&nbsp;<a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Request a Demo</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h2>Empowering Business Successes Together</h2>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div>
                        <h4>Who We Are</h4>
                        <div class="inside-spacer"></div>
                        <p>We help business owners focus on revenue-producing tasks and not on clerical duties. We pride ourself in providing HR and payroll solutions. It all starts with our technology and our people. We offer many different services to meet your needs with easy-to-use technology and Human Resources experts. Since 1999 National PEO has been helping small businesses with their back-office administrative tasks.</p>
                        <p class="m-t-40 m-b-40"><a href="<?php echo basePathUrl();?>why-national-peo" class="btn btn-rounded btn-light">Learn More</a></p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/Atlas-Home-Who-We-Are.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-10"></div>
    </section>
    
    <section class="background-grey p-t-50">
    <div class="section-spacer-40"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Atlas-Home-Specializing-in-PEO.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Specializing in PEO Services</h4>
                    <div class="inside-spacer"></div>
                    <p>By partnering with National PEO, companies gain access to our team of service professionals who specialize in human resources, payroll, workers' compensation, risk management, unemployment, benefits administration, and employment-related compliance.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-10"></div>
    </section>
    
    <section>
        <div class="section-spacer-20"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div>
                        <h4>More Time, More Money</h4>
                        <div class="inside-spacer"></div>
                        <p>National PEO will help eliminate all administrative tasks that diminish clients’ ability to focus on growing their businesses. Additionally, National PEO has a team of seasoned professionals to customize business solutions and negotiate services on clients’ behalf.</p>
                        <p>National PEO is an industry-leading PEO specializing in providing employers with best-in-class HR outsourcing services and support.</p>
                        <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/Atlas-Home-More-Time.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="section-spacer-10"></div>
    </section>
    <!-- end: Content -->