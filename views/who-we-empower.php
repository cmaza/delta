<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-Industry-Solutions.png">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Industry Solutions</h1>
            <span></span>
        </div>
    </div>
</section>

<section>
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="row text-center">
                <div class="col-lg-4">
                    <img src="<?php echo basePathUrl();?>images/home/icons/restaurant-icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/restaurant"><font color="54575a">Restaurant</font></a>
                    </h5>
                    <p class="font-size-15">For two decades, we have partnered with restaurants to develop customized solutions that simplify hiring and onboarding processes, streamline payroll processing, and ensure compliance with labor laws.</p>
                    <p class="font-size-15"><a href="<?php echo basePathUrl();?>industry/restaurant" class="home-link">Learn More ></a></p>
                    <div class="section-spacer-30"></div>
                </div>
                <div class="col-lg-4">
                    <img src="<?php echo basePathUrl();?>images/home/icons/health-care-icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/healthcare"><font color="54575a">Health Care</font></a>
                    </h5>
                    <p class="font-size-15">Health care organizations depend on us to foster safe work spaces, develop and administer excellent benefits packages, and manage employee time and attendance efficiently in a 24-hour industry.</p>
                    <p class="font-size-15"><a href="<?php echo basePathUrl();?>industry/healthcare" class="home-link">Learn More ></a></p>
                    <div class="section-spacer-30"></div>
                </div>
                <div class="col-lg-4">
                    <img src="<?php echo basePathUrl();?>images/home/icons/tech-icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/tech"><font color="54575a">Tech</font></a>
                    </h5>
                    <p class="font-size-15">Long-term employee relationships is vital to technological innovation. We support tech organizations with high-quality benefits, streamlined employee management, and Hire to Retire programs.</p>
                    <p class="font-size-15"><a href="<?php echo basePathUrl();?>industry/tech" class="home-link">Learn More ></a></p>
                    <div class="section-spacer-30"></div>
                </div>
                <div class="col-lg-4">
                    <img src="<?php echo basePathUrl();?>images/home/icons/non-profit-icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/non-profit"><font color="54575a">Non-Profit</font></a>
                    </h5>
                    <p class="font-size-15">We keep non-profit organizations’ HR, payroll, and benefits systems running smoothly so that they can direct their energy and resources toward serving the needs of their communities.</p>
                    <p class="font-size-15"><a href="<?php echo basePathUrl();?>industry/non-profit" class="home-link">Learn More ></a></p>
                    <div class="section-spacer-30"></div>
                </div>
                <div class="col-lg-4">
                    <img src="<?php echo basePathUrl();?>images/home/icons/construction-icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/construction"><font color="54575a">Construction</font></a>
                    </h5>
                    <p class="font-size-15">Construction organizations trust our workers’ compensation, time and attendance, and OSHA compliance services to keep their businesses running safely and efficiently.</p>
                    <p class="font-size-15"><a href="<?php echo basePathUrl();?>industry/construction" class="home-link">Learn More ></a></p>
                    <div class="section-spacer-30"></div>
                </div>
                <div class="col-lg-4">
                    <img src="<?php echo basePathUrl();?>images/home/icons/manufacturing-icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/manufacturing"><font color="54575a">Manufacturing</font></a>
                    </h5>
                    <p class="font-size-15">Manufacturing organizations rely on our safety management expertise, benefits administration, and sophisticated HRIS programs to meet the complex, constantly evolving needs of their industry.</p>
                    <p class="font-size-15"><a href="<?php echo basePathUrl();?>industry/manufacturing" class="home-link">Learn More ></a></p>
                    <div class="section-spacer-30"></div>
                </div>
            </div>
        </div>
    </div>

 <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Learn How National PEO Can Work for You</h4>
                <p class="m-t-20 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
                <div class="section-spacer-10"></div>
            </div>
        </div>
    </div>
    
</section>

