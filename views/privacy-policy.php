<section id="page-title" class="privacy-terms">
    <div class="container">
        <div class="page-title text-light">
            <h1>Privacy Policy</h1>
            <div class="line-small-light"></div>
        </div>
    </div>
</section>

<section id="privacy-terms">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="under">Privacy Policy</h4>
                <p>This Privacy Statement describes how National PEO and our subsidiaries and affiliates collect, use, share and protect business, financial, and personal information. This statement applies to all information collected or submitted on this website and mobile applications (“Site”). This notice is available on the homepage of this Site and at every login page where personally identifiable information may be requested.
                    Your privacy, and the privacy of the information provided, is important to us. We use reasonable care to protect your data from loss, misuse, unauthorized access, disclosure, alteration and untimely destruction. We do not grant access to personal information about you except as otherwise set forth herein. We do not share or sell personal information collected on the site with any third parties for their own marketing purposes.
                    At times, we will provide you with links to other websites. We encourage our users to be aware when they leave our site, and to read the privacy statements of every website that collects personally identifiable information.
                </p>
                <br>
                <h4>Information Collection and Use</h4>
                <div class="section-spacer-10"></div>
                <h4>What information is collected</h4>
                <p>We limit the collection of personal information to the information that we need to administer and improve the Site, to provide our products and services (“Services”) to our customers, and to fulfill any legal and regulatory requirements.</p>
                <p>The categories of personal information that we collect may include, but are not limited to:</p>
                <ul>
                    <li>Contact information to allow us to communicate with you</li>
                    <li>Employer information, including financial and bank account information, to provide the Services</li>
                    <li>Employee information, including social security number, date of birth, financial, bank account, biometric, geolocation, medical and beneficiary information, to provide the Services</li>
                    <li>Credit, debit, or cash/payment card information if used, such as for billing</li>
                    <li>Credit or debt history regarding your creditworthiness or credit history</li>
                    <li>Employment history and application information that can be used to determine eligibility for a job opening via our recruiting module</li>
                </ul>
                <div class="section-spacer-10"></div>
                <h4>How personal information is collected</h4>
                <p>We do not require you to provide any personal information in order to have general access to the Site. However, in order to access or use certain information, features or Services at the Site, you may be required to provide personal information. Personal information is primarily collected:</p>
                <ul>
                    <li>When you utilize the Services, we obtain from you the information we need to provide the Services.</li>
                    <li>From applications, forms and other information you provide us on the Site</li>
                    <li>When you establish an account, or an account is established for you at the direction of your employer, to receive Services</li>
                    <li>From survey information and/or site registration</li>
                    <li>If you provide us with comments or suggestions, request information about our Services, or contact our customer service department via phone, email or other forms of communication</li>
                    <li>From consumer and business reporting agencies regarding your creditworthiness or credit history</li>
                    <li>From third parties to verify information given to us</li>
                    <li>From information you may provide via Social Media.</li>
                </ul>
                <div class="section-spacer-10"></div>
                <h4>How personal information is used</h4>
                <p>We use the information provided on the Site to perform the Services you request. We limit the collection of personal customer information that we need to:</p>
                <ul>
                    <li>Facilitate customer requested Services, transactions, investments, distributions and benefits</li>
                    <li>Provide superior service to our customers</li>
                    <li>Comply with legal, reporting and regulatory requirements</li>
                    <li>Administer and improve our websites</li>
                    <li>Detect fraud or theft to protect our business and client information</li>
                    <li>Contact you with information on Services, new Services or products, or upcoming events</li>
                    <li>Facilitate applicant tracking and recruitment</li>
                </ul>
                <div class="section-spacer-10"></div>
                <h4>How aggregated, non-personal information is used</h4>
                <p>We may collect general, non-personal, statistical information about the users of the Site and our services in order to determine information regarding the use of our Site and general information about our customers. We may also group this information to provide general aggregated data. The aggregated data will not personally identify any customers or visitors to the Site.</p>
                <div class="section-spacer-10"></div>
                <h4>How cookies are used</h4>
                <p>A “cookie” is a piece of data that our Site may provide to your browser while you are at our Site. The information stored in a cookie is used for user convenience purposes, such as reducing repetitive messages, tracking helper tool versions, and retaining user display preferences. If a user rejects the cookie, they will be able to browse the Site but will be unable to use our online application.</p>
                <p>National PEO may use third-party service providers to use cookies, web beacons, and similar technologies to collect or receive information from our website and elsewhere on the internet and use that information to provide measurement services and target ads. You can opt-out of this information tracking using a web browser that supports Do Not Track functionality.</p>
                <div class="section-spacer-10"></div>
                <h4>Children under 13 years of age</h4>
                <p>This site is not intended for children under 13 years of age. We do not knowingly collect personal information from children under 13 years of age. All dependent data needed for benefits enrollment is customarily provided by the employee/guardian and kept secure as indicated in this Statement.</p>
                <div class="section-spacer-10"></div>
                <h4>Your California privacy rights</h4>
                <p>Under California Civil Code 1798, California residents with an established business relationship can request information about sharing their personal information with third parties for the third parties’ direct marketing purposes. If you are a California resident and would like more information, please contact your service provider.</p>
                <div class="section-spacer-10"></div>
                <h4>Parties With Whom Information May Be Shared</h4>
                <p>Information is shared to facilitate the Services needed in order to properly and efficiently handle duties related to your account. We may share information with:</p>
                <ul>
                    <li>Government agencies to fulfill legal, reporting and regulatory requirements</li>
                    <li>Attorneys, accountants and auditors</li>
                    <li>Credit reporting agencies to supply vendor references on client’s behalf</li>
                    <li>Our employees, affiliated companies, subsidiaries, agents and third party service vendors to perform Services related to your account, to offer additional Services, perform analysis to determine qualification to receive future services or collect amounts due.</li>
                    <li>Banking and brokerage firms to complete payroll processing and securities transactions</li>
                    <li>Credit bureaus and similar organizations, law enforcement or government officials. We reserve the right to release information if we are required to do so by law or if, in our business judgment, such disclosure is reasonably necessary to comply with legal process, in a fraud investigation, an audit or examination.</li>
                </ul>
                <div class="section-spacer-10"></div><h4>How to Access and Correct Your Information</h4>
                <p>Keeping your information accurate and up-to-date is very important. You can review or correct your account information by contacting a customer service representative. If you have an account at the Site, you can make changes to your account information after you login to the Site from your PC or wireless device and using the online tools. Note that some information changes may be done by or have to be done through your employer.</p>
                <div class="section-spacer-10"></div>
                <h4>Changes to This Privacy Statement</h4>
                <p>This statement may be revised from time to time due to legislative changes, changes in technology or our privacy practices or new uses of customer information not previously disclosed in this Statement. Revisions are effective upon posting and your continued use of this Site will indicate your acceptance of those changes. Please refer to this Statement regularly.</p>
                <p>If you have any comments, concerns or questions about this Privacy Statement, please contact your service provider.</p>
                <p>Effective date: March 21, 2019</p>
            </div>
        </div>
    </div>
</section>