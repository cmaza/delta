<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-Industry-Solutions-Health-Care.png">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Health Care</h1>
            <span>Industry Solutions</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-HR-Solutions.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>HR Solutions for Health Care</h4>
                <div class="inside-spacer"></div>
                <p>The vital work of health care comes with complicated administrative and regulatory responsibilities. Labor and safety regulations evolve constantly, and the 24-hour need for patient care means that traditional HR practices can be insufficient for health care organizations. Outsourcing HR services minimizes the administrative burden on health care providers so you can focus on what really matters: patient care.</p>
                <p>National PEO collaborates with organizations in the health care industry to develop human resources, benefits administration, and risk management services that help our clients focus on helping their patients live full, healthy lives. We are honored to handle HR responsibilities for organizations like Kids Kare Pediatrics and Family Care so that they can devote their energy and resources to taking care of their patients.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Unique Solutions Developed for You</h4>
                    <div class="inside-spacer"></div>
                    <p>Health care organizations have little in common with a traditional office environment, and our custom HR solutions are designed to meet your unique, complicated needs. Our partners in the health care industry depend on our dedicated team to foster safe work spaces, develop and administer excellent benefits packages, and manage employee time and attendance efficiently.</p>
                    <p>In particular, we specialize in developing the following services for our health care partners:</p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Unique-Solutions.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-4">
                <h4>Human Resources</h4>
                <p class="m-t-20">National PEO knows that working in health care is not a 9-to-5 job. Our HR outsourcing team provides time and attendance management services that ensure you will have enough hands on deck to meet your patients’ needs -- and that you comply with employee scheduling regulations.</p>
            </div>
            <div class="col-lg-4">
                <h4>Benefits Administration</h4>
                <p class="m-t-20">Health care organizations strive to take care of their employees as well as their employees take care of patients, and benefits administration is key to supporting health care staff. National PEO’s benefits consultants can handle the insurance, time off, and retirement benefits that support your health care team.</p>
            </div>
            <div class="col-lg-4">
                <h4>Risk Management</h4>
                <p class="m-t-20">Fostering a safe health care workplace is vital for your employees as well as your patients. Our auditing programs help you create a healthy patient care environment by ensuring your compliance with safety requirements and industry-specific labor regulations.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With National PEO?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
