<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-About-Us.png">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">About National PEO</h1>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-About-Us-Building.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>The Leading Provider of PEO Services</h4>
                    <p>Founded in 1999, National PEO has risen to become a leading provider of PEO services to hundreds of companies in Arizona and the rest of the United States. With roots deeply embedded in the PEO industry, National PEO has consistently led the curve in innovation, leadership and setting the trend for PEO “Best Practices”.</p>
                    <p>As we have witnessed other PEO’s come and go, our philosophy has been repeatedly validated. With the following principles guiding our direction, National PEO has enjoyed steady growth, excellent client retention and most importantly, extreme customer satisfaction!</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Interested in Starting a Career With Us?</h4>
                <p class="m-t-30 text-center">
                    <a href="<?php echo basePathUrl();?>career-opportunities/" class="btn btn-rounded btn-light">Careers</a>
                </p>
            </div>
        </div>
        <div class="section-spacer-10"></div>
    </div>
</section>