<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-COVID19.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Coronavirus (COVID-19) Resources</h1>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img alt=" " src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="heading-text heading-section text-center">
                <h2>National PEO Coronavirus (COVID-19) Updates</h2>
                </div>
                <p class="m-t-30 text-center">
                        <p>As an industry leader and trusted HR advisor, National PEO judges it is vital to inform our clients of our ongoing efforts to remain resilient in times of global crises such as the Coronavirus (COVID-19) pandemic.</p>
                        <p>The health and well-being of our clients, employees, and family members continue to be our top priority. National PEO is staying up to date on COVID-19 changes on local, state, and national levels to ensure our colleagues, clients, and family are well informed, provided all appropriate resources available, and receiving the medical attention and care they need. National PEO continues our proactive efforts to prepare our extended National PEO family for worst-case scenarios, as well as maintain a safe work environment to prolong our business ventures.</p>
                        <p>Preparing a defense, encouraging prevention, and maintaining wellbeing is vital to business operations. We have created valuable resources to assist our clients, employees, and family members to protect themselves from infection, prevent the spread of illness, and ensure unconditional support is provided to them during these trying times.</p>
                        <p>Please bookmark this page and continue to visit for updates and recommendations.</p>
                </p>
            </div>
        </div>
    <div class="section-spacer-40"></div>
        <div class="row">
            <div class="col-lg-9">
				<div class="thin-border">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>National PEO Prevention Recommendations for Coronavirus (COVID-19)</h3>
                        <p>We recommend that client employees take steps to reduce the transmission of communicable diseases in the work environment. Employees are recommended to:</p>
                        <ul class="list-icon dots list-icon-list list-icon-colored-grey">
                            <li><a href="https://www.cdc.gov/handwashing/" target="_blank"><font color="#e05206">Wash your hands frequently with warm, soapy water for at least 20 seconds.</font></a></li>
                            <li>Cover your mouth with tissues whenever you sneeze or cough, and discard used tissues in the trash</li>
                            <li>Avoid contact with people who are sick</li>
                            <li>Clean and disinfect frequently touched surfaces </li>
                        </ul>
                <div class="section-spacer-20"></div>
                   </div>
                   <div class="col-lg-11">
                        <h3>Coronavirus Aid, Relief, and Economic Security Act (CARES Act) Information</h3>
                        <br>
                        <div class="row text-center">
            			 <div class="col-lg-4">
                			<a href="<?php echo basePathUrl();?>PDFs/uwc-summary-of-ui-provisions-in-the-cares-act.pdf" target="_blank">
                        	<img alt="Download" src="<?php echo basePathUrl();?>images/UWC-Summary-Thumbnail.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="<?php echo basePathUrl();?>PDFs/uwc-summary-of-ui-provisions-in-the-cares-act.pdf"  target="_blank" class="btn btn-rounded btn-light">Download UWC Summary of UI Provisions</a>
                        	<div class="section-spacer-30"></div>
            			 </div>
            			 <div class="col-lg-4">
                			<a href="<?php echo basePathUrl();?>PDFs/CARES UI Resource 4.17.20.pdf" target="_blank">
                        	<img alt="Download" src="<?php echo basePathUrl();?>images/CARES-UI-Thumbnail.jpg"></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="<?php echo basePathUrl();?>PDFs/CARES UI Resource 4.17.20.pdf"  target="_blank" class="btn btn-rounded btn-light">Download CARES UI Resources</a>
                        	<div class="section-spacer-30"></div>
            		  	 </div>
        				</div>
                    </div>
                   <div class="col-lg-11">
                        <h3>Telemedicine Services by MDLIVE</h3>
                        <p>We now offer <a href="https://www.vensure.com/telemedicine"  target="_blank"><font color="#e05206">Telemedicine Services</font></a> by MDLIVE provided by Vensure Employer Services free of charge for the next 90 days. Program begins April 1, 2020 and ends June 30, 2020.</p>
                <div class="section-spacer-20"></div>
                   </div>
                   <div class="col-lg-11">
                        <h3>Paycheck Protection Program</h3>
                        <p>The Paycheck Protection Program prioritizes millions of Americans employed by small businesses by authorizing up to $349 billion toward job retention and certain other expenses. Learn more <a href="https://home.treasury.gov/policy-issues/top-priorities/cares-act/assistance-for-small-businesses"  target="_blank"><font color="#e05206">here</font></a>. Payroll Protection Program <a href="https://home.treasury.gov/system/files/136/Paycheck-Protection-Program-Application-3-30-2020-v3.pdf"  target="_blank"><font color="#e05206">application. </font></a>Find Loan Lender <a href="https://www.sba.gov/paycheckprotection/find"  target="_blank"><font color="#e05206">here</font></a>. IRS clarifies businesses’ options for SS tax deferral until your Paycheck Protection Program loan is forgiven. <a href="https://www.irs.gov/newsroom/deferral-of-employment-tax-deposits-and-payments-through-december-31-2020"  target="_blank"><font color="#e05206">IRS FAQ.</font></a></p>
                        <div class="section-spacer-20"></div>
            			 <div class="col-lg-4 text-center">
                			<a href="https://napeo.blob.core.windows.net/cdn/docs/default-source/covid-19/letter-for-ppp-loans.pdf?sfvrsn=425929d4_6" target="_blank">
                        	<img alt="NAPEO PPP Leter" src="<?php echo basePathUrl();?>images/NAPEO-Letter-Thumbnail.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="https://napeo.blob.core.windows.net/cdn/docs/default-source/covid-19/letter-for-ppp-loans.pdf?sfvrsn=425929d4_6"  target="_blank" class="btn btn-rounded btn-light">Download PEO PPP Explanation Letter from NAPEO</a>
                        	<div class="section-spacer-20"></div>
                   </div>
                <div class="section-spacer-20"></div>
                        <h3>Economic Injury Disaster Loan Program</h3>
                        <p>Small business owners in the following designated states are currently eligible to apply for a low-interest loan due to Coronavirus (COVID-19): Arizona, California, Colorado, Connecticut, Delaware, the District of Columbia, Florida, Georgia, Illinois, Indiana, Louisiana, Maine, Maryland, Massachusetts, Michigan, Montana, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina, Ohio, Pennsylvania, Rhode Island, South Carolina, Tennessee, Utah, Virginia, Washington, and West Virginia. <a href="https://disasterloan.sba.gov/ela/" target="_blank"><font color="#e05206">Learn more here</font></a> or <a href="https://covid19relief.sba.gov/#/" target="_blank"><font color="#e05206">click here to apply</font>.</a></p>
                        	<div class="section-spacer-20"></div>
            			 <div class="col-lg-4 text-center">
                			<a href="<?php echo basePathUrl();?>PDFs/corona_virus_smallbiz_loan_final_revised.pdf" target="_blank">
                        	<img alt="Small Business Emergency Loan Guide" src="<?php echo basePathUrl();?>images/Emergency-Loan-Thumbnail.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="<?php echo basePathUrl();?>PDFs/corona_virus_smallbiz_loan_final_revised.pdf"  target="_blank" class="btn btn-rounded btn-light">Download Small Business Emergency Loan Guide</a>
                        	<div class="section-spacer-30"></div>
            			
                <div class="section-spacer-20"></div>
                   </div>
                    <div class="col-lg-12">
                        <h3>Coronavirus (COVID-19) Employer Resources</h3>
                        <br>
                        <div class="row text-center">
            			 <div class="col-lg-4">
                			<a href="<?php echo basePathUrl();?>PDFs/Vensure Coronavirus FAQ hk 0317.pdf" target="_blank">
                        	<img alt="COVID-19 FAQ Download" src="<?php echo basePathUrl();?>images/NPEO-COVID-19-FAQ.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="<?php echo basePathUrl();?>PDFs/Vensure Coronavirus FAQ hk 0317.pdf"  target="_blank" class="btn btn-rounded btn-light">Download Coronavirus (COVID-19) FAQ</a>
                        	<div class="section-spacer-30"></div>
            			 </div>
            			 <div class="col-lg-4">
                			<a href="<?php echo basePathUrl();?>PDFs/Employee Work From Home Guide 20200313.pdf" target="_blank">
                        	<img alt="VensureHR Work from Home Guide" src="<?php echo basePathUrl();?>images/NPEO-Employee-Work-From-Home-Thumbnail.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="<?php echo basePathUrl();?>PDFs/Employee Work From Home Guide 20200313.pdf"  target="_blank" class="btn btn-rounded btn-light">Download Work From Home Guide</a>
                        	<div class="section-spacer-30"></div>
            		  	 </div>
            			 <div class="col-lg-4">
                			<a href="<?php echo basePathUrl();?>PDFs/Vensure - HR Compliance Bulletin - Coronavirus and the Workplace 20200313.pdf" target="_blank">
                        	<img alt="VensureHR HR Compliance Bulletin" src="<?php echo basePathUrl();?>images/NPEO-HR-Compliance-Coronavirus-Thumbnail.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                       		<a href="<?php echo basePathUrl();?>PDFs/Vensure - HR Compliance Bulletin - Coronavirus and the Workplace 20200313.pdf"  target="_blank" class="btn btn-rounded btn-light">Download HR Compliance Bulletin</a>
                       		<div class="section-spacer-30"></div>
            			 </div>
        				</div>
                    </div>
                    <div class="col-lg-12">
                        <div class="row text-center">
            			 <div class="col-lg-4">
            			 <div class="section-spacer-15"></div>
                			<a href="<?php echo basePathUrl();?>PDFs/VHR_Families_First_Coronavirus_Response_Act_HR_6201_20200320.pdf" target="_blank">
                        	<img alt="COVID-19 FAQ Download" src="<?php echo basePathUrl();?>images/NPEO-Family-First-Thumbnail.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="<?php echo basePathUrl();?>PDFs/VHR_Families_First_Coronavirus_Response_Act_HR_6201_20200320.pdf"  target="_blank" class="btn btn-rounded btn-light">Download Families First Coronavirus Response</a>
                        	<div class="section-spacer-30"></div>
            			 </div>
            			 <div class="col-lg-4">
                			<a href="<?php echo basePathUrl();?>PDFs/FFCRA_Poster_WH1422_Non-Federal.pdf" target="_blank">
                        	<img alt="VensureHR HR Compliance Bulletin" src="<?php echo basePathUrl();?>images/FFCRA-Thumbnail.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                       		<a href="<?php echo basePathUrl();?>PDFs/FFCRA_Poster_WH1422_Non-Federal.pdf"  target="_blank" class="btn btn-rounded btn-light">Download Families First Coronavirus Response Act Infographic</a>
                       		<div class="section-spacer-30"></div>
                       		</div>
        				</div>
                    </div>
                    <div class="col-lg-12">
                        <h3>Families First Coronavirus Response Act - H.R. 6201 Information</h3>
                         <br>
                        <div class="row text-center">
            			 <div class="col-lg-4">
                			<a href="<?php echo basePathUrl();?>PDFs/FULL_FINAL_BILL.pdf" target="_blank">
                        	<img alt="COVID-19 FAQ Download" src="<?php echo basePathUrl();?>images/FULL_FINAL_BILL_Thumbnail.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="<?php echo basePathUrl();?>PDFs/FULL_FINAL_BILL.pdf"  target="_blank" class="btn btn-rounded btn-light">Download H.R. 6201 Bill</a>
                        	<div class="section-spacer-30"></div>
            			 </div>
                            
            			 <div class="col-lg-9">
                			<p></a></p>
            			 </div>
        				</div>
                    </div>
                    </div>
                </div>
				</div>
            </div>
            <div class="col-lg-3">
                <div class="row text-center">
                    <div class="col-lg-12"></div>
                    <div class="section-spacer-5"></div>
                    <div class="row text-left">
                        <p><b>CDC Links</b></p>
                    </div>
                    <div class="row text-left">
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/CDC Fact Sheet on the Coronavirus.pdf" target="_blank"><b><font color="#e05206">Coronavirus Fact Sheet from the CDC</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/CDC Fact Sheet on the Coronavirus-Spanish.pdf" target="_blank"><b><font color="#e05206">Coronavirus Fact Sheet from the CDC (Espa&#241;ol)</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/COVID-19 information for Travelers _ CDC.pdf" target="_blank"><b><font color="#e05206">Coronavirus 2019 Info for Travel from the CDC</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/CDC handout COVID-19.pdf" target="_blank"><b><font color="#e05206">COVID-19 Handout from the CDC</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/COVID19-symptoms.pdf" target="_blank"><b><font color="#e05206">COVID-19 Symptoms from the CDC</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/hand-sanitizer-factsheet.pdf" target="_blank"><b><font color="#e05206">Hand Sanitizer Fact Sheet from the CDC</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/wash-your-hands-fact-sheet-508.pdf" target="_blank"><b><font color="#e05206">Handwashing Fact Sheet from the CDC</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/wash-your-hands-fact-sheet_esp-508.pdf" target="_blank"><b><font color="#e05206">Handwashing Fact Sheet from the CDC (Espa&#241;ol)</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/handwashing-poster.pdf" target="_blank"><b><font color="#e05206">Handwashing Information from the CDC</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/handwashing-poster-es.pdf" target="_blank"><b><font color="#e05206">Handwashing Information from the CDC (Espa&#241;ol)</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/family_handwashing-508.pdf" target="_blank"><b><font color="#e05206">Handwashing: Keeping Your Family Safe Fact Sheet from the CDC</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/family_handwashing_esp-508.pdf" target="_blank"><b><font color="#e05206">Handwashing: Keeping Your Family Safe Fact Sheet from the CDC (Espa&#241;ol)</font></b></a></h5>
                        <h5><a class="orangeText" href="https://www.cdc.gov/coronavirus/2019-ncov/community/disinfecting-building-facility.html?CDC_AA_refVal=https%3A%2F%2Fwww.cdc.gov%2Fcoronavirus%2F2019-ncov%2Fprepare%2Fdisinfecting-building-facility.html" target="_blank"><b><font color="#e05206">Proper Cleaning and Disenfecting from the CDC</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/CDC Fact Sheet on the Coronavirus-what to do if you are sick.pdf" target="_blank"><b><font color="#e05206">What to Do If You Are Sick CDC Fact Sheet</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/CDC Fact Sheet on the Coronavirus-what to do if you are sick-Spanish.pdf" target="_blank"><b><font color="#e05206">What to Do If You Are Sick CDC Fact Sheet (Espa&#241;ol)</font></b></a></h5>
                        <div class="section-spacer-20"></div>
                        </div>
                        
                        <div class="row text-left">
                        <p><b>State by State Unemployment Insurance Information</b></p>
                        </div>
                        <div class="row text-left">
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/Vensure_State_Unemployement_Insurance_Information_20200330.pdf" target="_blank"><b><font color="#e05206">Unemployment Insurance Information</font></b></a></h5>
                        <div class="section-spacer-30"></div>
                        </div>
                    
                    <div class="row text-left">
                        <p><b>Institutions Accepting Paycheck Protection Program Application From Non-Customers</b></p>
                        </div>
                        <div class="row text-left">
                        <ul style="list-style-type:none;">
                            <li><h5><a class="orangeText" href="https://www.amegybank.com/landing-pages/small-business-cares-act//?cid=aff-ABT_ch-Vnty_mnth-Apr_yr-20_cmpgn-CvdCrs" target="_blank"><b><font color="#e05206">Amegy Bank - Texas<br></font></b></a></h5></li>
                            <li><h5><a class="orangeText" href="https://www.calbanktrust.com/campaigns/sba-cares-act/" target="_blank"><b><font color="#e05206">California Bank & Trust<br></font></b></a></h5></li>
                            <li><h5><a class="orangeText" href="https://www.tcboregon.com/CARES/index.html" target="_blank"><b><font color="#e05206">Commerce Bank of Oregon<br></font></b></a></h5></li>
                            <li><h5><a class="orangeText" href="https://www.tcbwa.com/CARES/index.html" target="_blank"><b><font color="#e05206">Commerce Bank of Washington<br></font></b></a></h5></li>
                            <li><h5><a class="orangeText" href="https://www.nbarizona.com/small-business-cares-act/" target="_blank"><b><font color="#e05206">National Bank of Arizona<br></font></b></a></h5></li>
                            <li><h5><a class="orangeText" href="https://www.nsbank.com/small-business-cares-act/" target="_blank"><b><font color="#e05206">Nevada State Bank<br></font></b></a></h5></li>
                            <li><h5><a class="orangeText" href="https://www.vectrabank.com/offers/small-business-relief-loan.jsp" target="_blank"><b><font color="#e05206">Vectra Bank – Colorado<br></font></b></a></h5></li>
                            <li><h5><a class="orangeText" href="https://www.zionsbank.com/LandingPages/small-business-cares-act//?cid=aff-ZFNB_ch-Vnty_mnth-Apr_yr-20_cmpgn-CvdCrs" target="_blank"><b><font color="#e05206">Zions Bank</font></b></a></h5></li>
                            <div class="section-spacer-10"></div>
                        </ul>
                        </div>
                        
                        <div class="row text-left">
                        <p><b>Institutions Offering Loans to Larger Companies</b></p>
                        </div>
                        <div class="row text-left">
                        <ul style="list-style-type:none;">
                            <li><h5><a class="orangeText" href="https://www.federalreserve.gov/newsevents/pressreleases/files/monetary20200409a7.pdf" target="_blank"><b><font color="#e05206">MainStreet New Loan Facility</font></b></a></h5></li>
                            <li><h5><a class="orangeText" href="https://www.federalreserve.gov/newsevents/pressreleases/files/monetary20200409a4.pdf" target="_blank"><b><font color="#e05206">MainStreet Expanded Loan Facility</font></b></a></h5></li>
                            <div class="section-spacer-10"></div>
                        </ul>
                        </div>
                    
                        <div class="row text-left">
                        <p><b>California Employment Development Department Links</b></p>
                        </div>
                        <div class="row text-left">
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/California EDD -Coronavirus 2019 (COVID-19).pdf" target="_blank"><b><font color="#e05206">Coronavirus 2019 Fact Sheet from CA EDD</font></b></a></h5>
                        <h5><a class="orangeText" href="https://edd.ca.gov/about_edd/coronavirus-2019-espanol.htm" target="_blank"><b><font color="#e05206">Coronavirus 2019 Fact Sheet from CA EDD (Espa&#241;ol)</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/COVID_19_-California_EDD_Work_Sharing_Program.pdf" target="_blank"><b><font color="#e05206">California EDD Work Sharing Program</font></b></a></h5>
                        <div class="section-spacer-30"></div>
                        </div>
                        
                        <div class="row text-left">
                        <p><b>CA Department of Industrial Relations</b></p>
                        </div>
                        <div class="row text-left">
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/Coronavirus Disease (COVID-19) – FAQs on laws enforced California DIR.pdf" target="_blank"><b><font color="#e05206">FAQs on Laws Enforced by the California Labor Commissioner's Office from the DIR</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/Cal OSHA - Interim Guidance for Protecting Health Care Workers (COVID-19).pdf" target="_blank"><b><font color="#e05206">Interim Guidance for Protecting Health Care Workers from Exposure Fact Sheet from the DIR</font></b></a></h5>
                        <div class="section-spacer-20"></div>
                        </div>
                        
                        <div class="row text-left">
                        <p><b>Human Resources</b></p>
                        </div>
                        <div class="row text-left">
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/HIPAA Privacy and the Coronavirus.pdf" target="_blank"><b><font color="#e05206">HIPPA Privacy and the Coronavirus</font></b></a></h5>
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/EEOC resource-Pandemic Preparedness in the Workplace and the Americans with Disabilities Act.pdf" target="_blank"><b><font color="#e05206">Preparedness in the Workplace and the ADA</font></b></a></h5>
                        <div class="section-spacer-30"></div>
                        </div>
                        
                        <div class="row text-left">
                        <p><b>Resources from Seyfarth</b></p>
                        </div>
                        <div class="row text-left">
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/COVID-19_Immigration_Planning_for_US_Employers___Seyfarth_Shaw_LLP.pdf" target="_blank"><b><font color="#e05206">Immigration Planning for US Employers</font></b></a></h5>
                        <div class="section-spacer-30"></div>
                        </div>
                        
                        <div class="row text-left">
                        <p><b>Resources from Belden Blaine Raytis</b></p>
                        </div>
                        <div class="row text-left">
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/Belden_Blaine_Raytis_Law_Firm_-_Coronavirus_and_Employment_Guidebook.pdf" target="_blank"><b><font color="#e05206">Coronavirus and Employment Guidebook</font></b></a></h5>
                        <div class="section-spacer-30"></div>
                        </div>
                        
                        <div class="row text-left">
                        <p><b>Resources from Fischer Phillips</b></p>
                        </div>
                        <div class="row text-left">
                        <h5><a class="orangeText" href="<?php echo basePathUrl();?>PDFs/Fisher Phillips State by State COVID-19 Chart - updated 3.23.20.xlsx" target="_blank"><b><font color="#e05206">State by State COVID-19 Fact Chart</font></b></a></h5>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>