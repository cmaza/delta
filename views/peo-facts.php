<style>
    h5 {
  text-align: right;
}
</style><section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-Facts.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">PEO Facts</h1>
            <span></span>
        </div>
    </div>
</section>

<section>
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Facts-1.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-3">
                <h4>Stay in Business</h4>
                <p>PEO clients grow seven to nine percent faster, have 10 to 14 percent lower rates of employee turnover, and are 50 percent less likely to go out of business than other comparable small businesses.</p>
            </div>
            <div class="col-lg-3">
                <h4>More HR Services</h4>
                <p>PEOs provide access to a broader array of HR services at a cost that is almost $450 lower per employee.</p>
            </div>
            <div class="col-lg-3">
                <h4>Attract and Retain</h4>
                <p class="m-t-20">PEO clients are more likely to provide employer-sponsored retirement plans and other employee benefits, which helps them attract and retain their employees.</p>
            </div>
            <div class="col-lg-3">
                <h4>Fast Growing</h4>
                <p>“The PEO industry is known as one of the fastest growing business services in the United States. Annually, the industry generates $42 billion in gross revenues.”</p>
                <h5>-NAPEO</h5>
            </div>
        </div>
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-3">
                <h4>Value in Services</h4>
                <p>“The average client company has approximately 16 worksite employees. This number is increasing however, as larger corporations are finding value in the services of a PEO.”</p>
                <h5>-NAPEO</h5>
            </div>
            <div class="col-lg-3">
                <h4>86% Members</h4>
                <p>“Nearly 86% of companies that join a NAPEO member”</p>
                <h5>-NAPEO</h5>
            </div>
            <div class="col-lg-3">
                <h4>Stay Over a Year</h4>
                <p>“PEO, stay with the PEO for a year or more.”</p>
                <h5>-NAPEO</h5>
            </div>
            <div class="col-lg-3">
                <h4>Retain 89% of Clients</h4>
                <p>“PEO’s retain approximately 89% of their clients from year to year, proving that businesses who use PEO services truly appreciate the benefits of the co-employment relationship.”</p>
                <h5>-NAPEO</h5>
            </div>
        </div>
        <div class="section-spacer-50"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Request a Free HR Diagnostic</h4>
                <p class="m-t-60 text-center">
                    <a href="#modalBecomeAPartnerToday" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
