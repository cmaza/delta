<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-Industry-Solutions-Tech.png">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Tech</h1>
            <span>Industry Solutions</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Tech-HR-Support.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>HR Support for Tech Innovation</h4>
                <div class="inside-spacer"></div>
                <p>Tech organizations compete in a global marketplace where technical skill, hard work, and creative thinking are necessary to keep up with constant change and growth. Administrative responsibilities can draw you away from developing innovative tech products and solutions, but HR outsourcing takes care of these tasks so you can focus on growing your business.</p>
                <p>National PEO develops sophisticated solutions for tech organizations in benefits administration, human resources, and employee portals and perks.  These services allow our partner organizations like Digital Current to not only streamline their administrative responsibilities, but also find, hire, and retain the most creative, skilled talent in the tech industry.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Unique Solutions Developed for You</h4>
                    <div class="inside-spacer"></div>
                    <p>Tech organizations know that hiring and retaining skilled, creative employees is vital to fostering innovation and excelling in a competitive industry. We support tech organizations’ efforts to develop long-term employee relationships through high-quality benefits administration, streamlined employee management, and Hire to Retire programs.</p>
                    <p>Our specialized HR services for tech organizations include:</p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Tech-HR-Unique-Solutionst.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-4">
                <h4>Benefits Administration</h4>
                <p class="m-t-20">National PEO’s comprehensive human resources outsourcing helps you create safe, healthy restaurants and hire enthusiastic applicants who have the exact skills you need. Our HR support doesn’t just minimize your day-to-day administrative burden -- it helps you build a successful team with a low rate of turnover so you can focus on growing your business.</p>
            </div>
            <div class="col-lg-4">
                <h4>Human Resources</h4>
                <p class="m-t-20">National PEO’s electronic onboarding solutions minimize the complicated administrative work that comes with adding new team members so you can focus on training them to provide your customers with safe, high-quality dining experiences.</p>
            </div>
            <div class="col-lg-4">
                <h4>Employee Portals and Perks</h4>
                <p class="m-t-20">Payroll is tedious, and it can be even more complicated for businesses with tipped employees. National PEO will take care of deposits, deductions, and data tracking to ensure that your restaurant employees are always compensated correctly and on time.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With National PEO?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
