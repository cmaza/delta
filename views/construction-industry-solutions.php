<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-Industry-Solutions-Construction.png">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Construction</h1>
            <span>Industry Solutions</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Construction-Safety.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Ensuring Safety and Efficiency</h4>
                <div class="inside-spacer"></div>
                <p>Construction is one of the most high-pressure industries in today’s business market. The desire to finish important projects on deadline and the need to foster the safest work environment possible can put significant stress on your construction business. Outsourcing HR responsibilities helps relieve additional administrative pressures so you can focus on creating a safe, cost- and time-efficient construction business that your employees and clients will trust.</p>
                <p>National PEO collaborates with construction businesses to provide services in workers’ compensation, time and attendance management, and OSHA compliance to foster a safe, effective work environment. We understand that safety and efficiency are vital to your construction organization, and we are excited to help you keep overhead costs low so you can meet your business growth goals.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Unique Solutions Developed for You</h4>
                    <div class="inside-spacer"></div>
                    <p>National PEO understands the dangerous, deadline-driven nature of construction work. Our partners in the construction industry trust us to minimize administrative burdens and maintain cost efficiency with labor law compliance, general ledger integration, and payroll tax filing services that keep your business running safely and efficiently.</p>
                    <p>In particular, we specialize in providing the following services for our construction clients:</p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Construction-Unique-Solutions.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-4">
                <h4>Workers’ Compensation</h4>
                <p class="m-t-20">Construction employees face dangers that other workers don’t, and workers’ compensation claims can be complicated and overwhelming for your business. National PEO will take care of your workers’ compensation policy and manage any claims that arise.</p>
            </div>
            <div class="col-lg-4">
                <h4>Time and Attendance Management</h4>
                <p class="m-t-20">The project-driven nature of construction work can complicate work schedules. Our HR outsourcing team helps you create and track employee schedules that help you finish projects efficiently while adhering to labor regulations.</p>
            </div>
            <div class="col-lg-4">
                <h4>OSHA Compliance</h4>
                <p class="m-t-20">National PEO supports your goal of providing the safest, healthiest working environment possible for your construction team. Our regulatory audits help you identify and resolve OSHA violations and other potential workplace safety hazards.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With National PEO?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
