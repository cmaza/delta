<section>
    <div class="container">
        <div class="section-spacer-30"></div>
        <div class="row text-center">
            <div class="col-lg-12">
                <h2>Thank You for Your Submission</h2>
                <h5 class="home-services"><a href="/">Return to Homepage</a></h5>
            </div>
        </div>
        <div class="section-spacer-30"></div>
    </div>
</section>

