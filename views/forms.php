<style>
    ul {
        padding-left: 17px;
        
    }
</style>
<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/NationalPEO-Forms.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Employee Resource Forms</h1>
            <span>Customized and Designed with Your Needs in Mind</span>
        </div>
    </div>
</section>

<section id="client-center">
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-6">
                <div class="container-fluid table-responsive-sm">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr class="row">
                            <th class="col-sm-8 text-center">Tax Forms</th>
                            <th class="col-sm-2 text-center">English</th>
                            <th class="col-sm-2 text-center">Espa&ntilde;ol</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Federal Withholding Forms</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>PDFs/fw4.pdf">
                                    <i class="far fa-file-pdf"></i>
                                </a>
                            </td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Employment Eligibility Verification</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>PDFs/I-9_2020.pdf">
                                    <i class="far fa-file-pdf"></i>
                                </a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>PDFs/I-9_spanish.pdf">
                                    <i class="far fa-file-pdf"></i>
                                </a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">State Withholding Forms</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="https://www.nationalpeo.com/peo-forms/state-withholding-forms/">
                                    <i class="far fa-file-pdf"></i>
                                </a>
                            </td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Taxpayer ID# & Cert. Request (W-9)</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>PDFs/fw9.pdf">
                                    <i class="far fa-file-pdf"></i>
                                </a>
                            </td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Duplicate W-2 Request</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>PDFs/W2_duplicate_request.pdf">
                                    <i class="far fa-file-pdf"></i>
                                </a>
                            </td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">View I-9 How To Video</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="https://www.nationalpeo.com/peo-forms/view-i-9-how-to-video/">
                                    <i class="far fa-file-pdf"></i>
                                </a>
                            </td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="section-spacer-10"></div>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr class="row">
                                <th class="col-sm-8 text-center">Bank</th>
                                <th class="col-sm-2 text-center">English</th>
                                <th class="col-sm-2 text-center">Espa&ntilde;ol</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Direct Deposit Authorization</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/NF2104%20Direct%20Deposit%20Enrollment%20Form.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/NF2104%20Direct%20Deposit%20Enrollment%20Form%20Spanish.pdf"><i class="far fa-file-pdf"></i></a></td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Automatic Payment Authorization (ACH)</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/NF2108%20Authorization%20for%20Automatic%20Payment.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Direct Deposit Cancellation/Suspension</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/NF2109%20Direct%20Deposit%20Cancellation%20Suspension.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Check Re-Issue Authorization</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/NF2110%20Check%20Re-Issue%20Authorization.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Pay Card Enrollment</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/NF2105%20BofA%20Money%20Network%20PayCard%20Enrollment.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/NF2105%20BofA%20Money%20Network%20PayCard%20Enrollment%20Spanish.pdf"><i class="far fa-file-pdf"></i></a></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="section-spacer-10"></div>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr class="row">
                            <th class="col-sm-8 text-center">Workers' Compensation</th>
                            <th class="col-sm-2 text-center">English</th>
                            <th class="col-sm-2 text-center">Espa&ntilde;ol</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Workers' Compensation Certificate Request</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/work_comp_cert_request.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Report of Injury Packet</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/report_of_injury_packet.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Program</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_program.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Program Checklist</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_initial_establishment_checklist.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Policy for Posting</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_accident_reporting_rtw_policy.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Documentation Checklist</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_documentation_checklist.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Sample Letter to Physician</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_letter_to_physician.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Job Demands</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_job_demands.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Release</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_medical_release_to_return_to_full_duty.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Doctor Recommendations</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_doc_rtw_recommendations.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Employee Medical Release of Information</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_med_release_of_info.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Sample Letter to Employee: “Termination of Employment”</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_expiration_of_modified_duty_terminate_letter.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Return to Work Sample Letter to Employee: “Notification of Transitional Work Assignment”</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/rtw_notification_trans_work_assignment.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="container-fluid table-responsive-sm">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr class="row">
                            <th class="col-sm-8 text-center">Human Resource</th>
                            <th class="col-sm-2 text-center">English</th>
                            <th class="col-sm-2 text-center">Espa&ntilde;ol</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="row">
                            <td class="col-sm-8 text-center">AZ New Hire Packet</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>PDFs/2020_New_Hire_English.pdf"><i class="far fa-file-pdf"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>PDFs/2020_New_Hire_Spanish.pdf"><i class="far fa-file-pdf"></i></a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">New Hire Packet</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>PDFs/2020_New_Hire_English_no_a4.pdf"><i class="far fa-file-pdf"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>PDFs/2020_New_Hire_Spanish_no_a4.pdf"><i class="far fa-file-pdf"></i></a>
                            </td>
                        </tr>
                       <tr class="row">
                            <td class="col-sm-8 text-center">AZ New Hire Packet w/Tax Credit Forms</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/2020_New_Hire_English_tax_credit.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/2020_New_Hire_Spanish_tax_credit.pdf"><i class="far fa-file-pdf"></i></a></td>
                        </tr>
                       <tr class="row">
                            <td class="col-sm-8 text-center">Disciplinary Form</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/ee_discipline_notice.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Review Form</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/ee_performance_review.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                       <tr class="row">
                            <td class="col-sm-8 text-center">New Hire Packet w/Tax Credit Forms</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/2020_New_Hire_English_tax_credit_no_a4.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/2020_New_Hire_Spanish_tax_credit_no_a4.pdf"><i class="far fa-file-pdf"></i></a></td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Employee Change Form</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/personnel_action_form.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Employee Termination Form</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/term_form.pdf"><i class="far fa-file-pdf"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="section-spacer-10"></div>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr class="row">
                            <th class="col-sm-8 text-center">Other</th>
                            <th class="col-sm-2 text-center">English</th>
                            <th class="col-sm-2 text-center">Espa&ntilde;ol</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Paycard Enrollment</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/paycard_english-1.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Web Access Enrollment</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/online_payroll_enroll.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Paid Time Off Enrollment</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/pto_enrollment.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Delivery Instructions Form</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/delivery_instructions.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Payroll Deduction Form</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/payroll_deductions.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Colorado Affirmation of Legal Work Status Form</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/colorado_affirmation_of_legal_work_status.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Arizona Child Support Questionnaire</td>
                            <td class="col-sm-2 text-center"><a target="_blank" href="<?php echo basePathUrl();?>PDFs/child_support_questionnaire.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a></td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</section>