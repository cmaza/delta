<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-Industry-Solutions-Manufacturing.png">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Manufacturing</h1>
            <span>Industry Solutions</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Manufacturing-Sophisticated-Solutions.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Sophisticated Solutions for Manufacturing Businesses</h4>
                <div class="inside-spacer"></div>
                <p>Though the manufacturing industry has undergone massive technological changes, safety and efficiency remain top priorities for manufacturing business owners. HR outsourcing can help you recruit and retain skilled employees and foster a safe, efficient work environment in which they can help your manufacturing business grow and thrive.</p>
                <p>National PEO partners with manufacturing businesses to provide safety management expertise, benefits administration, and sophisticated HRIS programs that help you retain skilled employees and create safe, streamlined work environments. Our HR outsourcing ensures safety and tackles day-to-day administrative tasks so you can focus on expanding your business.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Unique Solutions Developed for You</h4>
                    <div class="inside-spacer"></div>
                    <p>Safety and efficiency are key to growing a successful manufacturing business, and National PEO supports your organization by taking care of HR responsibilities. Manufacturing organizations rely on our labor law compliance, safety compliance, and automated payroll services to meet the complex, constantly evolving needs of their industry while minimizing overhead costs and reducing employee turnover.</p>
                    <p>In particular, we specialize in developing the following services for our manufacturing partners:</p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Manufacturing-Unique-Solutions.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-4">
                <h4>Safety Management</h4>
                <p class="m-t-20">National PEO understands that safe, healthy work environment is the foundation on which a successful manufacturing business is built. We support your organization with safety and compliance audits that identify and resolve potential workplace hazards.</p>
            </div>
            <div class="col-lg-4">
                <h4>Benefits Administration</h4>
                <p class="m-t-20">Insurance, time off, and retirement benefits are vital to supporting and retaining your skilled manufacturing employees. National PEO takes care of the tedious paperwork and regulatory responsibilities that come with employee benefits so you can focus on streamlining and growing your business.</p>
            </div>
            <div class="col-lg-4">
                <h4>HRIS Services</h4>
                <p class="m-t-20">National PEO specializes in creating user-friendly HRIS systems that simplify your hiring, onboarding, and management processes and meet the complicated regulatory and scheduling needs of the manufacturing industry.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With National PEO?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
