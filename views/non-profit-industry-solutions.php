<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-Industry-Solutions-Non-Profit.png">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Non-Profit</h1>
            <span>Industry Solutions</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Helping-Others.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Helping You Help Others</h4>
                <div class="inside-spacer"></div>
                <p>The non profit organizations that serve our communities vary widely in their goals, resources, and budgets, but they all share a desire to focus their time and energy on community outreach. Day-to-day administrative tasks may draw your attention and resources away from your mission, but HR outsourcing can help you ensure your employees have a safe, healthy, supportive workplace with less cost and effort.</p>
                <p>National PEO partners with non profits to provide human resources, payroll, and benefits services that help you reduce overhead costs and retain devoted employees. By expending less time and energy on day-to-day administrative tasks, you get to focus on the community-oriented work you love. We enthusiastically support organizations like Arizona Animal Welfare League with our streamlined, cost-effective HR outsourcing.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Unique Solutions Developed for You</h4>
                    <div class="inside-spacer"></div>
                    <p>National PEO knows that non-profit resources can be limited, and minimizing overhead costs and streamlining administrative tasks is vital to helping you meet your community service goals. By providing labor law compliance services, benefits administration, and efficient HRIS systems, we empower non-profits to direct more energy and resources toward serving the needs of their communities.</p>
                    <p>In particular, we specialize in providing the following services for our non-profit clients:</p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Non-Profit-Unique-Solutions.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-4">
                <h4>Human Resources</h4>
                <p class="m-t-20">National PEO’s first priority in our HR outsourcing services is helping you save money, time, and energy. We specialize in creating user-friendly HRIS systems that simplify your hiring, onboarding, and management processes and reduce your paperwork burden.</p>
            </div>
            <div class="col-lg-4">
                <h4>Payroll</h4>
                <p class="m-t-20">National PEO’s automated payroll services make your employee compensation system more efficient by streamlining payments, deductions, and time-off tracking so that you can have the peace of mind that comes with knowing that your staff will be paid correctly and on time.</p>
            </div>
            <div class="col-lg-4">
                <h4>Benefits Administration</h4>
                <p class="m-t-20">Providing high-quality benefits is key to supporting your non-profit organization staff, but the work involved in enrolling employees can be time-consuming. National PEO can take care of all those paperwork and regulatory tasks for you.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With National PEO?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
