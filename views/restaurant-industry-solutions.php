<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-Industry-Solutions-Restaurant.png">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Restaurant</h1>
            <span>Industry Solutions</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Restaurant-Customized-Solutions.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Customized Restaurant Industry Solutions</h4>
                <div class="inside-spacer"></div>
                <p>The restaurant industry is a high pressure, customer-centered environment, and no two workdays or workplaces are the same. Outsourcing HR services takes the hassle of administrative tasks off restaurant owners’ plates so they can stay focused on creating quality dining experiences for their customers.</p>
                <p>When your restaurant partners with National PEO, we assign a hands-on representative who will understand the distinctive needs of your restaurant so that we can customize hiring, onboarding, and payroll processes that run smoothly and respond to changes efficiently. We are proud to have twenty years of experience partnering with restaurants like McDonald’s to develop specialized HR solutions that reduce overhead costs, minimize staff turnover, and streamline day-to-day business operations.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Unique Solutions Developed for You</h4>
                    <div class="inside-spacer"></div>
                    <p>Every restaurant has unique human resources needs, so when National PEO partners with you, we focus on customizing solutions that perfectly suit your circumstances. Over the last twenty years, we have developed systems that simplify our clients’ hiring and onboarding processes, streamline their payroll processing, and ensure compliance with labor laws.</p>
                    <p>In particular, we specialize in developing the following services for our restaurant partners:</p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Industry-Solutions-Restaurant-Unique-Solutions.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-4">
                <h4>Human Resources</h4>
                <p class="m-t-20">National PEO’s comprehensive human resources outsourcing helps you create safe, healthy restaurants and hire enthusiastic applicants who have the exact skills you need. Our HR support doesn’t just minimize your day-to-day administrative burden -- it helps you build a successful team with a low rate of turnover so you can focus on growing your business.</p>
            </div>
            <div class="col-lg-4">
                <h4>Electronic Onboarding</h4>
                <p class="m-t-20">National PEO’s electronic onboarding solutions minimize the complicated administrative work that comes with adding new team members so you can focus on training them to provide your customers with safe, high-quality dining experiences.</p>
            </div>
            <div class="col-lg-4">
                <h4>Payroll</h4>
                <p class="m-t-20">Payroll is tedious, and it can be even more complicated for businesses with tipped employees. National PEO will take care of deposits, deductions, and data tracking to ensure that your restaurant employees are always compensated correctly and on time.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With National PEO?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
