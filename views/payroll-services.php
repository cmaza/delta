<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/National-PEO-Payroll-Services.png">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Payroll Services</h1>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Payroll-Services-Operations.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>How Payroll Services Help Streamline Business Operations</h4>
                    <p>National PEO has become a leading Professional Employment Organization (PEO) that offers personal-touch online payroll processing services that are both reliable and responsive. Find out how our payroll processing company and payroll systems can help simplify and streamline your business.</p>
                    <p>At National PEO, we offer payroll solutions and the kind of service that our clients expect and deserve. We offer online payroll processing services that fosters a positive, productive relationship with our clients. Our payroll accounting systems ensure that your payroll is done right not just the first time, but every time, making sure that all of your needs are met.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-10"></div>
</section>

<section class="background-grey p-t-50">
    <div class="section-spacer-40"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Why National PEO</h4>
                    <p>Everything from processing employees’ paychecks accurately to administering your benefits carefully to addressing your human resources needs securely — National PEO is with you every step of the way. We like to think much of our success owes itself to the company philosophy that is ingrained in our functioning — the philosophy that treats clients fairly and approaches all situations with a can-do, positive attitude. It is second nature for our payroll company to be honest and upfront about all of our dealings to go that extra mile to ensure complete client satisfaction.</p>
                    <p>TNational PEO, based in Arizona, has shown remarkable staying power. As other payroll companies have fallen by the wayside, National PEO continues to grow, boasting excellent client retention and a high degree of customer satisfaction.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Payroll-Services-Why-National-PEO.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="section-spacer-10"></div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Payroll-Services-Company.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Exploring Payroll Processing Services</h4>
                    <p>Broadly speaking, a professional employer organization firm helps businesses outsource human resources and employee management tasks such as payroll services and workers’ compensation, safety and risk management, recruiting, human resource training, and business development.</p>
                </div>
            </div>
        <div class="container">
            <div class="section-spacer-30"></div>
            <h3>Payroll Service Client Functions</h3>
            <div class="inside-spacer"></div>
          <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-icon dots list-icon-list list-icon-colored-grey">
                            <li>Payroll administration alleviates the hassle of completing employee payroll while improving overall accuracy. From direct deposit and job costing to garnishments and deductions, National PEO manages the execution of tedious or complicated tasks on a recurring basis.</li>
                            <li>Complete your payroll service bundle by adding ASO services to existing tax-related services including federal and state tax filing and payments, year-end taxes, and unemployment taxes.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-icon dots list-icon-list list-icon-colored-grey">
                            <li>Online self-service payroll portal and general ledger interface allows clients to gain full, on-demand access to payroll and employee data using industry-leading technology front-loaded with accounting system integrations including QuickBooks and Great Plains.</li>
                            <li>Small business payroll processing includes access to paycards. Give employees convenient access to funds via direct deposit and ATMs.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
            <div class="row text-center">
            <div class="col-lg-12">
                <p class="m-t-60 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a>
                </p>
            </div>
        </div>
        </div>
        </div>
    </div>
    <div class="section-spacer-10"></div>
</section>

<section class="background-grey p-t-50">
    <div class="section-spacer-40"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Small Business Payroll Processing Services</h4>
                    <div class="inside-spacer"></div>
                    <p>Business owners know there is more to payroll than simply signing a check and dropping it in the mail. Employers must verify the proper tax withholdings and pay to the appropriate government agency at the time indicated. Unfortunately, many small businesses don’t have the luxury of a separate accounting department to manage these minute details, including the completion of cumbersome forms required to be completed regularly. Missed payment deadlines can mean heavy penalties as well as low morale in the office and less productive staff.</p>
                    <p>The addition of a seasoned payroll processing service specializing in small businesses can save time and eliminate employer hassles centered around outsourcing business payroll, reduce legal liabilities or obligations, and offer employees better remuneration packages to attract the best talent.</p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/National-PEO-Payroll-Services-Small-Business.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-20"></div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Learn How National PEO Can Work for You</h4>
                <p class="m-t-20 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
                <div class="section-spacer-10"></div>
            </div>
        </div>
    </div>
    
</section>