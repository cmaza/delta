<?php
// global php functions and variables
require 'include/functions.php';

// this section isn't a website route, it handle the ajax requests used by the forms submits
if ($isFormSend) {
    switch ($requestedPage) {
        case 'form-send/marketplace' :
            require __DIR__ . '/include/marketplace.php';
            break;
        case 'form-send/training' :
            require __DIR__ . '/include/training.php';
            break;
        case 'form-send/customer-service' :
            require __DIR__ . '/include/contact-customer-service.php';
            break;
        case 'form-send/become-agent' :
            require __DIR__ . '/include/contact-become-an-agent.php';
            break;
        case 'form-send/referrals' :
            require __DIR__ . '/include/referrals.php';
            break;
        case 'form-send/hr-download' :
            require __DIR__ . '/include/hr-download.php';
            break;
        case 'form-send/free-diagnostic' :
            require __DIR__ . '/include/free-diagnostic.php';
            break;
        case 'form-send/free-diagnostic-home' :
            require __DIR__ . '/include/free-diagnostic-home.php';
            break;
    }
    exit();
}
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="max-snippet:-1,max-image-preview:large,max-video-preview:-1,index,follow"/>

    <!-- Do not make changes here if you do not know what are you doing - Title and Metas -->
    <?php
        $metaTagsItems = json_decode(file_get_contents('include/metas.json'));
        $filteredObject = filterByKeyValue($metaTagsItems, 'link', $_SERVER['QUERY_STRING']);
        echo '<title>'.$filteredObject[0]->metas->title.'</title>';
        echo '<meta name="author" content="'.$filteredObject[0]->metas->author.'" />';
        if (strlen(trim($filteredObject[0]->metas->description)) > 0) {
            echo '<meta name="description" content="'.$filteredObject[0]->metas->description.'" />';
            echo '<meta property="og:description" content="'.$filteredObject[0]->metas->description.'" />';
            echo '<meta name="twitter:description" content="'.$filteredObject[0]->metas->description.'" />';
        }
        echo '<meta property="og:locale" content="'.$filteredObject[0]->metas->oglocale.'" />';
        echo '<meta property="og:type" content="'.$filteredObject[0]->metas->ogtype.'" />';
        echo '<meta property="og:title" content="'.$filteredObject[0]->metas->title.'" />';
        echo '<meta property="og:url" content="';
        echo getFullUrl().'" />';
        echo '<meta property="og:site_name" content="'.$filteredObject[0]->metas->ogsite_name.'" />';
        echo '<meta property="article:publisher" content="'.$filteredObject[0]->metas->articlepublisher.'" />';
        echo '<meta name="twitter:card" content="'.$filteredObject[0]->metas->twittercard.'" />';
        echo '<meta name="twitter:title" content="'.$filteredObject[0]->metas->title.'" />';
        echo '<meta name="twitter:site" content="'.$filteredObject[0]->metas->twittersite.'" />';
        echo '<meta name="twitter:creator" content="'.$filteredObject[0]->metas->twittercreator.'" />';
        echo '<link rel="canonical" href="';
        echo getFullUrl().'" />';
    ?>
    <!-- end: Title/metas -->
    <link rel="icon" type="image/png" href="<?php echo basePathUrl();?>images/fav-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo basePathUrl();?>images/fav-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" type="image/png" href="<?php echo basePathUrl();?>images/fav-180x180.png">
    <meta name="msapplication-TileImage" content="<?php echo basePathUrl();?>images/fav-270x270.png" />
    <!-- Css Stylesheets & Fonts -->
    <link href="<?php echo basePathUrl();?>css/plugins.css" rel="stylesheet">
    <link href="<?php echo basePathUrl();?>css/style.css" rel="stylesheet">
    <link href="<?php echo basePathUrl();?>css/theme.css" rel="stylesheet">
    <link href="<?php echo basePathUrl();?>css/custom.css" rel="stylesheet">
    <link href="<?php echo basePathUrl()?>css/map-style.css" rel="stylesheet">
    <link href="<?php echo basePathUrl()?>css/bootstrap-pre-header.css" rel="stylesheet">
    <!-- end: Css Stylesheets & Fonts -->
</head>

<body class="<?php echo ($isPortalsPage) ? 'breakpoint-xl b--desktop' : '';?>">
    

<!-- Client Center map container -->
<span id="usjstip"></span>
<!-- end: Client Center map container -->
<div class="body-inner">
    <?php
    // global header and top section
    if (!$isPortalsPage) {
		
    //extra top nav
    /*
		echo '<div id="topbar" class="d-none d-xl-block d-lg-block topbar-fullwidth">';
		echo '<div class="container container-top-nav">';
		echo '<div class="row">';
		echo '<div class="col-md-7">';
		echo '<ul class="top-menu">';
		echo '<li><a href="/covid19-resources"><b>Vensure COVID-19 Resources</b></a></li>';
		echo '</ul>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
        echo '</div>';
        */
        echo '<div id="ph-pre-header">
            <div class="container-fluid js-preheader banner-cta full-width bg-grey-f3f3f3 blue-close fadeIn">
                <section class="row_preh center-block_preh" >
                    <div class="col-xs-12 text-main p-5">
                        <div class="copy">
                            <div class="col-xs-12 col-sm-3 px-0">
                                <img alt="Medkit" src="https://www.vensure.com/images/orange-medkit.png" style="height: 42px;" class="mr-20 float-left" /><strong class="size20" style="line-height: 28px;">National PEO Coronavirus (COVID-19) Updates</strong>
                            </div>
                            <div class="col-xs-12 col-sm-9 pl-sm-40 px-0">
                                <h5>National PEO is actively monitoring Coronavirus (COVID-19) developments. We have compiled valuable resources for you to utilize as the Coronavirus situation continues to evolve. Learn more about our <a href="/covid19-resources"><font color="#e05206">COVID-19 List of Resources here</font></a>.</h5>
                            </div>
                        </div>
                    </div>      
                    <div class="preheader-close" onclick="parentNode.remove()"></div>          
                </section>
            </div>
        </div>';
        


	//main top nav	
		echo '<div id="topbar" class="d-none d-xl-block d-lg-block topbar-fullwidth">';
		echo '<div class="container container-top-nav">';
		echo '<div class="row">';
		echo '<div class="col-md-12">';
		require 'include/top-nav.php';
		echo '</div>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
		      
        echo  '<header id="header" data-fullwidth="true"><div class="header-inner"><div class="container"><div id="logo"><a href="' . basePathUrl() . '"><img src="' . basePathUrl() . 'images/National PEO_Logo.svg" class="logo-default"></a></div>';
        require 'include/nav.php';
        echo '</div></div></header>';
    }
        // internal content pages
        switch ($requestedPage) {
            case '/' :
                require __DIR__ . '/views/home.php';
                break;
            case '' :
                require __DIR__ . '/views/home.php';
                break;
            case 'payroll-services' :
            case 'payroll-services/' :
                require __DIR__ . '/views/payroll-services.php';
                break;
            case 'hr-solutions' :
            case 'hr-solutions/' :
                require __DIR__ . '/views/hr-solutions.php';
                break;
            case 'who-we-empower' :
            case 'who-we-empower/' :
                require __DIR__ . '/views/who-we-empower.php';
                break;
            case 'industry/construction' :
            case 'industry/construction/' :
                require __DIR__ . '/views/construction-industry-solutions.php';
                break;
            case 'industry/healthcare' :
            case 'industry/healthcare/' :
                require __DIR__ . '/views/health-care-industry-solutions.php';
                break;
            case 'industry/manufacturing' :
            case 'industry/manufacturing/' :
                require __DIR__ . '/views/manufacturing-industry-solutions.php';
                break;
            case 'industry/non-profit' :
            case 'industry/non-profit/' :
                require __DIR__ . '/views/non-profit-industry-solutions.php';
                break;
            case 'industry/restaurant' :
            case 'industry/restaurant/' :
                require __DIR__ . '/views/restaurant-industry-solutions.php';
                break;
            case 'industry/tech' :
            case 'industry/tech/' :
                require __DIR__ . '/views/tech-industry-solutions.php';
                break;
            case 'why-national-peo' :
            case 'why-national-peo/' :
                require __DIR__ . '/views/about-us.php';
                break;
            case 'contact' :
            case 'contact/' :
                require __DIR__ . '/views/contact-us.php';
                break;
            case 'resources' :
            case 'resources/' :
                require __DIR__ . '/views/resources.php';
                break;
            case 'peo-forms' :
            case 'peo-forms/' :
                require __DIR__ . '/views/forms.php';
                break;
            case 'peo-facts' :
            case 'peo-facts/' :
                require __DIR__ . '/views/peo-facts.php';
                break;
            case 'privacy-policy' :
            case 'privacy-policy/' :
                require __DIR__ . '/views/privacy-policy.php';
                break;
            case 'terms-of-use' :
            case 'terms-of-use/' :
                require __DIR__ . '/views/terms-of-use.php';
                break;
            case 'covid19-resources' :
            case 'covid19-resources/' :
                require __DIR__ . '/views/covid19-resources.php';
                break;
            case 'landing-page/thank-you' :
            case 'landing-page/thank-you/' :
                require __DIR__ . '/views/landing-page-thank-you.php';
                break;
            default:
                http_response_code(404);
                require __DIR__ . '/views/404.php';
                break;
    /*        
    /*        
            case 'solutions' :
            case 'solutions/' :
                require __DIR__ . '/views/size-based-peo-solutions.php';
                break;
            case 'peo-services/broker' :
            case 'peo-services/broker/' :
                require __DIR__ . '/views/peo-broker.php';
                break;
            case 'peo-services' :
            case 'peo-services/' :
                require __DIR__ . '/views/peo-services.php';
                break;
            case 'peo-services/performance-management' :
            case 'peo-services/performance-management/' :
                require __DIR__ . '/views/performance-management.php';
                break;
            case 'peo-services/hiring' :
            case 'peo-services/hiring/' :
                require __DIR__ . '/views/applicant-tracking.php';
                break;
            case 'peo-services/onboarding' :
            case 'peo-services/onboarding/' :
                require __DIR__ . '/views/onboarding.php';
                break;
            case 'peo-services/recruiting' :
            case 'peo-services/recruiting/' :
                require __DIR__ . '/views/recruiting.php';
                break;
            case 'peo-services/employee-benefits' :
            case 'peo-services/employee-benefits/' :
                require __DIR__ . '/views/employee-benefits.php';
                break;
            case 'peo-services/benefits-administration' :
            case 'peo-services/benefits-administration/' :
                require __DIR__ . '/views/benefits-enrollment-administration.php';
                break;
            case 'peo-services/marketplace' :
            case 'peo-services/marketplace/' :
                require __DIR__ . '/views/vensure-benefits-marketplace.php';
                break;
            case 'peo-services/payroll-services' :
            case 'peo-services/payroll-services/' :
                require __DIR__ . '/views/payroll-services.php';
                break;
            case 'peo-services/time-attendance' :
            case 'peo-services/time-attendance/' :
                require __DIR__ . '/views/time-attendance-system.php';
                break;
            case 'peo-services/risk-management' :
            case 'peo-services/risk-management/' :
                require __DIR__ . '/views/risk-management-compliance.php';
                break;
            case 'peo-services/workers-compensation' :
            case 'peo-services/workers-compensation/' :
                require __DIR__ . '/views/workers-compensation.php';
                break;
            case 'peo-services/vfficient' :
            case 'peo-services/vfficient/' :
                require __DIR__ . '/views/vfficient-hr-technology.php';
                break;
            case 'solutions/small-business' :
            case 'solutions/small-business/' :
                require __DIR__ . '/views/solutions-for-smaller-businesses.php';
                break;
            case 'solutions/midsize-business' :
            case 'solutions/midsize-business/' :
                require __DIR__ . '/views/mid-market-solutions.php';
                break;
            case 'solutions/large-business' :
            case 'solutions/large-business/' :
                require __DIR__ . '/views/large-entity-solutions.php';
                break;
            case 'mergers-and-acquisitions' :
            case 'mergers-and-acquisitions/' :
                require __DIR__ . '/views/mergers-and-acquisitions.php';
                break;
            case 'resources/what-is-a-peo' :
            case 'resources/what-is-a-peo/' :
                require __DIR__ . '/views/what-is-a-peo.php';
                break;
            case 'resources/events' :
            case 'resources/events/' :
                require __DIR__ . '/views/events.php';
                break;
            case 'resources/construction-training':
            case 'resources/construction-training/':
                require __DIR__ . '/views/construction-industry-osha-training.php';
                break;
            case 'resources/general-industry-training' :
            case 'resources/general-industry-training/' :
                require __DIR__ . '/views/general-industry-osha-training.php';
                break;
            case 'careers' :
            case 'careers/' :
                require __DIR__ . '/views/careers.php';
                break;
            case 'contact' :
            case 'contact/' :
                require __DIR__ . '/views/contact-us.php';
                break;
            case 'referrals' :
            case 'referrals/' :
                require __DIR__ . '/views/referrals.php';
                break;
            case 'eereferrals' :
            case 'eereferrals/' :
                require __DIR__ . '/views/eereferrals.php';
                break;
            case 'connect' :
            case 'connect/' :
                require __DIR__ . '/views/connect.php';
                break;
            case 'solvo-global' :
            case 'solvo-global/' :
                require __DIR__ . '/views/solvo-global.php';
                break;
            case 'resources/blog' :
            case 'resources/blog/' :
                echo "<script> location.href='http://blog.vensure.com/'; </script>";
                break;
            case 'hr-services-gb' :
            case 'hr-services-gb/' :
                require __DIR__ . '/views/hr-services-gb.php';
                break; 
            case 'hr-services-gb-2' :
            case 'hr-services-gb-2/' :
                require __DIR__ . '/views/hr-services-gb-2.php';
                break;
            case 'peo-services-galp' :
            case 'peo-services-galp/' :
                require __DIR__ . '/views/peo-services-galp.php';
                break;
            case 'benefits-services-galp' :
            case 'benefits-services-galp/' :
                require __DIR__ . '/views/benefits-services-galp.php';
                break;
            case 'employment-practices-liability-insurance' :
            case 'employment-practices-liability-insurance/' :
                require __DIR__ . '/views/employment-practices-liability-insurance.php';
                break;
            case 'employee-work-perks' :
            case 'employee-work-perks/' :
                require __DIR__ . '/views/employee-work-perks.php';
                break;
            case 'telehealth' :
            case 'telehealth/' :
                require __DIR__ . '/views/telehealth.php';
                break;
            case 'work-opportunity-tax-credits' :
            case 'work-opportunity-tax-credits/' :
                require __DIR__ . '/views/work-opportunity-tax-credits.php';
                break;
            case 'staples-2' :
            case 'staples-2/' :
                require __DIR__ . '/views/staples-2.php';
                break;
            case 'events' :
            case 'events' :
                require __DIR__ . '/views/events-landing.php';
                break;
            case 'roadside-assistance' :
            case 'roadside-assistance/' :
                require __DIR__ . '/views/roadside-assistance.php';
                break;
            case 'pet-insurance' :
            case 'pet-insurance/' :
                require __DIR__ . '/views/pet-insurance.php';
                break;
            case 'moneycard-landing-page' :
            case 'moneycard-landing-page/' :
                require __DIR__ . '/views/moneycard-landing-page.php';
                break;
            case 'partner' :
            case 'partner/' :
                require __DIR__ . '/views/partner.php';
                break;
            case 'perks' :
            case 'perks/' :
                require __DIR__ . '/views/perks.php';
                break;
            case 'identity-theft-protection-recovery' :
            case 'identity-theft-protection-recovery/' :
                require __DIR__ . '/views/identity-theft-protection-recovery.php';
                break;
            case 'choose-vensure' :
            case 'choose-vensure/' :
                require __DIR__ . '/views/choose-vensure.php';
                break;
            case 'hrtech2019' :
            case 'hrtech2019/' :
                require __DIR__ . '/views/hrtech2019.php';
                break;
            case 'constructionhr' :
            case 'constructionhr/' :
                require __DIR__ . '/views/constructionhr.php';
                break;
            case 'portals' :
            case 'portals/' :
                require __DIR__ . '/views/login.php';
                break;
            case 'client-center' :
            case 'client-center/' :
                require __DIR__ . '/views/client-center.php';
                break;
            case 'client-center/florida' :
            case 'client-center/florida/' :
                require __DIR__ . '/views/client-center-florida.php';
                break;
            case 'client-center/california' :
            case 'client-center/california/' :
                require __DIR__ . '/views/client-center-california.php';
                break;
            case 'client-center/nevada' :
            case 'client-center/nevada/' :
                require __DIR__ . '/views/client-center-nevada.php';
                break;
            case 'client-center/arizona' :
            case 'client-center/arizona/' :
                require __DIR__ . '/views/client-center-arizona.php';
                break;
            case 'client-center/utah' :
            case 'client-center/utah/' :
                require __DIR__ . '/views/client-center-utah.php';
                break;
            case 'client-center/colorado' :
            case 'client-center/colorado/' :
                require __DIR__ . '/views/client-center-colorado.php';
                break;
            case 'client-center/new-mexico' :
            case 'client-center/new-mexico/' :
                require __DIR__ . '/views/client-center-new-mexico.php';
                break;
            case 'client-center/nebraska' :
            case 'client-center/nebraska/' :
                require __DIR__ . '/views/client-center-nebraska.php';
                break;
            case 'client-center/kansas' :
            case 'client-center/kansas/' :
                require __DIR__ . '/views/client-center-kansas.php';
                break;
            case 'client-center/oklahoma' :
            case 'client-center/oklahoma/' :
                require __DIR__ . '/views/client-center-oklahoma.php';
                break;
            case 'client-center/texas' :
            case 'client-center/texas/' :
                require __DIR__ . '/views/client-center-texas.php';
                break;
            case 'client-center/louisiana' :
            case 'client-center/louisiana/' :
                require __DIR__ . '/views/client-center-louisiana.php';
                break;
            case 'client-center/arkansas' :
            case 'client-center/arkansas/' :
                require __DIR__ . '/views/client-center-arkansas.php';
                break;
            case 'client-center/missouri' :
            case 'client-center/missouri/' :
                require __DIR__ . '/views/client-center-missouri.php';
                break;
            case 'client-center/iowa' :
            case 'client-center/iowa/' :
                require __DIR__ . '/views/client-center-iowa.php';
                break;
            case 'client-center/minnesota' :
            case 'client-center/minnesota/' :
                require __DIR__ . '/views/client-center-minnesota.php';
                break;
            case 'client-center/wisconsin' :
            case 'client-center/wisconsin/' :
                require __DIR__ . '/views/client-center-wisconsin.php';
                break;
            case 'client-center/illinois' :
            case 'client-center/illinois/' :
                require __DIR__ . '/views/client-center-illinois.php';
                break;
            case 'client-center/mississippi' :
            case 'client-center/mississippi/' :
                require __DIR__ . '/views/client-center-mississippi.php';
                break;
            case 'client-center/michigan' :
            case 'client-center/michigan/' :
                require __DIR__ . '/views/client-center-michigan.php';
                break;
            case 'client-center/indiana' :
            case 'client-center/indiana/' :
                require __DIR__ . '/views/client-center-indiana.php';
                break;
            case 'client-center/kentucky' :
            case 'client-center/kentucky/' :
                require __DIR__ . '/views/client-center-kentucky.php';
                break;
            case 'client-center/tennessee' :
            case 'client-center/tennessee/' :
                require __DIR__ . '/views/client-center-tennessee.php';
                break;
            case 'client-center/alabama' :
            case 'client-center/alabama/' :
                require __DIR__ . '/views/client-center-alabama.php';
                break;
            case 'client-center/georgia' :
            case 'client-center/georgia/' :
                require __DIR__ . '/views/client-center-georgia.php';
                break;
            case 'client-center/north-carolina' :
            case 'client-center/north-carolina/' :
                require __DIR__ . '/views/client-center-north-carolina.php';
                break;
            case 'client-center/south-carolina' :
            case 'client-center/south-carolina/' :
                require __DIR__ . '/views/client-center-south-carolina.php';
                break;
            case 'client-center/virginia' :
            case 'client-center/virginia/' :
                require __DIR__ . '/views/client-center-virginia.php';
                break;
            case 'client-center/ohio' :
            case 'client-center/ohio/' :
                require __DIR__ . '/views/client-center-ohio.php';
                break;
            case 'client-center/pennsylvania' :
            case 'client-center/pennsylvania/' :
                require __DIR__ . '/views/client-center-pennsylvania.php';
                break;
            case 'client-center/new-york' :
            case 'client-center/new-york/' :
                require __DIR__ . '/views/client-center-new-york.php';
                break;
            case 'client-center/maryland' :
            case 'client-center/maryland/' :
                require __DIR__ . '/views/client-center-maryland.php';
                break;
            case 'client-center/delaware' :
            case 'client-center/delaware/' :
                require __DIR__ . '/views/client-center-delaware.php';
                break;
            case 'client-center/new-jersey' :
            case 'client-center/new-jersey/' :
                require __DIR__ . '/views/client-center-new-jersey.php';
                break;
            case 'client-center/vermont' :
            case 'client-center/vermont/' :
                require __DIR__ . '/views/client-center-vermont.php';
                break;
            case 'client-center/new-hampshire' :
            case 'client-center/new-hampshire/' :
                require __DIR__ . '/views/client-center-new-hampshire.php';
                break;
            case 'client-center/massachusetts' :
            case 'client-center/massachusetts/' :
                require __DIR__ . '/views/client-center-massachusetts.php';
                break;
            case 'client-center/connecticut' :
            case 'client-center/connecticut/' :
                require __DIR__ . '/views/client-center-connecticut.php';
                break;
            case 'client-center/rhode-island' :
            case 'client-center/rhode-island/' :
                require __DIR__ . '/views/client-center-rhode-island.php';
                break;
            case 'client-center/hawaii' :
            case 'client-center/hawaii/' :
                require __DIR__ . '/views/client-center-hawaii.php';
                break;
            case 'client-center/alaska' :
            case 'client-center/alaska/' :
                require __DIR__ . '/views/client-center-alaska.php';
                break;
            case 'client-center/oregon' :
            case 'client-center/oregon/' :
                require __DIR__ . '/views/client-center-oregon.php';
                break;
            case 'client-center/washington' :
            case 'client-center/washington/' :
                require __DIR__ . '/views/client-center-washington.php';
                break;
            case 'client-center/idaho' :
            case 'client-center/idaho/' :
                require __DIR__ . '/views/client-center-idaho.php';
                break;
            case 'client-center/wyoming' :
            case 'client-center/wyoming/' :
                require __DIR__ . '/views/client-center-wyoming.php';
                break;
            case 'client-center/montana' :
            case 'client-center/montana/' :
                require __DIR__ . '/views/client-center-montana.php';
                break;
            case 'client-center/north-dakota' :
            case 'client-center/north-dakota/' :
                require __DIR__ . '/views/client-center-north-dakota.php';
                break;
            case 'client-center/south-dakota' :
            case 'client-center/south-dakota/' :
                require __DIR__ . '/views/client-center-south-dakota.php';
                break;
            case 'client-center/west-virginia' :
            case 'client-center/west-virginia/' :
                require __DIR__ . '/views/client-center-west-virginia.php';
                break;
            case 'client-center/maine' :
            case 'client-center/maine/' :
                require __DIR__ . '/views/client-center-maine.php';
                break;
            case 'client-center/district-of-columbia' :
            case 'client-center/district-of-columbia/' :
                require __DIR__ . '/views/client-center-district-of-columbia.php';
                break;
            case 'free-diagnostic' :
            case 'free-diagnostic/' :
                require __DIR__ . '/views/custom-diagnostic-for-business.php';
                break;
            case 'communicationrequests' :
            case 'communicationrequests/' :
                require __DIR__ . '/views/communicationrequests.php';
                break;
            case 'marketingrequests' :
            case 'marketingrequests/' :
                require __DIR__ . '/views/marketingrequests.php';
                break;
            case 'telemedicine' :
            case 'telemedicine/' :
                require __DIR__ . '/views/telemedicine.php';
                break;
            case 'cyber' :
            case 'cyber/' :
                echo "<script> location.href='https://www.vensure.com/connect'; </script>";
                break;
            case 'epli' :
            case 'epli/' :
                echo "<script> location.href='https://www.vensure.com/connect'; </script>";
                break;
            case 'hr-service-gb' :
            case 'hr-service-gb/' :
                echo "<script> location.href='https://www.vensure.com/hr-services-gb/'; </script>";
                break;
            case 'horizon' :
            case 'horizon/' :
                echo "<script> location.href='https://www.vensure.com/peo-services/human-resources/'; </script>";
                break;
            case 'labor' :
            case 'labor/' :
                echo "<script> location.href='https://www.vensure.com/peo-services/marketplace/'; </script>";
                break;
            case 'moneycard' :
            case 'moneycard/' :
                echo "<script> location.href='https://vns-ep.prismhr.com/#/auth/login'; </script>";
                break;
            case 'payroll' :
            case 'payroll/' :
                echo "<script> location.href='https://www.vensure.com/connect'; </script>";
                break;
            case 'peo' :
            case 'peo/' :
                echo "<script> location.href='https://www.vensure.com/connect'; </script>";
                break;
            case 'peo-services/employee-benefits-program' :
            case 'peo-services/employee-benefits-program/' :
                echo "<script> location.href='https://www.vensure.com/peo-services/employee-benefits/'; </script>";
                break;
            case 'solvoglobal' :
            case 'solvoglobal/' :
                echo "<script> location.href='https://go.vensure.com/recruitingservices'; </script>";
                break;
            case 'support' :
            case 'support/' :
                echo "<script> location.href='https://www.vensure.com/connect'; </script>";
                break;
            case 'TLM' :
            case 'TLM/' :
                echo "<script> location.href='https://www.vensure.com/connect'; </script>";
                break;
            
            
                */
        }
        // end: internal Content pages
    if (!$isPortalsPage) {
        // footer section
        require 'include/footer.php';
    }
    ?>
    <div id="modalLetsGetStarted" class="modal no-padding" data-delay="3000" style="max-width: 400px;">
        <div class="p-30 p-xs-20">
            <h2 class="started-modal">Request a Free HR Diagnostic</h2>
            <iframe src="https://go.vensure.com/l/656143/2020-04-13/2bx224" width="90%" height="375" type="text/html" frameborder="0" allowtransparency="true" style="border: 0" scrolling="no"></iframe>
        </div>
    </div>
    <div id="modalBecomeAPartnerToday" class="modal no-padding" data-delay="2000" style="max-width: 400px;">
        <div class="p-30 p-xs-20">
            <h2 class="started-modal">Become a Partner Today</h2>
            <iframe src="https://go.vensure.com/l/656143/2020-01-24/ll619" width="90%" height="480" type="text/html" frameborder="0" allowtransparency="true" style="border: 0" scrolling="no"></iframe>
        </div>
    </div>
    <?php
    // HR insider popup left-bottom corner (only load in the home page)
    if ($isIndex) {
    echo '<div id="hrDownload" class="popup-hr-download modal-bottom modal-auto-open" data-delay="2000">
        <div class="container">
            <div class="row m-b-5">
                <div class="col-md-12 text-right p-0"><button type="button" class="pum-close modal-close">x</button></div>
            </div>
            <div class="row">
                <div class="col-md-3 p-5">
                    <img src="images/Vensure-HR-Insider-Preview.jpg" />
                </div>
                <div class="col-md-9 p-t-0 p-l-10 p-5">
                    Find out how partnering with a PEO saves you time and money while empowering you to grow your business with confidence.
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 p-r-10 p-l-10">
                    <form class="form-popup-hr" novalidate action="';
    echo basePathUrl();
            echo 'form-send/hr-download" role="form" method="post" data-hr-insider="https://go.vensure.com/l/656143/2019-07-01/2xgll/656143/31185/VensureHR_What_is_a_PEO_f_em.pdf">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="hr_email">Your Company Email</label>
                                <input type="email" class="form-control form-input-home email" id="hr_email" name="hr_email" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button type="submit" id="popup-hr-bottom" class="btn btn-rounded btn-light hr-button">Get Your Free HR Insider Now</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>';
    }
    ?>
</div>
<!-- global Javascript Plugins-->
<script src="<?php echo basePathUrl();?>js/jquery.js"></script>
<script src="<?php echo basePathUrl();?>js/plugins.js"></script>
<!-- end: global Javascript Plugins-->
<!--Javascript functions-->
<script src="<?php echo basePathUrl();?>js/functions.js"></script>
<script src="<?php echo basePathUrl();?>js/custom.js"></script>
<!-- end: Javascript functions-->
<!-- client center map -->
<script src="<?php echo basePathUrl()?>js/map-config.js"></script>
<script src="<?php echo basePathUrl()?>js/map-interact.js"></script>
<!-- end: client center map -->
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 10821202;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' === document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<noscript>
    <a href="https://www.livechatinc.com/chat-with/10821202/" rel="nofollow">Chat with us</a>,
    powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
</noscript>
<!-- end: LiveChat code -->
</body>
</html>
