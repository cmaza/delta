<?php
// get current base dir including the host url
function basePathUrl() {
    return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" .
        $_SERVER['HTTP_HOST'] . str_replace('index.php', '', $_SERVER['PHP_SELF']);
}

// get full url (domain + url)
function getFullUrl() {
    $host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];
    $queries = (strlen($_SERVER['REQUEST_URI']) > 0) ? $_SERVER['REQUEST_URI'] : "";
    return $host . $queries;
}

// filter list of objects by key value
function filterByKeyValue($listObject, $key, $value) {
    if (strlen($value) === 0) {
        return array($listObject[0]);
    }
    $filtered = array();
    foreach($listObject as $k => $v) {
        if($v->$key === $value) {
            $filtered[] = $v;
            return $filtered;
        }
    }
    return $filtered;
}

/**
 * Global variables
 */
// the current url route
$requestedPage = $_SERVER['QUERY_STRING'];
// check portal page
$isPortalsPage = (strlen($requestedPage) > 0 && ($requestedPage === 'portals' || $requestedPage === 'portals/'));
// check home page
$isIndex = ($requestedPage === '' || $requestedPage === '/');
// check if form has send
$isFormSend = (strlen($requestedPage) > 0 && strpos($requestedPage, 'form-send') !== false);
