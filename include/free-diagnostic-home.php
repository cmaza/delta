<?php
session_cache_limiter('nocache');
header('Expires: ' . gmdate('r', 0));
header('Content-type: application/json');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'php-mailer/src/Exception.php';
require 'php-mailer/src/PHPMailer.php';

$mail = new PHPMailer();

// Form Fields
$company_email = isset($_POST["company_email"]) ? $_POST["company_email"] : null;

// Enter your email address. If you need multiple email recipes simply add a comma: email@domain.com, email2@domain.com
$toEmails = "Lauren.Singletary@vensure.com,William.Kenimer@vensure.com,Hyun.Kim@vensure.com,Shawn.Premo@vensure.com,Julie.dower@vensure.com,Donnie.thomas@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com";
$fromName = "Vensure Website";
$fromEmail = "vensure@vensure.com";
$subject = "Home Form Fill - Email"; // email subject
$messageContent = "<p>A Company requested a free diagnostic</p>"; // content message

if ($_SERVER['REQUEST_METHOD'] == 'POST') {


    if ($company_email != '') {

        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';

        $mail->From = $fromEmail;
        $mail->FromName = $fromName;

        if (strpos($toEmails, ',') !== false) {
            $email_addresses = explode(',', $toEmails);
            foreach ($email_addresses as $email_address) {
                $mail->addAddress(trim($email_address));
            }
        } else {
            $mail->addAddress($toEmails);
        }

        $mail->addReplyTo($company_email, $company_email);
        $mail->Subject = $subject;

        $company_email = isset($company_email) ? "Company Email: $company_email<br>" : '';

        $mail->Body = $messageContent . $company_email;

        if (!$mail->send()) {
            $response = array('response' => 'error', 'message' => $mail->ErrorInfo);
        } else {
            $response = array('response' => 'success');
        }

        echo json_encode($response);
    } else {
        $response = array('response' => 'error');
        echo json_encode($response);
    }

}
