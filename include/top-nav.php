<ul class="top-menu">
    <?php
    // build the top menu from a json object
    $topMenuItems = json_decode(file_get_contents('include/top-menu.json'));
    function buildTopMenu($menu) {
        $htmlTopMenu = '';
        foreach ($menu as $item => $value) {
            if ($value->type === 'url') {
                $htmlTopMenu .= '<li><a href="';
                $htmlTopMenu .= basePathUrl() . $value->link .'">'.$value->title.'</a></li>';
            }
            if ($value->type === 'tel') {
                $htmlTopMenu .= '<li><a href="tel:'. $value->link .'">'.$value->title.'</a></li>';
            }
            if ($value->type === 'external') {
                $htmlTopMenu .= '<li><a href="'. $value->link .'">'.$value->title.'</a></li>';
            }
            if ($value->type === 'email') {
                $htmlTopMenu .= '<li><a href="mailto:'. $value->link .'">'.$value->title.'</a></li>';
            }
        }
        return $htmlTopMenu;
    }
    echo buildTopMenu($topMenuItems);
    ?>
</ul>