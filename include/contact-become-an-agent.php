<?php
session_cache_limiter('nocache');
header('Expires: ' . gmdate('r', 0));
header('Content-type: application/json');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'php-mailer/src/Exception.php';
require 'php-mailer/src/PHPMailer.php';

$mail = new PHPMailer();

// Form Fields
$first_name = isset($_POST["first_name"]) ? $_POST["first_name"] : null;
$last_name = isset($_POST["last_name"]) ? $_POST["last_name"] : null;
$company_email = isset($_POST["company_email"]) ? $_POST["company_email"] : null;
$phone_number = isset($_POST["phone_number"]) ? $_POST["phone_number"] : null;
$company_name = isset($_POST["company_name"]) ? $_POST["company_name"] : null;
$primary_business = isset($_POST["primary_business"]) ? $_POST["primary_business"] : null;
$how_hear_about_us = isset($_POST["how_hear_about_us"]) ? $_POST["how_hear_about_us"] : null;
$message = isset($_POST["message"]) ? $_POST["message"] : null;

// Enter your email address. If you need multiple email recipes simply add a comma: email@domain.com, email2@domain.com
$toEmails = "businessdevelopment@vensure.com";
$fromName = "Vensure Contact Form";
$fromEmail = "vensure@vensure.com";
$subject = "Become An Agent"; // email subject
$messageContent = "<p>Become An Agent Contact Form:</p>"; // content message

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($company_email != '') {

        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';

        $mail->From = $fromEmail;
        $mail->FromName = $fromName;

        if (strpos($toEmails, ',') !== false) {
            $email_addresses = explode(',', $toEmails);
            foreach ($email_addresses as $email_address) {
                $mail->addAddress(trim($email_address));
            }
        } else {
            $mail->addAddress($toEmails);
        }

        $mail->addReplyTo($company_email, $full_name);
        $mail->Subject = $subject;

        $first_name_send = isset($first_name) ? "First Name: $first_name<br>" : '';
        $last_name_send = isset($last_name) ? "Last Name: $last_name<br>" : '';
        $company_email_send = isset($company_email) ? "Company Email: $company_email<br>" : '';
        $phone_number = isset($phone_number) ? "Phone Number: $phone_number<br>" : '';
        $company_name = isset($company_name) ? "Company Name: $company_name<br>" : '';
        $primary_business = isset($primary_business) ? "What is your Primary Business?: $primary_business<br>" : '';
        $how_hear_about_us = isset($how_hear_about_us) ? "How Did You Hear About Us: $how_hear_about_us<br>" : '';
        $message = isset($message) ? "Message: $message<br>" : '';

        $mail->Body = $messageContent . $first_name_send . $last_name_send . $company_email_send . $phone_number . $company_name . $primary_business . $how_hear_about_us . $message;

        if (!$mail->send()) {
            $response = array('response' => 'error', 'message' => $mail->ErrorInfo);
        } else {
            $response = array('response' => 'success');

            $autoRespondEmail = new PHPMailer();

            $autoRespondEmail->isHTML(true);
            $autoRespondEmail->CharSet = 'UTF-8';
            $autoRespondEmail->From = "vensure@vensure.com";
            $autoRespondEmail->FromName = "Vensure Employer Services";
            $autoRespondEmail->addAddress($company_email);
            $autoRespondEmail->Subject = "Thank You for Contacting Us";
            $autoResponseMessage = "<p>Dear $first_name $last_name,</p>";
            $autoResponseMessage .= "<p>Thanks for reaching out! We appreciate you contacting us. Someone will get back to you as soon as possible.</p>";
            $autoResponseMessage .= "<p>Have a great day!.<br>";
            $autoResponseMessage .= "<p>Vensure Employer Services<br>800.941.8731</p>";
            $autoRespondEmail->Body = $autoResponseMessage;
            $autoRespondEmail->send();
        }

        echo json_encode($response);
    } else {
        $response = array('response' => 'error');
        echo json_encode($response);
    }

}
