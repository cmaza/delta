<footer id="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row gap-y">
                <div class="col-xl-4 col-lg-4 col-md-12">
                    <div class="widget clearfix">
                        <p><a href="<?php echo basePathUrl();?>"><img src="<?php echo basePathUrl();?>images/National-PEO-Reverse-Logo.png" alt="logo"></a></p>
                        <div class="clearfix m-b-10"></div>
                        <div class="social-icons social-icons-colored social-icons-square float-left m-l-80">
                            <a href="https://www.facebook.com/nationalpeo" target="_blank" class="btn btn-dark btn-outline footer-socials btn-faceb">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="https://twitter.com/nationalpeo" target="_blank" class="btn btn-dark btn-outline footer-socials btn-twit">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="https://www.linkedin.com/company/national-peo/" target="_blank" class="btn btn-dark btn-outline footer-socials btn-linke">
                                <i class="fab fa-linkedin"></i>
                            </a>
                        </div>
                        <div class="clearfix m-b-30"></div>
                        <p class="m-l-90">
                           <img alt="NAPEO Logo" src="<?php echo basePathUrl();?>images/napeo-logo-1.png" alt="logo">
                        </p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-3">
                    <div class="widget">
                        <h4>Navigation</h4>
                        <ul class="list">
                            <li><a href="<?php echo basePathUrl();?>">Home</a></li>
                            <li><a href="<?php echo basePathUrl();?>payroll-services">Payroll Services</a></li>
                            <li><a href="<?php echo basePathUrl();?>hr-solutions">HR Solutions</a></li>
                            <li><a href="<?php echo basePathUrl();?>who-we-empower">Who We Empower</a></li>
                            <li><a href="<?php echo basePathUrl();?>why-national-peo">Why National PEO</a></li>
                            <li><a href="<?php echo basePathUrl();?>resources">Resources</a></li>
                            <li><a href="<?php echo basePathUrl();?>contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-3">
                    <div class="widget">
                        <ul class="list">
                            <li class="menu-icon white-text">
                                <i class="fa fa-map-marker-alt fa-flip-horizontal"> </i>  &nbsp;&nbsp;5745 N. Scottsdale Road, B-110<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Scottsdale, AZ 85250
                            </li>
                            <li class="menu-icon">
                                <a href="tel:8882210945"><i class="fa fa-phone fa-flip-horizontal"></i>888.221.0945</a>
                            </li>
                            <li class="menu-icon">
                                <a href="tel:4809451525"><i class="fa fa-fax fa-flip-horizontal"></i>480.945.1525</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-content">
        <div class="container">
            <div class="copyright-text text-center">Copyright © <?php echo date('Y'); ?> National PEO | <a href="<?php echo basePathUrl();?>privacy-policy">Privacy Policy</a> | <a href="<?php echo basePathUrl();?>terms-of-use">Terms of use</a></div>
        </div>
    </div>
</footer>