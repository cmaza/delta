<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Recruiting-2.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Recruiting</h1>
            <span>The Talent You Need to Succeed</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Recruiting-Talent.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Find Talent for Any Industry</h4>
                <div class="inside-spacer"></div>
                <p>VensureHR takes your organizational needs, role responsibilities, and industry opportunity of the position and connects you with the best talent available.
                    Only our teams are able to ensure your new hires are as productive as possible from day one.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Recruiting-Process.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-3">
                <h4>Direct Candidate</br>Communication</h4>
                <p class="m-t-20">From email triggers to text capabilities, hiring managers can answer candidate questions, schedule interviews, or initiate the hiring process.</p>
            </div>
            <div class="col-lg-3">
                <h4>Near Shore Services</h4>
                <p class="m-t-20">Focus on maintaining your organization’s high productivity with a cost-effective service to fulfil existing staffing
                    requirements through Solvo, a global workforce solution for US-based businesses.</p>
            </div>
            <div class="col-lg-3">
                <h4>All-Inclusive</h4>
                <p class="m-t-20">Regardless of your business size, the VensureHR recruiting team can be thought of as your own using our first-class HR services
                    and recruiting strategies to accelerate your business growth.</p>
            </div>
            <div class="col-lg-3">
                <h4>End-to-End Process</h4>
                <p class="m-t-20">From applicant screening to new hire training, partnering with VensureHR means you have a partner throughout the complete recruiting process.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">HR Solutions That Deliver the Freedom to Succeed</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
