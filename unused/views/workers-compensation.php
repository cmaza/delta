<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/RiskMgmtandCompl.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Workers’ Compensation</h1>
            <span>Coverage & Protection for Your Team</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Workers-Compensation-1.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>PEO Workers’ Compensation Services</h4>
                    <div class="inside-spacer"></div>
                    <p>VensureHR’s comprehensive workers’ compensation outsourcing program includes cost-effective insurance options, claims administration,
                        and loss control. On-site inspections, safety program development, implementation, and OSHA training are available services, as well.</p>
                    <p>We offer solutions to reduce your exposure by increasing compliance with federal and state laws. The outsourcing services provide the
                        peace of mind needed to let business owners like you refocus on managing the business!</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h4>Flexible Workers’ Compensation Insurance Options</h4>
                <div class="section-spacer-10"></div>
                <p>With access to multiple “A” rated carriers, we provide cost-effective options to ensure that you’re getting the best rates possible.</p>
                <p>We leverage our group buying power to offer you competitive rates. Customized safety program helps you provide the safest work environment for your employees</p>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Workers-Compensation-Premium.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-50"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Workers-Compensation-Pay.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Pay-As-You-Go Compensation</h4>
                <div class="section-spacer-10"></div>
                <p>Your payments are based on actual payroll and paid at the time of processing, making it easy to calculate and forecast expenses</p>
                <ul class="list-unstyled m-l-20">
                    <li class="p-t-5 p-b-5">No standard market deposits</li>
                    <li class="p-t-5 p-b-5">No annual audits</li>
                    <li class="p-t-5 p-b-5">No surprise lump sums due at the end of the year</li>
                </ul>
                <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h4>Claims Services</h4>
                <div class="section-spacer-10"></div>
                <p>Our workers’ compensation claim services team acts as a liaison between the injured employee, your company, the workers’ compensation carrier,
                    and the medical care provider. This process ensures strong communication while reducing the potential for litigation.</p>
                <p>It is our goal to assist the injured employee in making a full and timely recovery so they can get back to work through careful
                    monitoring and processing of the claim. VensureHR is an advocate of introducing employees back into the workforce at a speed and
                    level at which they are medically able to do so. Additionally, this approach improves your loss history, ultimately resulting in lower premium costs.</p>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Claims-Service.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With VensureHR?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>