<div id="slider" class="vensure-slider slider-fullscreen dots-creative" data-height-xs="360">
    <div class="slide kenburns" data-bg-parallax="<?php echo basePathUrl();?>images/home/vensure-landingpage-bg.jpg">
        <div class="bg-overlay" data-style="referrals"></div>
        <div class="container">
            <div class="slide-captions">
                <div class="row">
                    <div class="col-lg-8" data-animate="fadeInUp" data-animate-delay="500">
                        <h1 data-animate="fadeInUp" data-animate-delay="500" class="referrals">Grow Your Business With VensureHR</h1>
                        <h2 data-animate="fadeInUp" data-animate-delay="1000" class="referrals-subtitle">As one of the nation’s fastest growing professional employer organizations (PEO), VensureHR offers complete, end-to-end solutions for outsourced Payroll administration, Human Resources, Benefits, Risk Management and Workers’ Compensation services for businesses of any size, anywhere in the country.</h2>
                        <h3 data-animate="fadeInUp" data-animate-delay="1500" class="referrals-text"></h3>
                    </div>
                    <div class="col-lg-4" data-animate="fadeInUp" data-animate-delay="1500">
                        <div class="card referrals">
                            <div class="card-body">
                                <h4 class="referrals text-center">Customize Your Solution Today</h4>
                                <form class="form-referrals" novalidate action="<?php echo basePathUrl();?>form-send/referrals" role="form" method="post">
                                    <iframe src="https://go.vensure.com/l/656143/2020-02-03/nzz8y" width="100%" height="400" type="text/html" frameborder="0" allowTransparency="true" style="border: 0" scrolling="no"></iframe>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="page-content">
    <div class="container">
        <div class="grid-system-demo-live">
            <div class="row">
                <div class="col-lg-12 p-t-40 p-b-20">
                    <div class="heading-text heading-section referrals">
                        <h2 class="text-center referrals-text">About Vensure</h2>
                        <div class="line-small"></div>
                    </div>
                </div>
                <div class="col-lg-12 text-center">
                    <img src="<?php echo basePathUrl();?>images/referrals/Vensure-Infographic.jpg" class="img-fluid mx-auto d-block" />
                </div>
                <div class="col-lg-12 p-t-40 p-b-20">
                    <div class="heading-text heading-section referrals">
                        <h2 class="text-center referrals-text">What Our Clients Say</h2>
                        <div class="line-small"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="blockquote blockquote-color-grey referrals-testimony">
                        <div class="icon-box1 medium color">
                            <div class="icon referrals-testimony"><i class="fa fa-quote-right referrals-testimony"></i></div>
                            <p class="referrals-testimony">Vensure has helped us document our processes, provide professional services, and ensures we
                                are able to focus on building our business. Additionally, we have taken advantage of their OSHA training for our employees every year.
                                It’s a great perk that ensures our teams remain compliant.</p>
                        </div>
                    </div>
                    <p class="referrals-testimony-author text-right">Sandy T, Owner</p>
                </div>
                <div class="col-lg-4">
                    <div class="blockquote blockquote-color-grey referrals-testimony">
                        <div class="icon-box1 medium color">
                            <div class="icon referrals-testimony"><i class="fa fa-quote-right referrals-testimony"></i></div>
                            <p class="referrals-testimony">I was certain we would need to stop servicing a large client due to safety concerns. However,
                                Vensure closely guided us in developing and implementing a safety program that turned this situation around. In the past year,
                                we eliminated a common type of injury at our client site. I'm still in awe of what we accomplished together. I can say, with confidence,
                                we have the best PEO in the nation.</p>
                        </div>
                    </div>
                    <p class="referrals-testimony-author text-right">Eli E, CEO</p>
                </div>
                <div class="col-lg-4">
                    <div class="blockquote blockquote-color-grey referrals-testimony">
                        <div class="icon-box1 medium color">
                            <div class="icon referrals-testimony"><i class="fa fa-quote-right referrals-testimony"></i></div>
                            <p class="referrals-testimony">We used other PEOs in the past and had issues that impacted our clients. Since coming to Vensure we finally
                                feel stable, that we are being taken care of, and have good communication and relationships. Vensure has a high standard for customer service,
                                which we now use as a model in our own business.</p>
                        </div>
                    </div>
                    <p class="referrals-testimony-author text-right">Tony Z, Vice President</p>
                </div>
            </div>
        </div>
    </div>
</section>
