<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Small-Business.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Solutions for Smaller Businesses</h1>
            <span>A Beneficial Partnership</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <a href="#"><img src="<?php echo basePathUrl();?>images/Vensure-HR-Small-Business-Img.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>1-49 Employees</h4>
                    <div class="inside-spacer"></div>
                    <p>As a small business owner, you have invested a lot of time and energy in building your business. Outsourcing responsibilities like payroll,
                        time and attendance, and compliance management will allow you to focus on what matters most: developing and growing the business.</p>
                    <p>VensureHR can provides and administer payroll, HR, and employee benefits. With offices in every time zone our team of experts are here
                        to help answer questions and manage different aspects of your back-office.</p>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With VensureHR?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
        <div class="section-spacer-40"></div>
    </div>
</section>

