<?php
$headerImage = 'clientcenter/DE/Delaware.png';
$stateName = 'Delaware';
$stateMapImage = 'clientcenter/DE/client-center-state-Delaware.png';
?>
<section id="page-title" class="clientcenter-internals" data-bg-parallax="<?php echo basePathUrl().$headerImage;?>">
    <div class="bg-overlay"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section id="client-center" class="internals">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 m-b-20">
                <img src="<?php echo basePathUrl().$stateMapImage;?>" width="100%">
            </div>
            <div class="col-lg-7">
                <h4><?php echo $stateName;?></h4>
                <div class="section-spacer-5"></div>
                <p>Ranked number one in innovation, Delaware is one of the best places in the nation to start and grow your business. VensureHR’s Delaware clients care first and foremost about a comprehensive workers’ compensation program that includes insurance and claims administration.</p>
                <div class="section-spacer-5"></div>
                <div class="container-fluid table-responsive-sm">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr class="row">
                            <th class="col-sm-8 text-center">File</th>
                            <th class="col-sm-2 text-center">English</th>
                            <th class="col-sm-2 text-center">Espa&ntilde;ol</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Enrollment Packet Delaware</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/Delaware-Enrollment-Packet-English-1.pdf"><i class="far fa-file-pdf"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/Delaware-Enrollment-Packet-Spanish.pdf"><i class="far fa-file-pdf"></i></a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">New Hire Data Input Form</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/Vensure-New-Hire-Data-Input-Form-English.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/Vensure-New-Hire-Data-Input-Form-Spanish.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Form W-4</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/2020-Form-W-4-1.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/2020-Form-W-4-Spanish.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">D-W4</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/d-w4.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Form I-9</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/USCIS-Form-I-9-2020.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/USCIS-Form-I-9-2020.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Direct Deposit Form</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/Vensure-Direct-Deposit-Form-English-20191230.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/Vensure-Direct-Deposit-Form-Spanish-20191230.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Other State & Local Forms</td>
                            <td class="col-sm-2 text-center">
                                <a href="https://www.symmetry.com/resources/blank-forms-page" target="_blank"><i class="fas fa-external-link-alt"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="section-spacer-5"></div>
                <p><strong>Nota importante:</strong>
                    <em>En estos momentos no existe un formulario I-9 en espa&ntilde;ol, por lo tanto, usted deber&aacute; descargar la versi&oacute;n en ingl&eacute;s y usar las
                        instrucciones en espa&ntilde;ol.</em></p>
            </div>
        </div>
    </div>
</section>

