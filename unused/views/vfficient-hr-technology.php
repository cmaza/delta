<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Vfficient.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Vfficient HR Technology</h1>
            <span>Easily Manage Multiple HR Functions</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <a href="#"><img src="<?php echo basePathUrl();?>images/videos/Vensure-HR-Employee-Experience-Thumbnail.jpg" alt=""></a>
                        </div>
                        <div class="portfolio-description">
                            <a data-lightbox="iframe" href="https://vimeo.com/292783594?autoplay=1">
                                <i class="fas fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Grow Your Business</h4>
                    <div class="inside-spacer"></div>
                    <p>Is your company burdened with the time and costs associated with HR management? VensureHR understands that a flexible plan and strategic approach
                        to your HR program is necessary to avoid any confusion and stop wasting valuable time.</p>
                    <p>Vfficient is our cloud-based, client-centered human resource management solution designed to manage payroll, human resources, and benefits administration
                        from a single robust technology hub. Empower your employees to take charge of their HR needs with a modern, integrated, and intuitive technology platform!</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h4>Reduce Costs and Increase Productivity</h4>
                <div class="section-spacer-10"></div>
                <ul class="list-icon dots list-icon-list list-icon-colored-grey">
                    <li>Vfficient eliminates the need for multiple systems.</li>
                    <li>Allow users to accomplish critical business functions, such as hiring and onboarding, processing payroll, and scheduling quickly and accurately.</li>
                    <li>Powerful reporting capabilities with granular analysis enables users to access critical information and process payroll in seconds.</li>
                    <li>Our streamlined employee portal offers anytime, anywhere access.</li>
                </ul>
                <p>Vfficient is more than just your run-of-the-mill HR software. VensureHR’s technology is combined with a team of experts standing by to
                    provide guidance, address issues, or answer questions as they arise. From <a href="<?php echo basePathUrl();?>resources/blog" class="internal">ACA concerns to wage garnishments</a>
                    – it’s time for Vfficient by VensureHR to help take your business to the next level.</p>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Vfficient-Mockup-White.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Grow Your Business with VensureHR</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
