<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Construction.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Construction Industry OSHA Training</h1>
            <span>Investing in Safety, Knowledge of Potential Disasters, and Reduced Risk</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-right.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">About the Training</h4>
                <div class="inside-spacer"></div>
                <p>The OSHA 10-hour construction training program provides entry-level construction workers information on how to identify, abate,
                    avoid and prevent job-related hazards on a construction site. The training covers a variety of construction safety and health
                    hazards that a worker may encounter at a construction site. If you are looking for something with a broader scope, please
                    reference the <a href="<?php echo basePathUrl();?>resources/general-industry-training" class="internal">OSHA 10-hour General Industry Training Page</a>.</p>
                <p class="m-t-30 text-center">
                    <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                </p>
            </div>
        </div>
    </div>
</section>

<section class="background-grey p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="blockquote blockquote-color-white">
                    <h3 class="training"><span><i class="far fa-clock"></i></span>How long is the training?</h3>
                    <div class="section-spacer-10"></div>
                    <p>The training takes a minimum of 10-hours to complete. VensureHR’s standard two-day delivery format separates the training into two, five-hour days.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="blockquote blockquote-color-white">
                    <h3 class="training">
                        <span><i class="fa fa-check"></i></span>Is the training OSHA approved?
                    </h3>
                    <div class="section-spacer-10"></div>
                    <p>VensureHR’s OSHA-authorized trainers conduct 10 and 30-hour training courses under OSHA’s guidelines and course requirements.
                        Students will receive a UCSD/OSHA issued student course completion and certification card.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="blockquote blockquote-color-white">
                    <div class="row">
                        <div class="col-lg-6">
                            <h3 class="training"><span><i class="fa fa-search"></i></span>Topics Covered</h3>
                            <div class="section-spacer-10"></div>
                            <ul class="list-icon dots2 list-icon-list list-icon-colored m-l-10">
                                <li>Introduction to OSHA</li>
                                <li>Focus Four (leading causes of death in construction)
                                    <ul class="list-icon dots2 list-icon-minus list-icon-colored m-l-10">
                                        <li>Falls</li>
                                        <li>Electrocution</li>
                                        <li>Struck-By (e.g., falling objects, trucks, cranes, etc.)</li>
                                        <li>Caught-In or Between (e.g., trench hazards, equipment, barricades, etc)</li>
                                    </ul>
                                </li>
                                <li>Personal Protective and Lifesaving Equipment</li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-icon dots2 list-icon-list list-icon-colored m-l-10">
                                <li>Fall Protection</li>
                                <li>Health Hazards in Construction</li>
                                <li>Excavations</li>
                                <li>Scaffolds</li>
                                <li>Materials Handling, Storage, Use and Disposa</li>
                                <li>Cranes, Derricks, Hoists, Elevators, and Conveyors</li>
                                <li>Fire Protection and Prevention</li>
                                <li>Stairways and Ladders</li>
                                <li>Hand and Power Tools</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="p-t-20 p-b-20">
    <div class="container">
        <div class="section-spacer-30"></div>
        <div class="row text-center">
            <div class="col-lg-12">
                <h2 class="big">Class Schedule</h2>
                <h5 class="big">All classes are scheduled from 8:00 a.m. to 2:30 p.m.*</h5>
                <h5 class="big">Attendees must complete the full 10-hours to receive OSHA-10 Hour Certification.</h5>
            </div>
        </div>
        <div class="section-spacer-40"></div>
        <div class="row">
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Porter, Texas</h5>
                        <h5 class="training-calendar">3/18/2020 - 3/19/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Harbor America Office</p>
                    <p>21977 E. Wallis Drive</p>
                    <p>Porter, TX 77365</p>
                    <p class="m-t-20">
                    <p><b>CANCELLED</b></p>
                    </p>
                    <br>
                    <a name="register"></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Fort Collins, Colorado</h5>
                        <h5 class="training-calendar">3/30/2020 - 3/31/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Hilton Garden Inn – Ft. Collins</p>
                    <p>2821 E. Harmony Road</p>
                    <p>Fort Collins, CO 80528</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                    <a name="register"></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Chandler, Arizona</h5>
                        <h5 class="training-calendar">5/15/2020 - 5/16/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Vensure Office</p>
                    <p>2600 W. Geronimo Place, Suite 100</p>
                    <p>Chandler, AZ 85224</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Orlando, Florida</h5>
                        <h5 class="training-calendar">8/6/2020 - 8/7/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Hampton Inn</p>
                    <p>7500 Futures Dr</p>
                    <p>Orlando, FL 32819</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="section-spacer-10"></div>
        <div class="line-icon">
            <span class="outer-line"></span>
            <span class="fa fa-angle-down icon"></span>
            <span class="outer-line"></span>
        </div>
        <div class="section-spacer-10"></div>
        <div class="row text-center">
            <div class="col-lg-12">
                <h2 class="big">Construction Class Registration</h2>
                <p>This information will be provided to UCSD OSHA Outreach who will issue certification cards to each person who attends and successfully completes 10-hours of training. Please ensure all information is correct as OSHA charges a fee for new card corrections.</p>
            </div>
        </div>
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-12">
                <form class="form-training" novalidate action="<?php echo basePathUrl();?>form-send/training" role="form" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control form-input-home" id="first_name" name="first_name" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control form-input-home" id="last_name" name="last_name" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="class">Class</label>
                            <select id="class" name="class" class="form-control form-input-home" required>
                                <option value="">Select Location and Date</option>
                                 <option value="Fort Collins on 3/30/20 - 3/31/20">Fort Collins on 3/30/20 - 3/31/20</option>
                                <option value="Chandler, AZ on 5/15/20 - 5/16/20">Chandler, AZ on 5/15/20 - 5/16/20</option>
                                <option value="Orlando, FL on 8/6/20 - 8/7/20">Orlando, FL on 8/6/20 - 8/7/20</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="company_email">Company Email</label>
                            <input type="email" class="form-control form-input-home email" id="company_email" name="company_email" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" class="form-control form-input-home" id="phone_number" name="phone_number" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="vensure_client_name">Vensure Client Name</label>
                            <input type="text" class="form-control form-input-home" id="vensure_client_name" name="vensure_client_name" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="client_id">Client ID</label>
                            <input type="text" class="form-control form-input-home" id="client_id" name="client_id" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="company_name">Company Name</label>
                            <input type="text" class="form-control form-input-home" id="company_name" name="company_name" required>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="address">Address</label>
                            <input type="text" class="form-control form-input-home" id="address" name="address" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="city">City</label>
                            <input type="text" class="form-control form-input-home" id="city" name="city" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="state">State</label>
                            <input type="text" class="form-control form-input-home" id="state" name="state" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="zip">Zip</label>
                            <input type="text" class="form-control form-input-home" id="zip" name="zip" required>
                        </div>
                    </div>
                    <div class="section-spacer-20"></div>
                    <div class="form-row">
                        <div class="form-group col-md-12 text-center">
                            <input type="hidden" name="training_type" value="construction">
                            <button type="submit" id="training-bottom" class="btn btn-rounded btn-light">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="bottom-message">*If you are unable to attend, training session cancellations must be received no later than seven business days prior to the class start date.
                    Employees who no-show or submit cancellation requests less than seven days prior to the start date will be charged a $125 fee.</p>
                <p class="bottom-message">To cancel your reservation, please email <a href="mailto:oshacancellation@vensure.com" class="internal">oshacancellation@vensure.com</a>. You will need to provide your full name, email address, class date and time, and Client ID.</p>
            </div>
        </div>
        <div class="section-spacer-20"></div>
    </div>
</section>

