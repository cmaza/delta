<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Large-Business.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Large Entity Solutions</h1>
            <span>A Strategic Partner</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <a href="#"><img src="<?php echo basePathUrl();?>images/Vensure-HR-Large-Business-Img.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Over 500 Employees</h4>
                    <div class="inside-spacer"></div>
                    <p>VensureHR has the experience and resources to remove your HR and back office management concerns. By streamlining essential
                        administrative business tasks and minimizing potential threats, VensureHR offers a cost-effective and employee-centered solution.</p>
                    <p>Gone are the days of fearing common employer risks as we will always provide  guidance as it relates to compliance, administration,
                        and staying up to date with impactful, changing legislations.</p>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Let’s Talk About Achieving Your Business Goals</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Request A Demo</a>
                </p>
            </div>
        </div>
        <div class="section-spacer-40"></div>
    </div>
</section>

