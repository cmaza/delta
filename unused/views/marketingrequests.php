<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/VensureHR-marketing-form-hero.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Marketing Requests</h1>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-right.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-30"></div>
        <div class="row text-center">
            <div class="col-lg-12">
                <iframe width="100%" height="1400px" src="https://www.wrike.com/form/eyJhY2NvdW50SWQiOjMyNzA1NzgsInRhc2tGb3JtSWQiOjI3MzgxMn0JNDczNjQyNTg4MjI4NAlmZjY3YjM4MGZiMDY1ZDIxMTZjNzZmNzg1YmM1NjY3YTU5MzQzMDFjMjQ2NGFhNTM1MzM3NjljMjg4NzE2Y2Uy" frameborder="0"></iframe>
            </div>
        </div>
        <div class="section-spacer-30"></div>
    </div>
</section>

