<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-General-Industry-Training.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">General Industry OSHA Training</h1>
            <span>Safety Begins With Knowledge: Learn To Identify and Abate Common Workplace Hazards</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-right.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-10"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">About the Training</h4>
                <div class="inside-spacer"></div>
                <p>The OSHA 10-Hour General Industry training program provides entry-level general industry workers information on how to identify,
                    abate, avoid and prevent job-related hazards on a job site. The training covers a variety of general industry safety and health
                    hazards that a worker may encounter. The course is designed for workers in most factories and manufacturing operations, healthcare,
                    transportation, warehousing, chemical plants, oil and gas production operations, and service industries.</p>
                <p class="m-t-30 text-center">
                    <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                </p>
            </div>
        </div>
    </div>
</section>

<section class="background-grey p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="blockquote blockquote-color-white">
                    <h3 class="training"><span><i class="far fa-clock"></i></span>How long is the training?</h3>
                    <div class="section-spacer-10"></div>
                    <p>The training takes a minimum of 10-hours to complete. VensureHR’s standard two-day delivery format separates the training into two, five-hour days.*</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="blockquote blockquote-color-white">
                    <h3 class="training">
                        <span><i class="fa fa-check"></i></span>Is the training OSHA approved?
                    </h3>
                    <div class="section-spacer-10"></div>
                    <p>VensureHR’s OSHA-authorized trainers conduct 10 and 30-hour training courses under OSHA’s guidelines and course requirements.
                        Students will receive a UCSD/OSHA issued student course completion and certification card.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="blockquote blockquote-color-white">
                    <div class="row">
                        <div class="col-lg-6">
                            <h3 class="training"><span><i class="fa fa-search"></i></span>Topics Covered</h3>
                            <div class="section-spacer-10"></div>
                            <ul class="list-icon dots2 list-icon-list list-icon-colored m-l-10">
                                <li>Introduction to OSHA</li>
                                <li>Walking and Working Services</li>
                                <li>Fall Protection</li>
                                <li>Exit Routes</li>
                                <li>Emergency Action Plans</li>
                                <li>Fire Prevention Plans</li>
                                <li>Fire Protection</li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-icon dots2 list-icon-list list-icon-colored m-l-10">
                                <li>Identifying Electrical Hazards</li>
                                <li>Electrical Grounding and Protection Standards</li>
                                <li>Personal Protective Equipment (PPE)</li>
                                <li>Respiratory Protection</li>
                                <li>Hazard Communication</li>
                                <li>Hazardous Materials</li>
                                <li>Machine Guarding</li>
                                <li>OSHA Safety and Health Programs</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="p-t-20 p-b-20">
    <div class="container">
        <div class="section-spacer-30"></div>
        <div class="row text-center">
            <div class="col-lg-12">
                <h2 class="big">Class Schedule</h2>
                <h5 class="big">All classes are scheduled from 8:00 a.m. to 2:30 p.m.*</h5>
                <h5 class="big">Attendees must complete the full 10-hours to receive OSHA-10 Hour Certification.</h5>
            </div>
        </div>
        <div class="section-spacer-40"></div>
        <div class="row">
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Vacaville, California</h5>
                        <h5 class="training-calendar">4/1/2020 - 4/2/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Hampton Inn</p>
                    <p>800 Mason St</p>
                    <p>Vacaville, CA 95688</p>
                    <p class="m-t-20">
                    <p><b>CANCELLED</b></p>
                    </p>
                    <br>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Troy, Michigan</h5>
                        <h5 class="training-calendar">5/7/2020 - 5/8/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Hampton Inn</p>
                    <p>100 Wilshire Dr</p>
                    <p>Troy, MI 48084</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Mount Hope, West Virginia</h5>
                        <h5 class="training-calendar">5/28/2020 - 5/29/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>EIN Office</p>
                    <p>5362 Robert C Byrd Dr</p>
                    <p>Mount Hope, WV 25880</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="section-spacer-10"></div>
        <div class="row">
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Ontario, California</h5>
                        <h5 class="training-calendar">6/5/2020 - 6/6/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Hampton Inn</p>
                    <p>4500 E. Mills Circle</p>
                    <p>Ontario, CA 91764</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Montebello, California</h5>
                        <h5 class="training-calendar">6/11/2020 - 6/12/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Quality Inn</p>
                    <p>7709 Telegraph Rd</p>
                    <p>Montebello, CA 90640</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Orlando, Florida</h5>
                        <h5 class="training-calendar">6/18/2020 - 6/19/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Hampton Inn</p>
                    <p>7500 Futures Dr</p>
                    <p>Orlando, FL 32819</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="section-spacer-10"></div>
        <div class="row">
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Duluth, Georgia</h5>
                        <h5 class="training-calendar">7/16/2020 - 7/17/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Vensure Office</p>
                    <p>2425 Commerce Ave, Ste 300</p>
                    <p>Duluth, GA 32819</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                    <a name="register"></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Porter, Texas</h5>
                        <h5 class="training-calendar">10/1/2020 - 10/2/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Harbor America Office</p>
                    <p>21977 E. Wallis Drive</p>
                    <p>Porter, TX 77365</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Ontario, California</h5>
                        <h5 class="training-calendar">10/9/2020 - 10/10/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p>Hampton Inn</p>
                    <p>4500 E. Mills Circle</p>
                    <p>Ontario, CA 91764</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="section-spacer-10"></div>
        <div class="row">
            <div class="col-lg-4">
                <div class="blockquote blockquote-color-grey">
                    <div class="icon-box1 medium color">
                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                        <h5 class="training-calendar">Ontario, California</h5>
                        <h5 class="training-calendar">10/23/2020 - 10/24/2020</h5>
                    </div>
                    <div class="section-spacer-10"></div>
                    <p><b>Presented in Spanish</b></p>
                    <p>Hampton Inn</p>
                    <p>4500 E. Mills Circle</p>
                    <p>Ontario, CA 91764</p>
                    <p class="m-t-20">
                        <a href="#register" class="btn btn-rounded btn-light">Register Now</a>
                    </p>
                </div>
            </div>
            </div>
        <div class="section-spacer-10"></div>
        <div class="line-icon">
            <span class="outer-line"></span>
            <span class="fa fa-angle-down icon"></span>
            <span class="outer-line"></span>
        </div>
        <div class="section-spacer-10"></div>
        <div class="row text-center">
            <div class="col-lg-12">
                <h2 class="big">General Industry Class Registration</h2>
                <p>This information will be provided to UCSD OSHA Outreach who will issue certification cards to each person that attends and successfully completes 10 hours of training.
                    Please ensure all information is correct as OSHA charges a fee for new card corrections.</p>
            </div>
        </div>
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-12">
                <form class="form-training" novalidate action="<?php echo basePathUrl();?>form-send/training" role="form" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control form-input-home" id="first_name" name="first_name" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control form-input-home" id="last_name" name="last_name" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="class">Class</label>
                            <select id="class" name="class" class="form-control form-input-home" required>
                                <option value="">Select Location and Date</option>
                                <option value="Troy, MI on 5/7/20 - 5/8/20">Troy, MI on 5/7/20 - 5/8/20</option>
                                <option value="Mount Hope, WV on 5/28/20 - 5/29/20">Mount Hope, WV on 5/28/20 - 5/29/20</option>
                                <option value="Ontario, CA on 6/5/20 - 6/6/20">Ontario, CA on 6/5/20 - 6/6/20</option>
                                <option value="Montebello, CA on 6/11/20 - 6/12/20">Montebello, CA on 6/11/20 - 6/12/20</option>
                                <option value="Orlando, FL on 6/18/20 - 6/19/20">Orlando, FL on 6/18/20 - 6/19/20</option>
                                <option value="Duluth, GA on 7/16/20 - 7/17/20">Duluth, GA on 7/16/20 - 7/17/20</option>
                                <option value="Porter, TX on 10/1/20 - 10/2/20">Porter, TX on 10/1/20 - 10/2/20</option>
                                <option value="Ontario, CA on 10/9/20 - 10/10/20">Ontario, CA on 10/9/20 - 10/10/20</option>
                                <option value="Ontario, CA on 10/23/20 - 10/24/20">Ontario, CA on 10/23/20 - 10/24/20 presented in Spanish</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="company_email">Company Email</label>
                            <input type="email" class="form-control form-input-home email" id="company_email" name="company_email" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" class="form-control form-input-home" id="phone_number" name="phone_number" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="vensure_client_name">Vensure Client Name</label>
                            <input type="text" class="form-control form-input-home" id="vensure_client_name" name="vensure_client_name" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="client_id">Client ID</label>
                            <input type="text" class="form-control form-input-home" id="client_id" name="client_id" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="company_name">Company Name</label>
                            <input type="text" class="form-control form-input-home" id="company_name" name="company_name" required>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="address">Address</label>
                            <input type="text" class="form-control form-input-home" id="address" name="address" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="city">City</label>
                            <input type="text" class="form-control form-input-home" id="city" name="city" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="state">State</label>
                            <input type="text" class="form-control form-input-home" id="state" name="state" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="zip">Zip</label>
                            <input type="text" class="form-control form-input-home" id="zip" name="zip" required>
                        </div>
                    </div>
                    <div class="section-spacer-20"></div>
                    <div class="form-row">
                        <div class="form-group col-md-12 text-center">
                            <input type="hidden" name="training_type" value="general_industry">
                            <button type="submit" id="training-bottom" class="btn btn-rounded btn-light">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="bottom-message">*If you are unable to attend, training session cancellations must be received no later than seven business days prior to the class start date.
                    Employees who no-show or submit cancellation requests less than seven days prior to the start date will be charged a $125 fee.</p>
                <p class="bottom-message">To cancel your reservation, please email <a href="mailto:oshacancellation@vensure.com" class="internal">oshacancellation@vensure.com</a>.
                    You will need to provide your full name, email address, class date and time, and Client ID.</p>
            </div>
        </div>
        <div class="section-spacer-20"></div>
    </div>
</section>

