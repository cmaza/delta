<div id="slider" class="vensure-slider slider-fullscreen dots-creative" data-height-xs="360">
    <div class="slide kenburns" data-bg-parallax="<?php echo basePathUrl();?>images/home/vensure-landingpage-bg.jpg">
        <div class="bg-overlay" data-style="referrals"></div>
        <div class="container">
            <div class="slide-captions">
                <div class="row">
                    <div class="col-lg-8" data-animate="fadeInUp" data-animate-delay="500">
                        <h1 data-animate="fadeInUp" data-animate-delay="500" class="referrals">Put Perks to Work For Your Business</h1>
                        <h2 data-animate="fadeInUp" data-animate-delay="1000" class="referrals-subtitle">What’s the best way to maximize the impact of your benefits package? With VensureHR’s Perks Program, you can elevate your offering through a comprehensive employee savings and rewards plan. From discounts on movie tickets and theme parks, to hotels and car rentals, employees have private access to more than 300,000 perks in 10,000 cities, available on-the-go.</h2>
                        <h3 data-animate="fadeInUp" data-animate-delay="1500" class="referrals-text"></h3>
                    </div>
                    <div class="col-lg-4" data-animate="fadeInUp" data-animate-delay="1500">
                        <div class="card referrals">
                            <div class="card-body">
                                <h4 class="referrals text-center">Ready to Perk Up <br>Your Benefits?</h4>
                                <form class="form-referrals" novalidate action="<?php echo basePathUrl();?>form-send/referrals" role="form" method="post">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-input-home" id="first_name" name="first_name" placeholder="Name*" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control form-input-home email" id="company_email" name="company_email" placeholder="Company Email*" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-input-home" id="phone_number" name="phone_number" placeholder="Phone Number*" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-input-home" id="company_name" name="company_name" placeholder="Company Name*" required>
                                        </div>
                                    <div class="section-spacer-10"></div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-center">
                                            <button type="submit" id="referrals-bottom" class="btn btn-rounded btn-light">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="page-content">
    <div class="container">
        <div class="grid-system-demo-live">
            <div class="row">
                <div class="col-lg-12 p-t-40 p-b-20">
                    <div class="heading-text heading-section referrals">
                        <h2 class="text-center referrals-text">About Vensure</h2>
                        <div class="line-small"></div>
                    </div>
                </div>
                <div class="col-lg-12 text-center">
                    <img src="<?php echo basePathUrl();?>images/referrals/Vensure-Infographic.jpg" class="img-fluid mx-auto d-block" />
                </div>
                <div class="col-lg-12 p-t-40 p-b-20">
                    <div class="heading-text heading-section referrals">
                        <h2 class="text-center referrals-text">What Our Clients Say</h2>
                        <div class="line-small"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="blockquote blockquote-color-grey referrals-testimony">
                        <div class="icon-box1 medium color">
                            <div class="icon referrals-testimony"><i class="fa fa-quote-right referrals-testimony"></i></div>
                            <p class="referrals-testimony">Vensure has helped us document our processes, provide professional services, and ensures we
                                are able to focus on building our business. Additionally, we have taken advantage of their OSHA training for our employees every year.
                                It’s a great perk that ensures our teams remain compliant.</p>
                        </div>
                    </div>
                    <p class="referrals-testimony-author text-right">Sandy T, Owner</p>
                </div>
                <div class="col-lg-4">
                    <div class="blockquote blockquote-color-grey referrals-testimony">
                        <div class="icon-box1 medium color">
                            <div class="icon referrals-testimony"><i class="fa fa-quote-right referrals-testimony"></i></div>
                            <p class="referrals-testimony">I was certain we would need to stop servicing a large client due to safety concerns. However,
                                Vensure closely guided us in developing and implementing a safety program that turned this situation around. In the past year,
                                we eliminated a common type of injury at our client site. I'm still in awe of what we accomplished together. I can say, with confidence,
                                we have the best PEO in the nation.</p>
                        </div>
                    </div>
                    <p class="referrals-testimony-author text-right">Eli E, CEO</p>
                </div>
                <div class="col-lg-4">
                    <div class="blockquote blockquote-color-grey referrals-testimony">
                        <div class="icon-box1 medium color">
                            <div class="icon referrals-testimony"><i class="fa fa-quote-right referrals-testimony"></i></div>
                            <p class="referrals-testimony">We used other PEOs in the past and had issues that impacted our clients. Since coming to Vensure we finally
                                feel stable, that we are being taken care of, and have good communication and relationships. Vensure has a high standard for customer service,
                                which we now use as a model in our own business.</p>
                        </div>
                    </div>
                    <p class="referrals-testimony-author text-right">Tony Z, Vice President</p>
                </div>
            </div>
        </div>
    </div>
</section>
