//VENSURE Global var
var VENSURE = {},
  $ = jQuery.noConflict();
(function($) {
  "use strict";
  // Predefined Global Variables
  var $window = $(window),
    //Main
    $body = $("body"),
    $section = $("section"),
    //Header
    $topbar = $("#topbar"),
    $header = $("#header"),
    $headerCurrentClasses = $header.attr("class"),
    darkClassRemoved = false,
    //Logo
    headerLogo = $("#logo"),
    //Menu
    $mainMenu = $("#mainMenu"),
    $mainMenuTriggerBtn = $("#mainMenu-trigger a, #mainMenu-trigger button"),
    //Slider
    $vensureSlider = $(".vensure-slider"),
    $carousel = $(".carousel"),
    // Grid Layout
    $gridLayout = $(".grid-layout");
  //Check if header exist
  if ($header.length > 0) {
    var $headerOffsetTop = $header.offset().top;
  }
  var Events = {
    browser: {
      isMobile: function() {
        if (
          navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)
        ) {
          return true;
        } else {
          return false;
        }
      }
    }
  };
  //Settings
  var Settings = {
    isMobile: Events.browser.isMobile,
    submenuLight: $header.hasClass("submenu-light") == true ? true : false,
    menuIsOpen: false,
    menuOverlayOpened: false
  };
  // Window breakpoints
  $(window).breakpoints({
    breakpoints: [
      {
        name: "xs",
        width: 0
      },
      {
        name: "sm",
        width: 576
      },
      {
        name: "md",
        width: 768
      },
      {
        name: "lg",
        width: 1025
      },
      {
        name: "xl",
        width: 1200
      }
    ]
  });
  var currentBreakpoint = $(window).breakpoints("getBreakpoint");
  $body.addClass("breakpoint-" + currentBreakpoint);
  $(window).bind("breakpoint-change", function(breakpoint) {
    $body.removeClass("breakpoint-" + breakpoint.from);
    $body.addClass("breakpoint-" + breakpoint.to);
  });
  $(window).breakpoints("greaterEqualTo", "lg", function() {
    $body.addClass("b--desktop");
    $body.removeClass("b--responsive");
  });
  $(window).breakpoints("lessThan", "lg", function() {
    $body.removeClass("b--desktop");
    $body.addClass("b--responsive");
  });
  VENSURE.core = {
    functions: function() {
      VENSURE.core.rtlStatus();
      VENSURE.core.rtlStatusActivate();
      VENSURE.core.customHeight();
    },
    rtlStatus: function() {
      var $rtlStatusCheck = $("html").attr("dir");
      if ($rtlStatusCheck == "rtl") {
        return true;
      }
      return false;
    },
    rtlStatusActivate: function() {
      if (VENSURE.core.rtlStatus() == true) {
        $("head").append(
          '<link rel="stylesheet" type="text/css" href="css/rtl.css">'
        );
      }
    },
    customHeight: function(setHeight) {
      var $customHeight = $(".custom-height");
      if ($customHeight.length > 0) {
        $customHeight.each(function() {
          var elem = $(this),
            elemHeight = elem.attr("data-height") || 400,
            elemHeightLg = elem.attr("data-height-lg") || elemHeight,
            elemHeightMd = elem.attr("data-height-md") || elemHeightLg,
            elemHeightSm = elem.attr("data-height-sm") || elemHeightMd,
            elemHeightXs = elem.attr("data-height-xs") || elemHeightSm;

          function customHeightBreakpoint(setHeight) {
            if (setHeight) {
              elem = setHeight;
            }
            switch ($(window).breakpoints("getBreakpoint")) {
              case "xs":
                elem.height(elemHeightXs);
                break;
              case "sm":
                elem.height(elemHeightSm);
                break;
              case "md":
                elem.height(elemHeightMd);
                break;
              case "lg":
                elem.height(elemHeightLg);
                break;
              case "xl":
                elem.height(elemHeight);
                break;
            }
          }
          customHeightBreakpoint(setHeight);
          $(window).resize(function() {
            setTimeout(function() {
              customHeightBreakpoint(setHeight);
            }, 100);
          });
        });
      }
    },
  };
  VENSURE.header = {
    functions: function() {
      VENSURE.header.logoStatus();
      VENSURE.header.stickyHeader();
      VENSURE.header.topBar();
      VENSURE.header.mainMenu();
      VENSURE.header.mainMenuOverlay();
      VENSURE.header.sidebarOverlay();
      VENSURE.header.dotsMenu();
    },
    logoStatus: function(status) {
      var headerLogoDefault = headerLogo.find($(".logo-default")),
        headerLogoDark = headerLogo.find($(".logo-dark")),
        headerLogoFixed = headerLogo.find(".logo-fixed"),
        headerLogoResponsive = headerLogo.find(".logo-responsive");

      if ($header.hasClass("header-sticky") && headerLogoFixed.length > 0) {
        headerLogoDefault.css("display", "none");
        headerLogoDark.css("display", "none");
        headerLogoResponsive.css("display", "none");
        headerLogoFixed.css("display", "block");
      } else {
        headerLogoDefault.removeAttr("style");
        headerLogoDark.removeAttr("style");
        headerLogoResponsive.removeAttr("style");
        headerLogoFixed.removeAttr("style");
      }
      $(window).breakpoints("lessThan", "lg", function() {
        if (headerLogoResponsive.length > 0) {
          headerLogoDefault.css("display", "none");
          headerLogoDark.css("display", "none");
          headerLogoFixed.css("display", "none");
          headerLogoResponsive.css("display", "block");
        }
      });
    },
    stickyHeader: function() {
      var elem = $(this),
        shrinkHeader = elem.attr("data-shrink") || 0,
        shrinkHeaderActive = elem.attr("data-sticky-active") || 200,
        scrollOnTop = $window.scrollTop(),
        darkClassRemoved;

      if ($header.hasClass("header-modern")) {
        shrinkHeader = 300;
      }

      $(window).breakpoints("greaterEqualTo", "lg", function() {
        if (!$header.is(".header-disable-fixed")) {
          if (scrollOnTop > $headerOffsetTop + shrinkHeader) {
            $header.addClass("header-sticky");
            if (scrollOnTop > $headerOffsetTop + shrinkHeaderActive) {
              $header.addClass("sticky-active");
              if (Settings.submenuLight) {
                $header.removeClass("dark");
                darkClassRemoved = true;
              }
              VENSURE.header.logoStatus();
            }
          } else {
            $header.removeClass().addClass($headerCurrentClasses);
            VENSURE.header.logoStatus();
          }
        }
      });

      $(window).breakpoints("lessThan", "lg", function() {
        if ($header.attr("data-responsive-fixed") == "true") {
          if (scrollOnTop > $headerOffsetTop + shrinkHeader) {
            $header.addClass("header-sticky");
            if (scrollOnTop > $headerOffsetTop + shrinkHeaderActive) {
              $header.addClass("sticky-active");
              if (Settings.submenuLight) {
                $header.removeClass("dark");
                darkClassRemoved = true;
              }
              VENSURE.header.logoStatus();
            }
          } else {
              $header.removeClass().addClass($headerCurrentClasses);
              if(darkClassRemoved == true && $body.hasClass("mainMenu-open")) { $header.removeClass("dark");}
              VENSURE.header.logoStatus();
          }
        }
      });
    },
    //chkd
    topBar: function() {
      if ($topbar.length > 0) {
        $("#topbar .topbar-dropdown .topbar-form").each(function (index, element) {
          if ($window.width() - ($(element).width() + $(element).offset().left) < 0) {
            $(element).addClass("dropdown-invert");
          }
        });
      }
    },
    mainMenu: function () {
          if ($mainMenu.length > 0) {
              $mainMenu.find(".dropdown, .dropdown-submenu").prepend('<span class="dropdown-arrow"></span>');

              var $menuItemLinks = $('#mainMenu nav > ul > li.dropdown > a[href="#"], #mainMenu nav > ul > li.dropdown > .dropdown-arrow, .dropdown-submenu > a[href="#"], .dropdown-submenu > .dropdown-arrow, .dropdown-submenu > span'),
                  $triggerButton = $("#mainMenu-trigger a, #mainMenu-trigger button"),
                  processing = false,
                  triggerEvent;

              $triggerButton.on("click", function (e) {
                  var elem = $(this);
                  e.preventDefault();
                  $(window).breakpoints("lessThan", "lg", function () {
                      var openMenu = function () {
                          if (!processing) {
                              processing = true;
                              Settings.menuIsOpen = true;
                              if (Settings.submenuLight) {
                                  $header.removeClass("dark");
                                  darkClassRemoved = true;
                              }
                              elem.addClass("toggle-active");
                              $body.addClass("mainMenu-open");
                              VENSURE.header.logoStatus();
                              $mainMenu.animate({
                                  "min-height": $window.height()
                              }, {
                                  duration: 500,
                                  easing: "easeInOutQuart",
                                  start: function () {
                                      setTimeout(function () {
                                          $mainMenu.addClass("menu-animate");
                                      }, 300);
                                  },
                                  complete: function () {
                                      processing = false;
                                  }
                              })
                          }
                      }
                      var closeMenu = function () {
                          if (!processing) {
                              processing = true;
                              Settings.menuIsOpen = false;
                              VENSURE.header.logoStatus();
                              $mainMenu.animate({
                                  "min-height": 0
                              }, {
                                  start: function () {
                                      $mainMenu.removeClass("menu-animate");
                                  },
                                  done: function () {
                                      $body.removeClass("mainMenu-open");
                                      elem.removeClass("toggle-active");
                                      if (Settings.submenuLight && darkClassRemoved && !$header.hasClass("header-sticky")) {
                                          $header.addClass("dark");
                                      }
                                  },
                                  duration: 500,
                                  easing: "easeInOutQuart",
                                  complete: function () {
                                      processing = false;
                                  }
                              })
                          }
                      }
                      if (!Settings.menuIsOpen) {
                          triggerEvent = openMenu();
                      } else {
                          triggerEvent = closeMenu();
                      }
                  })
              })
              if ($body.hasClass("b--responsive") || $mainMenu.hasClass("menu-onclick")) {
                  $menuItemLinks.on("click", function (e) {
                      $(this).parent("li").siblings().removeClass("hover-active");
                      $(this).parent("li").toggleClass("hover-active");
                      e.stopPropagation();
                      e.preventDefault();
                  });
              };
              $body.on("click", function (e) {
                  $mainMenu.find(".hover-active").removeClass("hover-active");
              });
              /*invert menu fix*/
              $(window).breakpoints("greaterEqualTo", "lg", function () {
                  var $menuLastItem = $("nav > ul > li:last-child"),
                      $menuLastItemUl = $("nav > ul > li:last-child > ul"),
                      $menuLastInvert = $menuLastItemUl.width() - $menuLastItem.width(),
                      $menuItems = $("nav > ul > li").find(".dropdown-menu");

                  $menuItems.css("display", "block");

                  $(".dropdown:not(.mega-menu-item) ul ul").each(function (index, element) {
                      if ($window.width() - ($(element).width() + $(element).offset().left) < 0) {
                          $(element).addClass("menu-invert");
                      }
                  })
                  if ($window.width() - ($menuLastItemUl.width() + $menuLastItem.offset().left) < 0) {
                      $menuLastItemUl.addClass("menu-last");
                  }
                  $menuItems.css("display", "");
              })
          }

      },
    mainMenuOverlay: function() {
    },
    sidebarOverlay: function() {
      var sidebarOverlay = $("#side-panel");
      if (sidebarOverlay.length > 0) {
        sidebarOverlay.css("opacity", 1);
        $("#close-panel").on("click", function() {
          $body.removeClass("side-panel-active");
          $("#side-panel-trigger").removeClass("toggle-active");
        });
      }


      var $sidepanel = $("#sidepanel"),
      $sidepanelTrigger = $(".panel-trigger"),
      sidepanelProcessing = false,
      sidepanelEvent;

    $sidepanelTrigger.on("click", function(e) {
      e.preventDefault();
      var panelOpen = function() {
        if (!sidepanelProcessing) {
          sidepanelProcessing = true;
          Settings.panelIsOpen = true;
          $sidepanel.addClass("panel-open");
          sidepanelProcessing = false;
        }
      };
      var panelClose = function() {
        if (!sidepanelProcessing) {
          sidepanelProcessing = true;
          Settings.panelIsOpen = false;
          $sidepanel.removeClass("panel-open");
          sidepanelProcessing = false;
        }
      };
      if (!Settings.panelIsOpen) {
        sidepanelEvent = panelOpen();
      } else {
        sidepanelEvent = panelClose();
      }
    });


    },
    dotsMenu: function () {
      var $dotsMenu = $("#dotsMenu"),
        $dotsMenuItems = $dotsMenu.find("ul > li > a");
      if ($dotsMenu.length > 0) {
        $dotsMenuItems.on("click", function () {
          $dotsMenuItems.parent("li").removeClass("current");
          $(this).parent("li").addClass("current");
          return false;
        })
        $dotsMenuItems.parents("li").removeClass("current");
        $dotsMenu.find('a[href="#' + VENSURE.header.currentSection() + '"]').parent("li").addClass("current");
      }
    },
    currentSection: function () {
      var elemCurrent = "body";
      $section.each(function () {
        var elem = $(this),
          elemeId = elem.attr("id");
        if (elem.offset().top - $window.height() / 3 < $window.scrollTop() && elem.offset().top + elem.height() - $window.height() / 3 > $window.scrollTop()) {
          elemCurrent = elemeId;
        }
      })
      return elemCurrent;
    }
  };
  VENSURE.slider = {
    functions: function() {
      VENSURE.slider.vensureSlider();
      VENSURE.slider.carousel();
    },
    vensureSlider: function() {
      if ($vensureSlider.length > 0) {
        //Check if flickity plugin is loaded
        if (typeof $.fn.flickity === "undefined") {
          VENSURE.elements.notification(
            "Warning",
            "jQuery flickity slider plugin is missing in plugins.js file.",
            "danger"
          );
          return true;
        }
        var defaultAnimation = "fadeInUp";

        function animate_captions($elem) {
          var $captions = $elem;
          $captions.each(function() {
            var $captionElem = $(this),
              animationDuration = "600ms";
            if ($(this).attr("data-animate-duration")) {
              animationDuration = $(this).attr("data-animate-duration") + "ms";
            }
            $captionElem.css({
              opacity: 0
            });
            $(this).css("animation-duration", animationDuration);
          });
          $captions.each(function(index) {
            var $captionElem = $(this),
              captionDelay =
                $captionElem.attr("data-caption-delay") || index * 350 + 1000,
              captionAnimation =
                $captionElem.attr("data-caption-animate") || defaultAnimation;
            var t = setTimeout(function() {
              $captionElem.css({
                opacity: 1
              });
              $captionElem.addClass(captionAnimation);
            }, captionDelay);
          });
        }

        function hide_captions($elem) {
          var $captions = $elem;
          $captions.each(function(caption) {
            var caption = $(this),
              captionAnimation =
                caption.attr("data-caption-animate") || defaultAnimation;
            caption.removeClass(captionAnimation);
            caption.removeAttr("style");
          });
        }

        function start_kenburn(elem) {
          var currentSlide = elem.find(".slide.is-selected"),
            currentSlideKenburns = currentSlide.hasClass("kenburns");
          if (currentSlideKenburns) {
            setTimeout(function() {
              currentSlide.find(".kenburns-bg").addClass("kenburns-bg-animate");
            }, 1500);
          }
        }

        function stop_kenburn(elem) {
          var notCurrentSlide = elem.find(".slide:not(.is-selected)");
          notCurrentSlide
            .find(".kenburns-bg")
            .removeClass("kenburns-bg-animate");
        }

        function slide_dark(elem) {
          var $sliderClassSlide = elem.find(".slide.is-selected");
          if ($sliderClassSlide.hasClass("slide-dark")) {
            $header.removeClass("dark").addClass("dark-removed");
          } else {
            if (
              !$header.hasClass("sticky-active") &&
              $header.hasClass("dark-removed")
            ) {
              $header.addClass("dark").removeClass("dark-removed");
            }
          }
        }

        function sliderHeight(elem, state) {
          var elem,
            headerHeight = $header.outerHeight(),
            topbarHeight = $topbar.outerHeight() || 0,
            windowHeight = $window.height(),
            sliderCurrentHeight = elem.height(),
            screenHeightExtra = headerHeight + topbarHeight,
            $sliderClassSlide = elem.find(".slide"),
            sliderFullscreen = elem.hasClass("slider-fullscreen"),
            screenRatio = elem.hasClass("slider-halfscreen") ? 1 : 1.2,
            transparentHeader = $header.attr("data-transparent"),
            customHeight = elem.attr("data-height"),
            responsiveHeightXs = elem.attr("data-height-xs"),
            containerFullscreen = elem
              .find(".container")
              .first()
              .outerHeight(),
            contentCrop;
          if (containerFullscreen >= windowHeight) {
            contentCrop = true;
            var sliderMinHeight = containerFullscreen;
            elem.css("min-height", sliderMinHeight + 100);
            $sliderClassSlide.css("min-height", sliderMinHeight + 100);
            elem
              .find(".flickity-viewport")
              .css("min-height", sliderMinHeight + 100);
          }
          sliderElementsHeight("null");

          function sliderElementsHeight(height) {
            if (height == "null") {
              elem.css("height", "");
              $sliderClassSlide.css("height", "");
              elem.find(".flickity-viewport").css("height", "");
            } else {
              elem.css("height", height);
              $sliderClassSlide.css("height", height);
              elem.find(".flickity-viewport").css("height", height);
            }
          }
          if (customHeight) {
            $(window).breakpoints("greaterEqualTo", "lg", function() {
              sliderElementsHeight(customHeight + "px");
            });
          }
        }
        $vensureSlider.each(function() {
          var elem = $(this);
          //Plugin Options
          elem.options = {
            cellSelector: elem.attr("data-item") || false,
            prevNextButtons: elem.data("arrows") == false ? false : true,
            pageDots: elem.data("dots") == false ? false : true,
            fade: elem.data("fade") == true ? true : false,
            draggable: elem.data("drag") == true ? true : false,
            freeScroll: elem.data("free-scroll") == true ? true : false,
            wrapAround: elem.data("loop") == false ? false : true,
            groupCells: elem.data("group-cells") == true ? true : false,
            autoPlay: elem.attr("data-autoplay") || 7000,
            pauseAutoPlayOnHover:
              elem.data("hoverpause") == true ? true : false,
            adaptiveHeight:
              elem.data("adaptive-height") == false ? false : false,
            asNavFor: elem.attr("data-navigation") || false,
            selectedAttraction: elem.attr("data-attraction") || 0.07,
            friction: elem.attr("data-friction") || 0.9,
            initialIndex: elem.attr("data-initial-index") || 0,
            accessibility: elem.data("accessibility") == true ? true : false,
            setGallerySize: elem.data("gallery-size") == false ? false : false,
            resize: elem.data("resize") == false ? false : false,
            cellAlign: elem.attr("data-align") || "left",
            playWholeVideo:
              elem.attr("data-play-whole-video") == false ? false : true
          };
          // Kenburns effect
          elem.find(".slide").each(function() {
            if ($(this).hasClass("kenburns")) {
              var elemChild = $(this),
                elemChildImage = elemChild
                  .css("background-image")
                  .replace(/.*\s?url\([\'\"]?/, "")
                  .replace(/[\'\"]?\).*/, "");

              if (elemChild.attr("data-bg-image")) {
                elemChildImage = elemChild.attr("data-bg-image");
              }
              elemChild.prepend(
                '<div class="kenburns-bg" style="background-image:url(' +
                  elemChildImage +
                  ')"></div>'
              );
            }
          });
          elem.find(".slide video").each(function() {
            this.pause();
          });
          $(window).breakpoints("lessThan", "lg", function() {
            elem.options.draggable = true;
          });


          if (elem.find(".slide").length <= 1) {
            elem.options.prevNextButtons = false;
            elem.options.pageDots = false;
            elem.options.autoPlay = false;
            elem.options.draggable = false;
          }

        
          if (
            !$.isNumeric(elem.options.autoPlay) &&
            elem.options.autoPlay != false
          ) {
            elem.options.autoPlay = Number(7000);
          }

          var vensureSliderData = elem.flickity({
            cellSelector: elem.options.cellSelector,
            prevNextButtons: elem.options.prevNextButtons,
            pageDots: elem.options.pageDots,
            fade: elem.options.fade,
            draggable: elem.options.draggable,
            freeScroll: elem.options.freeScroll,
            wrapAround: elem.options.wrapAround,
            groupCells: elem.options.groupCells,
            autoPlay: Number(elem.options.autoPlay),
            pauseAutoPlayOnHover: elem.options.pauseAutoPlayOnHover,
            adaptiveHeight: elem.options.adaptiveHeight,
            asNavFor: elem.options.asNavFor,
            selectedAttraction: Number(elem.options.selectedAttraction),
            friction: elem.options.friction,
            initialIndex: elem.options.initialIndex,
            accessibility: elem.options.accessibility,
            setGallerySize: elem.options.setGallerySize,
            resize: elem.options.resize,
            cellAlign: elem.options.cellAlign,
            rightToLeft: VENSURE.core.rtlStatus(),
            on: {
              ready: function(index) {
                var $captions = elem.find(
                  ".slide.is-selected .slide-captions > *"
                );
                sliderHeight(elem);
                start_kenburn(elem);
                animate_captions($captions);
                setTimeout(function() {
                  elem.find(".slide video").each(function(i, video) {
                    video.pause();
                    video.currentTime = 0;
                  });
                }, 700);
              }
            }
          });

          var flkty = vensureSliderData.data("flickity");

          vensureSliderData.on("change.flickity", function() {
            var $captions = elem.find(".slide.is-selected .slide-captions > *");
            hide_captions($captions);
            setTimeout(function() {
              stop_kenburn(elem);
            }, 500);
            slide_dark(elem);
            start_kenburn(elem);
            animate_captions($captions);
            elem.find(".slide video").each(function(i, video) {
              video.currentTime = 0;
            });
          });

          vensureSliderData.on("select.flickity", function() {
            VENSURE.elements.backgroundImage();
            var $captions = elem.find(".slide.is-selected .slide-captions > *");
            sliderHeight(elem);
            start_kenburn(elem);
            animate_captions($captions);
            var video = flkty.selectedElement.querySelector("video");
            if (video) {
              video.play();
              flkty.options.autoPlay = Number(video.duration * 1000);
            } else {
              flkty.options.autoPlay = Number(elem.options.autoPlay);
            }
          });
          vensureSliderData.on("dragStart.flickity", function() {
            var $captions = elem.find(
              ".slide:not(.is-selected) .slide-captions > *"
            );
            hide_captions($captions);
          });
          $(window).resize(function() {
            sliderHeight(elem);
            elem.flickity("reposition");
          });
        });
      }
    },
    carouselAjax: function() {
      // Check if flickity plugin is loaded
      if (typeof $.fn.flickity === "undefined") {
        VENSURE.elements.notification(
          "Warning",
          "jQuery flickity plugin is missing in plugins.js file.",
          "danger"
        );
        return true;
      }
      // Plugin Options
      var elem = $(".carousel");
      // Initializing plugin and passing the options
      var $carouselAjax = $(elem).imagesLoaded(function() {
        // init Isotope after all images have loaded
        $carouselAjax.flickity({
          adaptiveHeight: false,
          wrapAround: true,
          cellAlign: "left",
          resize: true
        });
        elem.addClass("carousel-loaded");
      });
    },
    carousel: function(elem) {
      if (elem) {
        $carousel = elem;
      }

      if ($carousel.length > 0) {
        // Check if flickity plugin is loaded
        if (typeof $.fn.flickity === "undefined") {
          VENSURE.elements.notification(
            "Warning",
            "jQuery flickity plugin is missing in plugins.js file.",
            "danger"
          );
          return true;
        }
        $carousel.each(function() {
          var elem = $(this);
          // Plugin Options
          elem.options = {
            containerWidth: elem.width(),
            items: elem.attr("data-items") || 4,
            itemsLg: elem.attr("data-items-lg"),
            itemsMd: elem.attr("data-items-md"),
            itemsSm: elem.attr("data-items-sm"),
            itemsXs: elem.attr("data-items-xs"),
            margin: elem.attr("data-margin") || 10,
            cellSelector: elem.attr("data-item") || false,
            prevNextButtons: elem.data("arrows") == false ? false : true,
            pageDots: elem.data("dots") == false ? false : true,
            fade: elem.data("fade") == true ? true : false,
            draggable: elem.data("drag") == false ? false : true,
            freeScroll: elem.data("free-scroll") == true ? true : false,
            wrapAround: elem.data("loop") == false ? false : true,
            groupCells: elem.data("group-cells") == true ? true : false,
            autoPlay: elem.attr("data-autoplay") || 5000,
            pauseAutoPlayOnHover:
              elem.data("hover-pause") == false ? false : true,
            asNavFor: elem.attr("data-navigation") || false,
            lazyLoad: elem.data("lazy-load") == true ? true : false,
            initialIndex: elem.attr("data-initial-index") || 0,
            accessibility: elem.data("accessibility") == true ? true : false,
            adaptiveHeight: elem.data("adaptive-height") == true ? true : false,
            autoWidth: elem.data("auto-width") == true ? true : false,
            setGallerySize: elem.data("gallery-size") == false ? false : true,
            resize: elem.data("resize") == false ? false : true,
            cellAlign: elem.attr("data-align") || "left",
            rightToLeft: VENSURE.core.rtlStatus()
          };

          //Calculate min/max on responsive breakpoints
          elem.options.itemsLg =
            elem.options.itemsLg ||
            Math.min(Number(elem.options.items), Number(4));
          elem.options.itemsMd =
            elem.options.itemsMd ||
            Math.min(Number(elem.options.itemsLg), Number(3));
          elem.options.itemsSm =
            elem.options.itemsSm ||
            Math.min(Number(elem.options.itemsMd), Number(2));
          elem.options.itemsXs =
            elem.options.itemsXs ||
            Math.min(Number(elem.options.itemsSm), Number(1));
          var setResponsiveColumns;

          function getCarouselColumns() {
            switch ($(window).breakpoints("getBreakpoint")) {
              case "xs":
                setResponsiveColumns = Number(elem.options.itemsXs);
                break;
              case "sm":
                setResponsiveColumns = Number(elem.options.itemsSm);
                break;
              case "md":
                setResponsiveColumns = Number(elem.options.itemsMd);
                break;
              case "lg":
                setResponsiveColumns = Number(elem.options.itemsLg);
                break;
              case "xl":
                setResponsiveColumns = Number(elem.options.items);
                break;
            }
          }
          getCarouselColumns();
          var itemWidth;
          elem.find("> *").wrap('<div class="polo-carousel-item">');
          if (elem.hasClass("custom-height")) {
            elem.options.setGallerySize = false;
            VENSURE.core.customHeight(elem);
            VENSURE.core.customHeight(elem.find(".polo-carousel-item"));
            var carouselCustomHeightStatus = true;
          }
          if (Number(elem.options.items) !== 1) {
            if (elem.options.autoWidth || carouselCustomHeightStatus) {
              elem.find(".polo-carousel-item").css({
                "padding-right": elem.options.margin + "px"
              });
            } else {
              itemWidth =
                (elem.options.containerWidth + Number(elem.options.margin)) /
                setResponsiveColumns;
              elem.find(".polo-carousel-item").css({
                width: itemWidth,
                "padding-right": elem.options.margin + "px"
              });
            }
          } else {
            elem.find(".polo-carousel-item").css({
              width: "100%",
              "padding-right": "0 !important;"
            });
          }
          if (elem.options.autoWidth || carouselCustomHeightStatus) {
            elem.options.cellAlign = "center";
          }

          if (elem.options.autoPlay == "false") {
            elem.options.autoPlay = false;
          }
          //Initializing plugin and passing the options
          var $carouselElem = $(elem);
          //   .imagesLoaded(function() {
          // init Isotope after all images have loaded
          $carouselElem.flickity({
            cellSelector: elem.options.cellSelector,
            prevNextButtons: elem.options.prevNextButtons,
            pageDots: elem.options.pageDots,
            fade: elem.options.fade,
            draggable: elem.options.draggable,
            freeScroll: elem.options.freeScroll,
            wrapAround: elem.options.wrapAround,
            groupCells: elem.options.groupCells,
            autoPlay: elem.options.autoPlay,
            pauseAutoPlayOnHover: elem.options.pauseAutoPlayOnHover,
            adaptiveHeight: elem.options.adaptiveHeight,
            asNavFor: elem.options.asNavFor,
            initialIndex: elem.options.initialIndex,
            accessibility: elem.options.accessibility,
            setGallerySize: elem.options.setGallerySize,
            resize: elem.options.resize,
            cellAlign: elem.options.cellAlign,
            rightToLeft: elem.options.rightToLeft,
            contain: true
          });
          elem.addClass("carousel-loaded");
          //  });
          if (elem.hasClass("custom-height")) {
            VENSURE.core.customHeight(elem);
          }
          if (Number(elem.options.items) !== 1) {
            $(window).on("resize", function() {
              setTimeout(function() {
                getCarouselColumns();
                itemWidth =
                  (elem.width() + Number(elem.options.margin)) /
                  setResponsiveColumns;
                if (elem.options.autoWidth || carouselCustomHeightStatus) {
                  elem.find(".polo-carousel-item").css({
                    "padding-right": elem.options.margin + "px"
                  });
                } else {
                  if (!elem.hasClass("custom-height")) {
                    elem.find(".polo-carousel-item").css({
                      width: itemWidth,
                      "padding-right": elem.options.margin + "px"
                    });
                  } else {
                    VENSURE.core.customHeight(elem.find(".polo-carousel-item"));
                    elem.find(".polo-carousel-item").css({
                      width: itemWidth,
                      "padding-right": elem.options.margin + "px"
                    });
                  }
                }
                elem.find(".flickity-slider").css({
                  "margin-right":
                    -elem.options.margin / setResponsiveColumns + "px"
                });
                elem.flickity("reposition");
              }, 100);
            });
          }
        });
      }
    }
  };
  VENSURE.elements = {
    functions: function() {
      VENSURE.elements.naTo();
      VENSURE.elements.buttons();
      VENSURE.elements.accordion();
      VENSURE.elements.animations();
      VENSURE.elements.parallax();
      VENSURE.elements.parallaxHome();
      VENSURE.elements.backgroundImage();
      VENSURE.elements.responsiveVideos();
      VENSURE.elements.counters();
      VENSURE.elements.countdownTimer();
      VENSURE.elements.gridLayout();
      VENSURE.elements.magnificPopup();
      VENSURE.elements.modal();
      VENSURE.elements.sidebarFixed();
      VENSURE.elements.countdown();
      VENSURE.elements.other();
      VENSURE.elements.forms();
      VENSURE.elements.formValidation();
      VENSURE.elements.formAjaxProcessing();
      VENSURE.elements.formMarketplace();
      VENSURE.elements.formTraining();
      VENSURE.elements.formContact();
      VENSURE.elements.formReferrals();
      VENSURE.elements.formHRDownload();
      VENSURE.elements.formFreeDiagnostic();
      VENSURE.elements.formFreeDiagnosticHome();
    },
    forms: function() {
      //Show hide password
      var $showHidePassword = $(".show-hide-password");
      if ($showHidePassword.length > 0) {
        $showHidePassword.each(function() {
          var elem = $(this),
            $iconEye = "icon-eye11",
            $iconClosedEye = "icon-eye-off",
            elemShowHideIcon = elem.find(".input-group-append i"),
            elemInput = elem.children("input");
          elem.find(".input-group-append i").css({
            cursor: "pointer"
          });
          elemShowHideIcon.on("click", function(event) {
            event.preventDefault();
            if (elem.children("input").attr("type") == "text") {
              elemInput.attr("type", "password");
              elemShowHideIcon.removeClass($iconEye);
              elemShowHideIcon.addClass($iconClosedEye);
            } else if (elem.children("input").attr("type") == "password") {
              elemInput.attr("type", "text");
              elemShowHideIcon.addClass($iconEye);
              elemShowHideIcon.removeClass($iconClosedEye);
            }
          });
        });
      }
    },
    formValidation: function() {
      var forms = document.getElementsByClassName("needs-validation");
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener(
          "submit",
          function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add("was-validated");
          },
          false
        );
      });
    },
    formAjaxProcessing: function() {
      var $ajaxForm = $(".ghdfghdfgdddd, .ajax-form");
      if ($ajaxForm.length > 0) {
        $ajaxForm.each(function() {
          var elem = $(this),
            elemSuccessMessage =
              elem.attr("data-success-message") ||
              "We have <strong>successfully</strong> received your Message and will get Back to you as soon as possible.",
            elemCustomRedirectPage = elem.attr("data-success-page");
          var button = elem.find("button#dfghdfghdfgh"),
            buttonText = button.html();
          var validation = Array.prototype.filter.call(elem, function(form) {
            form.addEventListener(
              "submit",
              function(event) {
                if (form[0].checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add("was-validated");
                return false;
              },
              false
            );
          });
          elem.submit(function(event) {
            event.preventDefault();
            var post_url = $(this).attr("action");
            var request_method = $(this).attr("method");
            var form_data = $(this).serialize();
            if (elem[0].checkValidity() === false) {
              event.stopPropagation();
              elem.addClass("was-validated");
            } else {
              $(elem).removeClass("was-validated");
              button.html('<i class="icon-loader fa-spin"> </i> Sending...');
              $.ajax({
                url: post_url,
                type: request_method,
                data: form_data,
                success: function(text) {
                  if (text.response == "success") {
                    if (elem.find(".g-recaptcha").children("div").length > 0) {
                      grecaptcha.reset();
                    }
                    $(elem)[0].reset();
                    button.html(buttonText);
                    if (elemCustomRedirectPage) {
                      window.location.href = elemCustomRedirectPage;
                    } else {
                      $.notify(
                        {
                          message: elemSuccessMessage
                        },
                        {
                          type: "success"
                        }
                      );
                    }
                  } else {
                    $.notify(
                      {
                        message: elem.attr("data-error-message") || text.message
                      },
                      {
                        type: "danger"
                      }
                    );
                    var t = setTimeout(function() {
                      button.html(buttonText);
                    }, 1000);
                  }
                }
              });
            }
          });
        });
      }
    },
    formMarketplace: function() {
        var $ajaxForm = $(".form-marketplace, .ajax-form");
        if ($ajaxForm.length > 0) {
            $ajaxForm.each(function() {
                var elem = $(this),
                    elemSuccessMessage =
                        elem.attr("data-success-message") ||
                        "Thank you for your message we will get back to you as soon as possible.",
                    elemCustomRedirectPage = elem.attr("data-success-page");
                var button = elem.find("button#marketplace-bottom"),
                    buttonText = button.html();
                var validation = Array.prototype.filter.call(elem, function(form) {
                    form.addEventListener(
                        "submit",
                        function(event) {
                            if (form[0].checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add("was-validated");
                            return false;
                        },
                        false
                    );
                });
                elem.submit(function(event) {
                    event.preventDefault();
                    var post_url = $(this).attr("action");
                    var request_method = $(this).attr("method");
                    var form_data = $(this).serialize();
                    if (elem[0].checkValidity() === false) {
                        event.stopPropagation();
                        elem.addClass("was-validated");
                    } else {
                        $(elem).removeClass("was-validated");
                        button.html('<i class="icon-loader fa-spin"> </i> Sending...');
                        $.ajax({
                            url: post_url,
                            type: request_method,
                            data: form_data,
                            success: function(text) {
                                if (text.response === "success") {
                                    $(elem)[0].reset();
                                    button.html(buttonText);
                                    if (elemCustomRedirectPage) {
                                        window.location.href = elemCustomRedirectPage;
                                    } else {
                                        $.notify(
                                            {message: elemSuccessMessage},
                                            {type: "success"}
                                        );
                                    }
                                } else {
                                    $.notify(
                                        {message: elem.attr("data-error-message") || text.message},
                                        {type: "danger"}
                                    );
                                    var t = setTimeout(function() {
                                        button.html(buttonText);
                                    }, 1000);
                                }
                            }
                        });
                    }
                });
            });
        }
      },
    formTraining: function() {
          var $ajaxForm = $(".form-training, .ajax-form");
          if ($ajaxForm.length > 0) {
              $ajaxForm.each(function() {
                  var elem = $(this),
                      elemSuccessMessage =
                          elem.attr("data-success-message") ||
                          "Thank you for your message we will get back to you as soon as possible.",
                      elemCustomRedirectPage = elem.attr("data-success-page");
                  var button = elem.find("button#training-bottom"),
                      buttonText = button.html();
                  var validation = Array.prototype.filter.call(elem, function(form) {
                      form.addEventListener(
                          "submit",
                          function(event) {
                              if (form[0].checkValidity() === false) {
                                  event.preventDefault();
                                  event.stopPropagation();
                              }
                              form.classList.add("was-validated");
                              return false;
                          },
                          false
                      );
                  });
                  elem.submit(function(event) {
                      event.preventDefault();
                      var post_url = $(this).attr("action");
                      var request_method = $(this).attr("method");
                      var form_data = $(this).serialize();
                      if (elem[0].checkValidity() === false) {
                          event.stopPropagation();
                          elem.addClass("was-validated");
                      } else {
                          $(elem).removeClass("was-validated");
                          button.html('<i class="icon-loader fa-spin"> </i> Sending...');
                          $.ajax({
                              url: post_url,
                              type: request_method,
                              data: form_data,
                              success: function(text) {
                                  if (text.response === "success") {
                                      $(elem)[0].reset();
                                      button.html(buttonText);
                                      if (elemCustomRedirectPage) {
                                          window.location.href = elemCustomRedirectPage;
                                      } else {
                                          $.notify(
                                              {
                                                  message: elemSuccessMessage
                                              },
                                              {
                                                  type: "success"
                                              }
                                          );
                                      }
                                  } else {
                                      $.notify(
                                          {
                                              message: elem.attr("data-error-message") || text.message
                                          },
                                          {
                                              type: "danger"
                                          }
                                      );
                                      var t = setTimeout(function() {
                                          button.html(buttonText);
                                      }, 1000);
                                  }
                              }
                          });
                      }
                  });
              });
          }
      },
    formContact: function() {
          var $ajaxForm = $(".form-contact, .ajax-form");
          if ($ajaxForm.length > 0) {
              $ajaxForm.each(function() {
                  var elem = $(this),
                      elemSuccessMessage =
                          elem.attr("data-success-message") ||
                          "Thank you for your message we will get back to you as soon as possible.",
                      elemCustomRedirectPage = elem.attr("data-success-page");
                  var button = elem.find("button#contact-bottom"),
                      buttonText = button.html();
                  var validation = Array.prototype.filter.call(elem, function(form) {
                      form.addEventListener(
                          "submit",
                          function(event) {
                              if (form[0].checkValidity() === false) {
                                  event.preventDefault();
                                  event.stopPropagation();
                              }
                              form.classList.add("was-validated");
                              return false;
                          },
                          false
                      );
                  });
                  elem.submit(function(event) {
                      event.preventDefault();
                      var post_url = $(this).attr("action");
                      var request_method = $(this).attr("method");
                      var form_data = $(this).serialize();
                      if (elem[0].checkValidity() === false) {
                          event.stopPropagation();
                          elem.addClass("was-validated");
                      } else {
                          $(elem).removeClass("was-validated");
                          button.html('<i class="icon-loader fa-spin"> </i> Sending...');
                          $.ajax({
                              url: post_url,
                              type: request_method,
                              data: form_data,
                              success: function(text) {
                                  if (text.response === "success") {
                                      $(elem)[0].reset();
                                      button.html(buttonText);
                                      if (elemCustomRedirectPage) {
                                          window.location.href = elemCustomRedirectPage;
                                      } else {
                                          $.notify(
                                              {
                                                  message: elemSuccessMessage
                                              },
                                              {
                                                  type: "success"
                                              }
                                          );
                                      }
                                  } else {
                                      $.notify(
                                          {
                                              message: elem.attr("data-error-message") || text.message
                                          },
                                          {
                                              type: "danger"
                                          }
                                      );
                                      var t = setTimeout(function() {
                                          button.html(buttonText);
                                      }, 1000);
                                  }
                              }
                          });
                      }
                  });
              });
          }
      },
    formReferrals: function() {
          var $ajaxForm = $(".form-referrals, .ajax-form");
          if ($ajaxForm.length > 0) {
              $ajaxForm.each(function() {
                  var elem = $(this),
                      elemSuccessMessage =
                          elem.attr("data-success-message") ||
                          "Thank you for your message we will get back to you as soon as possible.",
                      elemCustomRedirectPage = elem.attr("data-success-page");
                  var button = elem.find("button#referrals-bottom"),
                      buttonText = button.html();
                  var validation = Array.prototype.filter.call(elem, function(form) {
                      form.addEventListener(
                          "submit",
                          function(event) {
                              if (form[0].checkValidity() === false) {
                                  event.preventDefault();
                                  event.stopPropagation();
                              }
                              form.classList.add("was-validated");
                              return false;
                          },
                          false
                      );
                  });
                  elem.submit(function(event) {
                      event.preventDefault();
                      var post_url = $(this).attr("action");
                      var request_method = $(this).attr("method");
                      var form_data = $(this).serialize();
                      if (elem[0].checkValidity() === false) {
                          event.stopPropagation();
                          elem.addClass("was-validated");
                      } else {
                          $(elem).removeClass("was-validated");
                          button.html('<i class="icon-loader fa-spin"> </i> Sending...');
                          $.ajax({
                              url: post_url,
                              type: request_method,
                              data: form_data,
                              success: function(text) {
                                  if (text.response === "success") {
                                      $(elem)[0].reset();
                                      button.html(buttonText);
                                      if (elemCustomRedirectPage) {
                                          window.location.href = elemCustomRedirectPage;
                                      } else {
                                          $.notify(
                                              {
                                                  message: elemSuccessMessage
                                              },
                                              {
                                                  type: "success"
                                              }
                                          );
                                      }
                                  } else {
                                      $.notify(
                                          {
                                              message: elem.attr("data-error-message") || text.message
                                          },
                                          {
                                              type: "danger"
                                          }
                                      );
                                      var t = setTimeout(function() {
                                          button.html(buttonText);
                                      }, 1000);
                                  }
                              }
                          });
                      }
                  });
              });
          }
      },
    formHRDownload: function() {
        var $ajaxForm = $(".form-popup-hr, .ajax-form");
        var $modalHr = $(".popup-hr-download");
        var $modalFormHr = $('form.form-popup-hr');
        if ($ajaxForm.length > 0) {
            $ajaxForm.each(function () {
                var elem = $(this),
                    elemSuccessMessage =
                        elem.attr("data-success-message") ||
                        "Thank you for your message we will get back to you as soon as possible.",
                    elemCustomRedirectPage = elem.attr("data-hr-insider");
                var button = elem.find("button#popup-hr-bottom"),
                    buttonText = button.html();
                elem.submit(function (event) {
                    event.preventDefault();
                    var post_url = $(this).attr("action");
                    var request_method = $(this).attr("method");
                    var form_data = $(this).serialize();
                    if (elem[0].checkValidity() === false) {
                        event.stopPropagation();
                        elem.addClass("was-validated");
                    } else {
                        $(elem).removeClass("was-validated");
                        button.html('<i class="icon-loader fa-spin"> </i> Sending...');
                        $.ajax({
                            url: post_url,
                            type: request_method,
                            data: form_data,
                            success: function (text) {
                                if (text.response === "success") {
                                    $(elem)[0].reset();
                                    button.html(buttonText);
                                    if (elemCustomRedirectPage) {
                                        window.location.href = elemCustomRedirectPage;
                                        $modalFormHr.hide();
                                        $modalHr.each(function() {
                                            var elemModal = $(this);
                                            elemModal.removeClass("modal-active");
                                            return false;
                                        });
                                    } else {
                                        $.notify(
                                            {
                                                message: elemSuccessMessage
                                            },
                                            {
                                                type: "success"
                                            }
                                        );
                                    }
                                } else {
                                    $.notify(
                                        {
                                            message: elem.attr("data-error-message") || text.message
                                        },
                                        {
                                            type: "danger"
                                        }
                                    );
                                    var t = setTimeout(function () {
                                        button.html(buttonText);
                                    }, 1000);
                                }
                            }
                        });
                    }
                });
            });
        }
    },
    formFreeDiagnostic: function() {
          var $ajaxForm = $(".form-free-diagnostic, .ajax-form");
          if ($ajaxForm.length > 0) {
              $ajaxForm.each(function() {
                  var elem = $(this),
                      elemSuccessMessage =
                          elem.attr("data-success-message") ||
                          "Thank you for your message we will get back to you as soon as possible.",
                      elemCustomRedirectPage = elem.attr("data-success-page");
                  var button = elem.find("button#free-diagnostic-bottom"),
                      buttonText = button.html();
                  var validation = Array.prototype.filter.call(elem, function(form) {
                      form.addEventListener(
                          "submit",
                          function(event) {
                              if (form[0].checkValidity() === false) {
                                  event.preventDefault();
                                  event.stopPropagation();
                              }
                              form.classList.add("was-validated");
                              return false;
                          },
                          false
                      );
                  });
                  elem.submit(function(event) {
                      event.preventDefault();
                      var post_url = $(this).attr("action");
                      var request_method = $(this).attr("method");
                      var form_data = $(this).serialize();
                      if (elem[0].checkValidity() === false) {
                          event.stopPropagation();
                          elem.addClass("was-validated");
                      } else {
                          $(elem).removeClass("was-validated");
                          button.html('<i class="icon-loader fa-spin"> </i> Sending...');
                          $.ajax({
                              url: post_url,
                              type: request_method,
                              data: form_data,
                              success: function(text) {
                                  if (text.response === "success") {
                                      $(elem)[0].reset();
                                      button.html(buttonText);
                                      if (elemCustomRedirectPage) {
                                          window.location.href = elemCustomRedirectPage;
                                      } else {
                                          $.notify(
                                              {
                                                  message: elemSuccessMessage
                                              },
                                              {
                                                  type: "success"
                                              }
                                          );
                                      }
                                  } else {
                                      $.notify(
                                          {
                                              message: elem.attr("data-error-message") || text.message
                                          },
                                          {
                                              type: "danger"
                                          }
                                      );
                                      var t = setTimeout(function() {
                                          button.html(buttonText);
                                      }, 1000);
                                  }
                              }
                          });
                      }
                  });
              });
          }
      },
    formFreeDiagnosticHome: function() {
          var $ajaxForm = $(".form-free-diagnostic-home, .ajax-form");
          if ($ajaxForm.length > 0) {
              $ajaxForm.each(function() {
                  var elem = $(this),
                      elemSuccessMessage =
                          elem.attr("data-success-message") ||
                          "Thank you for your message we will get back to you as soon as possible.",
                      elemCustomRedirectPage = elem.attr("data-success");
                  var button = elem.find("button#free-diagnostic-home-bottom"),
                      buttonText = button.html();
                  var validation = Array.prototype.filter.call(elem, function(form) {
                      form.addEventListener(
                          "submit",
                          function(event) {
                              if (form[0].checkValidity() === false) {
                                  event.preventDefault();
                                  event.stopPropagation();
                              }
                              form.classList.add("was-validated");
                              return false;
                          },
                          false
                      );
                  });
                  elem.submit(function(event) {
                      event.preventDefault();
                      var post_url = $(this).attr("action");
                      var request_method = $(this).attr("method");
                      var form_data = $(this).serialize();
                      if (elem[0].checkValidity() === false) {
                          event.stopPropagation();
                          elem.addClass("was-validated");
                      } else {
                          $(elem).removeClass("was-validated");
                          button.html('<i class="icon-loader fa-spin"> </i> Sending...');
                          $.ajax({
                              url: post_url,
                              type: request_method,
                              data: form_data,
                              success: function(text) {
                                  if (text.response === "success") {
                                      $(elem)[0].reset();
                                      button.html(buttonText);
                                      if (elemCustomRedirectPage) {
                                          sessionStorage.freeDiagnostic = 1;
                                          window.location.href = elemCustomRedirectPage;
                                      } else {
                                          $.notify(
                                              {
                                                  message: elemSuccessMessage
                                              },
                                              {
                                                  type: "success"
                                              }
                                          );
                                      }
                                  } else {
                                      sessionStorage.freeDiagnostic = 2;
                                      $.notify(
                                          {
                                              message: elem.attr("data-error-message") || text.message
                                          },
                                          {
                                              type: "danger"
                                          }
                                      );
                                      var t = setTimeout(function() {
                                          button.html(buttonText);
                                      }, 1000);
                                  }
                              }
                          });
                      }
                  });
              });
          }
      },
    other: function(context) {
      //Lazy Load
      $(function() {
        $(".lazy").Lazy({
          afterLoad: function(element) {
            element.addClass("img-loaded");
          }
        });
      });
      if ($(".toggle-item").length > 0) {
        $(".toggle-item").each(function() {
          var elem = $(this),
            toggleItemClass = elem.attr("data-class"),
            toggleItemClassTarget = elem.attr("data-target");
          elem.on("click", function() {
            if (toggleItemClass) {
              if (toggleItemClassTarget) {
                $(toggleItemClassTarget).toggleClass(toggleItemClass);
              } else {
                elem.toggleClass(toggleItemClass);
              }
            }
            elem.toggleClass("toggle-active");
            return false;
          });
        });
      }
      /*Dropdown popup invert*/
      var $pDropdown = $(".p-dropdown");
      if ($pDropdown.length > 0) {
        $pDropdown.each(function() {
          var elem = $(this);
          if ($window.width() / 2 > elem.offset().left) {
            elem.addClass("p-dropdown-invert");
          }
        });
      }
    },
    naTo: function() {
      $(
        "a.scroll-to, #dotsMenu > ul > li > a, .menu-one-page nav > ul > li > a"
      ).on("click", function() {
        var extraPaddingTop = 0,
          extraHeaderHeight = 0;
        $(window).breakpoints("lessThan", "lg", function() {
          if (Settings.menuIsOpen) {
            $mainMenuTriggerBtn.trigger("click");
          }
          if ($header.attr("data-responsive-fixed") === true) {
            extraHeaderHeight = $header.height();
          }
        });
        $(window).breakpoints("greaterEqualTo", "lg", function() {
          if ($header.length > 0) {
            extraHeaderHeight = $header.height();
          }
        });
        if ($(".dashboard").length > 0) {
          extraPaddingTop = 30;
        }
        var $anchor = $(this);
        $("html, body")
          .stop(true, false)
          .animate(
            {
              scrollTop:
                $($anchor.attr("href")).offset().top -
                (extraHeaderHeight + extraPaddingTop)
            },
            1500,
            "easeInOutExpo"
          );
        return false;
      });
    },
    buttons: function() {
      //Button slide width
      if ($(".btn-slide[data-width]")) {
        $(".btn.btn-slide[data-width]").each(function() {
          var elem = $(this),
            elemWidth = elem.attr("data-width"),
            elemDefaultWidth;
          switch (true) {
            case elem.hasClass("btn-lg"):
              elemDefaultWidth = "60";
              break;
            case elem.hasClass("btn-sm"):
              elemDefaultWidth = "36";
              break;
            case elem.hasClass("btn-xs"):
              elemDefaultWidth = "28";
              break;
            default:
              elemDefaultWidth = "48";
              break;
          }
          elem.hover(
            function() {
              $(this).css("width", elemWidth + "px");
            },
            function() {
              $(this).css("width", elemDefaultWidth + "px");
            }
          );
        });
      }
    },
    accordion: function() {
      var accordionType = "accordion",
        toogleType = "toggle",
        accordionItem = "ac-item",
        itemActive = "ac-active",
        itemTitle = "ac-title",
        itemContent = "ac-content",
        $accs = $("." + accordionItem);
      $accs.length &&
        ($accs.each(function() {
          var $item = $(this);
          $item.hasClass(itemActive)
            ? $item.addClass(itemActive)
            : $item.find("." + itemContent).hide();
        }),
        $("." + itemTitle).on("click", function(e) {
          var $link = $(this),
            $item = $link.parents("." + accordionItem),
            $acc = $item.parents("." + accordionType);
          $item.hasClass(itemActive)
            ? $acc.hasClass(toogleType)
              ? ($item.removeClass(itemActive),
                $link.next("." + itemContent).slideUp())
              : ($acc.find("." + accordionItem).removeClass(itemActive),
                $acc.find("." + itemContent).slideUp())
            : ($acc.hasClass(toogleType) ||
                ($acc.find("." + accordionItem).removeClass(itemActive),
                $acc.find("." + itemContent).slideUp("fast")),
              $item.addClass(itemActive),
              $link.next("." + itemContent).slideToggle("fast")),
            e.preventDefault();
          return false;
        }));
    },
    animations: function() {
      var $animate = $("[data-animate]");
      if ($animate.length > 0) {
        //Check if jQuery Waypoint plugin is loaded
        if (typeof Waypoint === "undefined") {
          VENSURE.elements.notification(
            "Warning",
            "jQuery Waypoint plugin is missing in plugins.js file.",
            "danger"
          );
          return true;
        }
        $animate.each(function() {
          var elem = $(this);
          elem.addClass("animated");
          //Plugin Options
          elem.options = {
            animation: elem.attr("data-animate") || "fadeIn",
            delay: elem.attr("data-animate-delay") || 200,
            direction: ~elem.attr("data-animate").indexOf("Out")
              ? "back"
              : "forward",
            offsetX: elem.attr("data-animate-offsetX") || 0,
            offsetY: elem.attr("data-animate-offsetY") || -100
          };
          //Initializing jQuery Waypoint plugin and passing the options from data animations attributes
          if (elem.options.direction == "forward") {
            new Waypoint({
              element: elem,
              handler: function() {
                var t = setTimeout(function() {
                  elem.addClass(elem.options.animation + " visible");
                }, elem.options.delay);
                this.destroy();
              },
              offset: "100%"
            });
          } else {
            elem.addClass("visible");
            elem.on("click", function() {
              elem.addClass(elem.options.animation);
              return false;
            });
          }
          //Demo play
          if (elem.parents(".demo-play-animations").length) {
            elem.on("click", function() {
              elem.removeClass(elem.options.animation);
              var t = setTimeout(function() {
                elem.addClass(elem.options.animation);
              }, 50);
              return false;
            });
          }
        });
      }
    },
    parallax: function() {
      var $parallax = $("[data-bg-parallax]");
      if ($parallax.length > 0) {
        //Check if scrolly plugin is loaded
        if (typeof $.fn.scrolly === "undefined") {
          VENSURE.elements.notification(
            "Warning",
            "jQuery scrolly plugin is missing in plugins.js file.",
            "danger"
          );
          return true;
        }
        $parallax.each(function() {
          var $elem = $(this),
            elemImageSrc = $elem.attr("data-bg-parallax"),
            elemImageVelocity = $elem.attr("data-velocity") || "-.140";
          $elem.prepend('<div class="parallax-container" data-lazy-background="' +elemImageSrc +'"  data-velocity="' +elemImageVelocity +'" style="background: url(' +elemImageSrc +')"></div>'
          );
          $(".parallax-container").lazy({
            attribute: "data-lazy-background",
            afterLoad: function(element) {
              $elem.find(".parallax-container").addClass("img-loaded");
            }
          });
          if ($body.hasClass("breakpoint-lg") || $body.hasClass("breakpoint-xl")) {
            $elem.find(".parallax-container").scrolly({
              bgParallax: true
            });
          } else {
            $elem.find(".parallax-container").addClass("parallax-responsive");
          }
        });
      }
    },
    parallaxHome: function() {
          var $parallax = $("[data-bg-parallax-home]");
          if ($parallax.length > 0) {
              //Check if scrolly plugin is loaded
              if (typeof $.fn.scrolly === "undefined") {
                  VENSURE.elements.notification(
                      "Warning",
                      "jQuery scrolly plugin is missing in plugins.js file.",
                      "danger"
                  );
                  return true;
              }
              $parallax.each(function() {
                  var $elem = $(this),
                      elemImageSrc = $elem.attr("data-bg-parallax-home"),
                      elemImageVelocity = $elem.attr("data-velocity") || "0";
                  $elem.prepend('<div class="parallax-container-home" data-lazy-background="' +elemImageSrc +'" data-velocity="' +elemImageVelocity +'" style="background: url(' +elemImageSrc +')"></div>'
                  );
                  $(".parallax-container-home").lazy({
                      attribute: "data-lazy-background",
                      afterLoad: function(element) {
                          $elem.find(".parallax-container-home").addClass("img-loaded");
                      }
                  });
                  if ($body.hasClass("breakpoint-lg") || $body.hasClass("breakpoint-xl")) {
                      $elem.find(".parallax-container-home").scrolly({
                          bgParallax: true
                      });
                  } else {
                      $elem.find(".parallax-container-home").addClass("parallax-responsive");
                  }
              });
          }
    },
    backgroundImage: function() {
      var $backgroundImage = $("[data-bg-image]");
      if ($backgroundImage.length > 0) {
        $backgroundImage.each(function() {
          var $elem = $(this),
            elemImageSrc = $elem.attr("data-bg-image");
          $elem.attr("data-lazy-background", elemImageSrc);
          $elem.lazy({
            attribute: "data-lazy-background",
            afterLoad: function(element) {
              $elem.addClass("bg-loaded");
            }
          });
        });
      }
    },
    responsiveVideos: function() {
      //selecting elements
      var selectors = [
        'iframe[src*="player.vimeo.com"]',
        'iframe[src*="youtube.com"]',
        'iframe[src*="youtube-nocookie.com"]',
        'iframe[src*="kickstarter.com"][src*="video.html"]',
        "object",
        "embed"
      ];
      var videoContainers = $(
        "section, .content, .post-content, .video-js, .post-video, .video-wrap, .ajax-quick-view,#slider:not(.revslider-wrap)"
      );
      var elem = videoContainers.find(selectors.join(","));
      if (elem) {
        elem.each(function() {
          $(this).wrap(
            '<div class="embed-responsive embed-responsive-16by9"></div>'
          );
        });
      }
    },
    counters: function() {
      var $counter = $(".counter");
      if ($counter.length > 0) {
        //Check if countTo plugin is loaded
        if (typeof $.fn.countTo === "undefined") {
          VENSURE.elements.notification(
            "Warning",
            "jQuery countTo plugin is missing in plugins.js file.",
            "danger"
          );
          return true;
        }
        //Initializing countTo plugin
        $counter.each(function() {
          var elem = $(this),
            prefix = elem.find("span").attr("data-prefix") || "",
            suffix = elem.find("span").attr("data-suffix") || "";

          new Waypoint({
            element: elem,
            handler: function() {
              elem.find("span").countTo({
                refreshInterval: 2,
                formatter: function(value, options) {
                  return (
                    String(prefix) +
                    value.toFixed(options.decimals) +
                    String(suffix)
                  );
                }
              });
              this.destroy();
            },
            offset: "104%"
          });
        });
      }
    },
    countdownTimer: function() {
      var $countdownTimer = $(".countdown");
      if ($countdownTimer.length > 0) {
        //Check if countdown plugin is loaded
        if (typeof $.fn.countdown === "undefined") {
          VENSURE.elements.notification(
            "Warning",
            "jQuery countdown plugin is missing in plugins.js file.",
            "danger"
          );
          return true;
        }
        $("[data-countdown]").each(function() {
          var $this = $(this),
            finalDate = $(this).attr("data-countdown");
          $this.countdown(finalDate, function(event) {
            $this.html(
              event.strftime(
                '<div class="countdown-container"><div class="countdown-box"><div class="number">%-D</div><span>Day%!d</span></div>' +
                  '<div class="countdown-box"><div class="number">%H</div><span>Hours</span></div>' +
                  '<div class="countdown-box"><div class="number">%M</div><span>Minutes</span></div>' +
                  '<div class="countdown-box"><div class="number">%S</div><span>Seconds</span></div></div>'
              )
            );
          });
        });
      }
    },
    gridLayout: function() {
      if ($gridLayout.length > 0) {
        //Check if isotope plugin is loaded
        if (typeof $.fn.isotope === "undefined") {
          VENSURE.elements.notification(
            "Warning",
            "jQuery isotope plugin is missing in plugins.js file.",
            "danger"
          );
          return true;
        }

        var isotopeRTL;
        if (VENSURE.core.rtlStatus()) {
          isotopeRTL = false;
        } else {
          isotopeRTL = true;
        }

        $gridLayout.each(function() {
          var elem = $(this);
          elem.options = {
            itemSelector: elem.attr("data-item") || "portfolio-item",
            layoutMode: elem.attr("data-layout") || "masonry",
            filter: elem.attr("data-default-filter") || "*",
            stagger: elem.attr("data-stagger") || 0,
            autoHeight: elem.data("auto-height") == false ? false : true,
            gridMargin: elem.attr("data-margin") || 20,
            gridMarginXs: elem.attr("data-margin-xs"),
            transitionDuration: elem.attr("data-transition") || "0.45s",
            isOriginLeft: isotopeRTL
          };

          $(window).breakpoints("lessThan", "lg", function() {
            elem.options.gridMargin =
              elem.options.gridMarginXs || elem.options.gridMargin;
          });

          elem.css(
            "margin",
            "0 -" +
              elem.options.gridMargin +
              "px -" +
              elem.options.gridMargin +
              "px 0"
          );
          elem
            .find("." + elem.options.itemSelector)
            .css(
              "padding",
              "0 " +
                elem.options.gridMargin +
                "px " +
                elem.options.gridMargin +
                "px 0"
            );
          if (elem.attr("data-default-filter")) {
            var elemDefaultFilter = elem.options.filter;
            elem.options.filter = "." + elem.options.filter;
          }
          elem.append('<div class="grid-loader"></div>');
          var $isotopelayout = $(elem).imagesLoaded(function() {
            // init Isotope after all images have loaded
            $isotopelayout.isotope({
              layoutMode: elem.options.layoutMode,
              transitionDuration: elem.options.transitionDuration,
              stagger: Number(elem.options.stagger),
              resize: true,
              itemSelector:
                "." + elem.options.itemSelector + ":not(.grid-loader)",
              isOriginLeft: elem.options.isOriginLeft,
              autoHeight: elem.options.autoHeight,
              masonry: {
                columnWidth: elem.find(
                  "." + elem.options.itemSelector + ":not(.large-width)"
                )[0]
              },
              filter: elem.options.filter
            });
            elem.remove(".grid-loader").addClass("grid-loaded");
          });
        });
      }
    },
    magnificPopup: function() {
      var $lightbox = $("[data-lightbox]");
      if ($lightbox.length > 0) {
        //Check if magnificPopup plugin is loaded
        if (typeof $.fn.magnificPopup === "undefined") {
          VENSURE.elements.notification(
            "Warning",
            "jQuery magnificPopup plugin is missing in plugins.js file.",
            "danger"
          );
          return true;
        }
        //Get lightbox data type
        var getType = {
          image: {
            type: "image",
            closeOnContentClick: true,
            removalDelay: 500,
            image: {
              verticalFit: true
            },
            callbacks: {
              beforeOpen: function() {
                this.st.image.markup = this.st.image.markup.replace(
                  "mfp-figure",
                  "mfp-figure mfp-with-anim"
                );
                this.st.mainClass = "mfp-zoom-out";
              }
            }
          },
          gallery: {
            delegate:
              'a[data-lightbox="gallery-image"], a[data-lightbox="image"]',
            type: "image",
            image: {
              verticalFit: true
            },
            gallery: {
              enabled: true,
              navigateByImgClick: true,
              preload: [0, 1]
            },
            removalDelay: 500,
            callbacks: {
              beforeOpen: function() {
                this.st.image.markup = this.st.image.markup.replace(
                  "mfp-figure",
                  "mfp-figure mfp-with-anim"
                );
                this.st.mainClass = "mfp-zoom-out";
              }
            }
          },
          iframe: {
            type: "iframe",
            removalDelay: 500,
            callbacks: {
              beforeOpen: function() {
                this.st.image.markup = this.st.image.markup.replace(
                  "mfp-figure",
                  "mfp-figure mfp-with-anim"
                );
                this.st.mainClass = "mfp-zoom-out";
              }
            }
          },
          ajax: {
            type: "ajax",
            removalDelay: 500,
            callbacks: {
              ajaxContentAdded: function(mfpResponse) {
                VENSURE.slider.carouselAjax();
                VENSURE.elements.responsiveVideos();
                VENSURE.elements.buttons();
              }
            }
          },
          inline: {
            type: "inline",
            removalDelay: 500,
            callbacks: {
              beforeOpen: function() {
                this.st.image.markup = this.st.image.markup.replace(
                  "mfp-figure",
                  "mfp-figure mfp-with-anim"
                );
                this.st.mainClass = "mfp-zoom-out";
              }
            },
            closeBtnInside: false,
            fixedContentPos: true,
            overflowY: "scroll"
          }
        };
        //Initializing jQuery magnificPopup plugin and passing the options
        $lightbox.each(function() {
          var elem = $(this),
            elemType = elem.attr("data-lightbox");
          switch (elemType) {
            case "image":
              elem.magnificPopup(getType.image);
              break;
            case "gallery":
              elem.magnificPopup(getType.gallery);
              break;
            case "iframe":
              elem.magnificPopup(getType.iframe);
              break;
            case "ajax":
              elem.magnificPopup(getType.ajax);
              break;
            case "inline":
              elem.magnificPopup(getType.inline);
              break;
          }
        });
      }
    },
    modal: function() {
      //Check if magnificPopup plugin is loaded
      if (typeof $.fn.magnificPopup === "undefined") {
        VENSURE.elements.notification(
          "Warning",
          "jQuery magnificPopup plugin is missing in plugins.js file.",
          "danger"
        );
        return true;
      }
      var $modal = $(".modal"),
        $modalStrip = $(".popup-hr-download"),
        $btnModal = $(".btn-modal"),
        modalShow = "modal-auto-open",
        modalShowClass = "modal-active",
        modalDecline = $(".modal-close"),
        cookieNotify = $(".cookie-notify"),
        cookieConfirm = cookieNotify.find(".modal-confirm, .mfp-close");
      /*Modal*/
      if ($modal.length > 0) {
        $modal.each(function() {
          var elem = $(this),
            elemDelay = elem.attr("data-delay") || 3000,
            elemCookieExpire = elem.attr("data-cookie-expire") || 365,
            elemCookieName =
              elem.attr("data-cookie-name") || "cookieModalName2020_3",
            elemCookieEnabled =
              elem.data("cookie-enabled") == true ? true : false,
            elemModalDismissDelay = elem.attr("data-delay-dismiss");
          /*Modal Auto Show*/
          if (elem.hasClass(modalShow)) {
            var modalElem = $(this);
            var timeout = setTimeout(function() {
              modalElem.addClass(modalShowClass);
            }, elemDelay);
          }
          /*Modal Dissmis Button*/
          elem.find(modalDecline).click(function() {
            elem.removeClass(modalShowClass);
            return false;
          });
          /*Modal Auto Show*/
          if (elem.hasClass(modalShow)) {
            if (elemCookieEnabled != true) {
              /*Cookie Notify*/
              var t = setTimeout(function() {
                $.magnificPopup.open(
                  {
                    items: {
                      src: elem
                    },
                    type: "inline",
                    closeBtnInside: true,
                    callbacks: {
                      beforeOpen: function() {
                        this.st.image.markup = this.st.image.markup.replace(
                          "mfp-figure",
                          "mfp-figure mfp-with-anim"
                        );
                        this.st.mainClass = "mfp-zoom-out";
                      },
                      open: function() {
                        if (elem.find("video").length > 0) {
                          elem
                            .find("video")
                            .get(0)
                            .play();
                        }
                      }
                    }
                  },
                  0
                );
              }, elemDelay);
            } else {
              if (typeof Cookies.get(elemCookieName) == "undefined") {
                /*Cookie Notify*/
                var t = setTimeout(function() {
                  $.magnificPopup.open(
                    {
                      items: {
                        src: elem
                      },
                      type: "inline",
                      closeBtnInside: true,
                      callbacks: {
                        beforeOpen: function() {
                          this.st.image.markup = this.st.image.markup.replace(
                            "mfp-figure",
                            "mfp-figure mfp-with-anim"
                          );
                          this.st.mainClass = "mfp-zoom-out";
                        },
                        open: function() {
                          if (elem.find("video").length > 0) {
                            elem
                              .find("video")
                              .get(0)
                              .play();
                          }
                        },
                        close: function() {
                          Cookies.set(elemCookieName, true, {
                            expires: Number(elemCookieExpire)
                          });
                        }
                      }
                    },
                    0
                  );
                }, elemDelay);
              }
            }
          }
          /*Modal Dissmis Button*/
          elem.find(modalDecline).click(function() {
            $.magnificPopup.close();
            return false;
          });

          if (elemModalDismissDelay) {
          }
        });
      }
      /*Modal Strip*/
      if ($modalStrip.length > 0) {
          var modalFormHr = $('form.form-popup-hr');
          modalFormHr.hide();
        $modalStrip.each(function() {
          var elem = $(this),
            elemDelay = elem.attr("data-delay") || 3000,
            elemCookieExpire = elem.attr("data-cookie-expire") || 365,
            elemCookieName = elem.attr("data-cookie-name") || "cookieName2013",
            elemCookieEnabled =
            elem.data("cookie-enabled") == true ? true : false,
            elemModalDismissDelay = elem.attr("data-delay-dismiss");
          /*Modal Auto Show*/
          if (elem.hasClass(modalShow)) {
            var modalElem = $(this);
            var timeout = setTimeout(function() {
              modalFormHr.show();
              modalElem.addClass(modalShowClass);
              if (elemModalDismissDelay) {
                var t = setTimeout(function() {
                  elem.removeClass(modalShowClass);
                }, elemModalDismissDelay);
              }
            }, elemDelay);
          }
          /*Modal Dissmis Button*/
          elem.find(modalDecline).click(function() {
            modalFormHr.hide();
            elem.removeClass(modalShowClass);
            return false;
          });
          /*Cookie Notify*/
          if (elem.hasClass("cookie-notify")) {
            var timeout = setTimeout(function() {
              if (elemCookieEnabled != true) {
                cookieNotify.addClass(modalShowClass);
              } else {
                if (typeof Cookies.get(elemCookieName) == "undefined") {
                  cookieNotify.addClass(modalShowClass);
                }
              }
            }, elemDelay);
            cookieConfirm.click(function() {
              Cookies.set(elemCookieName, true, {
                expires: Number(elemCookieExpire)
              });
              $.magnificPopup.close();
              cookieNotify.removeClass(modalShowClass);
              return false;
            });
          }
        });
      }

      /*Modal toggles*/
      if ($btnModal.length > 0) {
        $btnModal.each(function() {
          var elem = $(this),
            modalTarget = elem.attr("data-modal");
          elem.click(function() {
            $(modalTarget).toggleClass(modalShowClass, 1000);
            return false;
          });
        });
      }
    },
    notification: function($title, $message, $type, $element, $delay, $placement, $animateEnter, $animateExit, $backgroundImage, $timeout) {
      var $element,
        $elementContainer,
        $animateEnter = $animateEnter || "fadeInDown",
        $animateExit = $animateExit || "fadeOutDown",
        $placement,
        $backgroundImage,
        $backgroundImageContainer,
        $timeout;

      if ($placement) {
        $placement = $placement;
      } else {
        $placement = "top";
      }

      if ($element) {
        $elementContainer = "element-container";
        ($animateEnter = "fadeIn"), ($animateExit = "fadeOut");
      } else {
        $elementContainer = "col-11 col-md-4";
      }

      if ($backgroundImage) {
        $backgroundImageContainer =
          'style="background-image:url(' +
          $backgroundImage +
          '); background-repeat: no-repeat; background-position: 50% 20%; height:120px !important; width:430px !important; border:0px;" ';
      }

      if (!$message) {
        $message = "";
      }

      $element = "body";

      var notify = function() {
        $.notify(
          {
            title: $title,
            message: $message
          },
          {
            element: $element,
            type: $type || "warning",
            delay: $delay || 10000,
            template:
              '<div data-notify="container" ' +
              $backgroundImageContainer +
              ' class="bootstrap-notify ' +
              $elementContainer +
              ' alert alert-{0}" role="alert">' +
              '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
              '<span data-notify="icon"></span> ' +
              '<span data-notify="title">{1}</span> ' +
              '<span data-notify="message">{2}</span>' +
              "</div>",
            mouse_over: true,
            allow_dismiss: true,
            placement: {
              from: $placement
            },
            animate: {
              enter: "animated " + $animateEnter,
              exit: "animated " + $animateExit
            }
          }
        );
      };

      if ($timeout) {
        setTimeout(function() {
          notify();
        }, 2000);
      } else {
        notify();
      }
    },
    sidebarFixed: function() {
      if (VENSURE.core.rtlStatus()) {
        return true;
      }
      var $sidebarFixed = $(".sticky-sidebar");
      if ($sidebarFixed.length > 0) {
        //Check if theiaStickySidebar plugin is loaded
        if (typeof $.fn.theiaStickySidebar === "undefined") {
          VENSURE.elements.notification(
            "Warning",
            "jQuery theiaStickySidebar plugin is missing in plugins.js file.",
            "danger"
          );
          return true;
        }
        $sidebarFixed.each(function() {
          var elem = $(this);
          elem.options = {
            additionalMarginTop: elem.attr("data-margin-top") || 120,
            additionalMarginBottom: elem.attr("data-margin-bottom") || 50
          };
          //Initialize theiaStickySidebar plugin and passing the options
          elem.theiaStickySidebar({
            additionalMarginTop: Number(elem.options.additionalMarginTop),
            additionalMarginBottom: Number(elem.options.additionalMarginBottom),
            disableOnResponsiveLayouts: true
          });
        });
      }
    },
    countdown: function() {
      var $countdown = $(".p-countdown");
      if ($countdown.length > 0) {
        $countdown.each(function() {
          var $elem = $(this),
            $elemCount = $elem.find(".p-countdown-count"),
            $elemShow = $elem.find(".p-countdown-show"),
            $elemSeconds = $elem.attr("data-delay") || 5;
          $elemCount.find(".count-number").html($elemSeconds);
          new Waypoint({
            element: $elem,
            handler: function() {
              var interval = setInterval(function() {
                $elemSeconds--;
                if ($elemSeconds == 0) {
                  clearInterval(interval);
                  $elemCount.fadeOut("slow");
                  setTimeout(function() {
                    $elemShow.fadeIn("show");
                  }, 1000);
                } else {
                  $elemCount.find(".count-number").html($elemSeconds);
                }
              }, 1000);
              this.destroy();
            },
            offset: "100%"
          });
        });
      }
    },
  };
  //Load Functions on document ready
  $(document).ready(function() {
    VENSURE.core.functions();
    VENSURE.header.functions();
    VENSURE.slider.functions();
    VENSURE.elements.functions();
  });
  //Recall Functions on window scroll
  $window.on("scroll", function() {
    VENSURE.header.stickyHeader();
    VENSURE.header.dotsMenu();
  });
  //Recall Functions on window resize
  $window.on("resize", function() {
    VENSURE.header.logoStatus();
    VENSURE.header.stickyHeader();
  });
})(jQuery);
