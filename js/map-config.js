﻿var usjsconfig = {
  "usjs1":{
    "hover": "ALABAMA", // info of the popup
    "url": "client-center/alabama", // link
    "target": "same_window", //  use "new_window", "same_window", "modal", or "none"
    "upColor": "#f8a262", // default color
    "overColor": "#ffffff", // highlight color
    "downColor": "#f8a262", // clicking color
    "active": true // true or false to activate or deactivate
  },
  "usjs2":{
    "hover": "ALASKA",
    "url": "client-center/alaska", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs3":{
    "hover": "ARIZONA",
    "url": "client-center/arizona", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs4":{
    "hover": "ARKANSAS",
    "url": "client-center/arkansas", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs5":{
    "hover": "CALIFORNIA",
    "url": "client-center/california", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs6":{
    "hover": "COLORADO",
    "url": "client-center/colorado", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs7":{
    "hover": "CONNECTICUT",
    "url": "client-center/connecticut", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs8":{
    "hover": "DELAWARE",
    "url": "client-center/delaware", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs9":{
    "hover": "FLORIDA",
    "url": "client-center/florida", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs10":{
    "hover": "GEORGIA",
    "url": "client-center/georgia", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs11":{
    "hover": "HAWAII",
    "url": "client-center/hawaii", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs12":{
    "hover": "IDAHO",
    "url": "client-center/idaho", "target": "same_window",
    "upColor": "#f9a462", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs13":{
    "hover": "ILLINOIS",
    "url": "client-center/illinois", "target": "same_window",
    "upColor": "#fdddc2", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs14":{
    "hover": "INDIANA",
    "url": "client-center/indiana", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs15":{
    "hover": "IOWA",
    "url": "client-center/iowa", "target": "same_window",
    "upColor": "#fcc090", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs16":{
    "hover": "KANSAS",
    "url": "client-center/kansas", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs17":{
    "hover": "KENTUCKY",
    "url": "client-center/kentucky", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs18":{
    "hover": "LOUISIANA",
    "url": "client-center/louisiana", "target": "same_window",
    "upColor": "#fddec2", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs19":{
    "hover": "MAINE",
    "url": "client-center/maine", "target": "same_window",
    "upColor": "#fcc090", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs20":{
    "hover": "MARYLAND",
    "url": "client-center/maryland", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs21":{
    "hover": "MASSACHUSETTS",
    "url": "client-center/massachusetts", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs22":{
    "hover": "MICHIGAN",
    "url": "client-center/michigan", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs23":{
    "hover": "MINNESOTA",
    "url": "client-center/minnesota", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs24":{
    "hover": "MISSISSIPPI",
    "url": "client-center/mississippi", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs25":{
    "hover": "MISSOURI",
    "url": "client-center/missouri", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs26":{
    "hover": "MONTANA",
    "url": "client-center/montana", "target": "same_window",
    "upColor": "#fdddc2", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs27":{
    "hover": "NEBRASKA",
    "url": "client-center/nebraska", "target": "same_window",
    "upColor": "#fdddc2", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs28":{
    "hover": "NEVADA",
    "url": "client-center/nevada", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs29":{
    "hover": "NEW HAMPSHIRE",
    "url": "client-center/new-hampshire", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs30":{
    "hover": "NEW JERSEY",
    "url": "client-center/new-jersey", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs31":{
    "hover": "NEW MEXICO",
    "url": "client-center/new-mexico", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs32":{
    "hover": "NEW YORK",
    "url": "client-center/new-york", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs33":{
    "hover": "NORTH CAROLINA",
    "url": "client-center/north-carolina", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs34":{
    "hover": "NORTH DAKOTA",
    "url": "client-center/north-dakota", "target": "same_window",
    "upColor": "#fcc090", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs35":{
    "hover": "OHIO",
    "url": "client-center/ohio", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs36":{
    "hover": "OKLAHOMA",
    "url": "client-center/oklahoma", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs37":{
    "hover": "OREGON",
    "url": "client-center/oregon", "target": "same_window",
    "upColor": "#f68932", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs38":{
    "hover": "PENNSYLVANIA",
    "url": "client-center/pennsylvania", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs39":{
    "hover": "RHODE ISLAND",
    "url": "client-center/rhode-island", "target": "same_window",
    "upColor": "#f8a262", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs40":{
    "hover": "SOUTH CAROLINA",
    "url": "client-center/south-carolina", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs41":{
    "hover": "SOUTH DAKOTA",
    "url": "client-center/south-dakota", "target": "same_window",
    "upColor": "#f68932", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs42":{
    "hover": "TENNESSEE",
    "url": "client-center/tennessee", "target": "same_window",
    "upColor": "#fddec2", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs43":{
    "hover": "TEXAS",
    "url": "client-center/texas", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs44":{
    "hover": "UTAH",
    "url": "client-center/utah", "target": "same_window",
    "upColor": "#fddec2", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs45":{
    "hover": "VERMONT",
    "url": "client-center/vermont", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs46":{
    "hover": "VIRGINIA",
    "url": "client-center/virginia", "target": "same_window",
    "upColor": "#fdddc2", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs47":{
    "hover": "WASHINGTON",
    "url": "client-center/washington", "target": "same_window",
    "upColor": "#fcc090", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs48":{
    "hover": "WEST VIRGINIA",
    "url": "client-center/west-virginia", "target": "same_window",
    "upColor": "#fdddc2", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs49":{
    "hover": "WISCONSIN",
    "url": "client-center/wisconsin", "target": "same_window",
    "upColor": "#f68831", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs50":{
    "hover": "WYOMING",
    "url": "client-center/wyoming", "target": "same_window",
    "upColor": "#fcc090", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "usjs51":{
    "hover": "WASHINGTON DC",
    "url": "client-center/district-of-columbia", "target": "same_window",
    "upColor": "#fbc08f", "overColor": "#e05205", "downColor": "#ffffff",
    "active": true
  },
  "general":{
    "borderColor": "",
    "visibleNames": "#35383b",
    "lakesFill": "#C5E8FF",
    "lakesOutline": "#95d6ff"
  }
};